<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validaciones
    |--------------------------------------------------------------------------
    |
    | Las siguientes líneas se usan para construir los mensajes de validaciones
    |
    */
    'required' => ':attribute es requerido.',
    'alpha_num'=> ':attribute debe ser alfanumérico',
    'between' => ':attribute: :input debe ser un valor entre :min - :max.',
    'numeric' => ':attribute debe ser un número.',
    'max' => [
        'numeric' => ':attribute no puede ser mayor a  :max.',
        'file' => ':attribute no puede ser mayor a  :max. kilobytes.',
        'string' => ':attribute no puede tener mas de :max letras.',
        'array' => ':attribute no puede tener mas de :max items.',
    ],
    'mimes' => 'The :attribute must be a file of type: :values.',
    'mimetypes' => 'The :attribute must be a file of type: :values.',
    'min' => [
        'numeric' => ':attribute no puede ser menor a :min.',
        'file' => ':attribute no puede ser menor a :min kilobytes.',
        'string' => ':attribute no puede tener menos de :min letras.',
        'array' => ':attribute no puede tener menos de :min items.',
    ],
    'email' => ':attribute: Ingrese un e-mail válido',
    'phone' => 'Ingrese un teléfono válido',
    'not_regex' => 'El formato de :attribute es inválido.',
    'regex' => 'El formato de :attribute es inválido.',
    'url' => 'El formato de :attribute es inválido',
    'date' => ':attribute debe ser válida.',
    'unique' => ':attribute ya esta registrada/o',
    'exists'=>':attribute es inexistente',
    'attributes'=>
    [   
        'nombre' => 'Nombre',
        'apellido' => 'Apellido',
        'mail' => 'Mail',
        'telefono' => 'Teléfono',
        'direccion' => 'Dirección',
        'calle' => 'Calle',
        'puntos' => 'Puntos',
        'altura' => 'Altura',
        'depto' => 'Depto.',
        'codigoPostal' => 'Código postal',
        'piso' => 'Piso',
        'pais' => 'País',
        'Ciudad' => 'Ciudad',
        'genero' => 'Género',
        'estadoCivil' => 'Estado civil',
        'nacimiento' => 'Fecha de Nacimiento',
        'dni' => 'DNI',
        'idmagtarjeta' => 'Identificador magnético de tarjeta',
        'purchaseItems'=> 'Items de compra',
        'swapItems'=>'Items de canje',
        'ciudad_id'=>'Ubicacion',
        'extra'=>'Extra',
        'idmagtarjeta'=>'Id magnético de tarjeta',
        'mailUsername'=>'Mail->Nombre de usuario',
        'mailFromMail'=>'Mail->De',
        'mailHost'=>'Mail->Host',
        'mailPort'=>'Mail->Puerto',
        'puntosPeso'=>'Coeficiente puntos por peso',
        'toDate'=>'Fecha-> Hasta',
        'fromDate'=>'Fecha-> Desde',
        'business_name'=>'Nombre de instancia',
        'businessName'=>'Nombre de instancia',
        'url'=>'URL',
        'adminusr'=>'Usuario administrador',
        'adminpass'=>'Password de administrador',
        'alias'=>'Alias'

    ],

];
