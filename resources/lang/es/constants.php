<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Constantes
    |--------------------------------------------------------------------------
    |
    | Las siguientes líneas se usan para construir los mensajes de constantes
    | y filtros globales
    |
    */
    'fem' => 'Mujer',
    'masc' => 'Hombre',
    'nc' => 'NC',
    'sin'=>'Soltera/o',
    'wid'=>'Viuda/o',
    'cou'=>'En pareja',
    'marr'=>'Casada/o',
    'admin'=>'Administrador',
    'business'=>'Comercio',
    'manual'=>'Manual'
];
