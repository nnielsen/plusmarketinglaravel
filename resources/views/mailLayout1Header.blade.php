<!doctype html>
<html>
<head>
	<meta name="viewport" content="width=device-width" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="author" content="mangools.com" />
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet"> 
	<style>
	table{
		font-family:"Source Sans Pro", sans-serif;
		color:#999fb8;
	}
	.contactate{
		margin:0 auto;
		margin-top: 8%;
	}
	@media only screen and (max-width: 640px) {
		.container {
			width: 100% !important;
			max-width: 100% !important;
		}
		table.column-1-2 {
			float: none !important;
			margin-right: 0 !important;
			width: 100% !important;
			text-align: center !important;
		}
		.contactate{
			margin-top: 0;
		}
	}
	@media only screen and (min-width: 641px) {
		.container {
			max-width: 600px !important;
		}
	}
</style> 
</head>
<body style="margin: 0; font-family: Source Sans Pro, sans-serif; font-size: 14px;">
	<table style="border-collapse: collapse;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
		<tr>
			<td style="background: #ecf5f9; padding: 50px 0;">
				<div style="max-width: 600px; margin: 0 auto;">
				<div style="max-width: 600px; margin: 0 auto;">