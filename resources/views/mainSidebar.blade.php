<div id="right-sidebar" class="animated">
    <div class="slimScrollDiv" style="position: relative; overflow: scroll; width: auto; height: 100%;"><div class="sidebar-container" style=" width: auto; height: 100%;">
        <div class="tab-content">
            <div class="tab-pane active">
                <div class="sidebar-title">
                    <h3><i class="fa fa-tasks"></i> {{ __('ui.configuration')}} </h3>
                </div>
                <div class="setings-item">
                    <span>
                        {{ __('ui.manualInput')}}
                    </span>
                    <div class="row">
                        <div class="col-md-6">
                            <button ng-click="setInputMode('0');setInputMethod()" ng-class="{'btn-info':manualCardInput}" class="btn btn-block btn-outline btn-xs m-t">ON</button>
                        </div>
                        <div class="col-md-6">
                            <button  ng-click="setInputMode('1')" ng-class="{'btn-info':!manualCardInput}" class="btn btn-block btn-outline btn-xs m-t">OFF</button>
                        </div>
                    </div>
                </div>
                <div ng-show="!manualCardInput" class="setings-item form-group" style="margin-top: 10px;">
                   <span> {{ __('ui.inputMethod')}}</span>
                    <div ng-repeat="x in mitodos">
                        <div class="radio">
                            <button ng-click="setInputMethod(x)" ng-class="{'btn-info':inputMethodId==x.id}" class="btn btn-block btn-outline btn-xs">
                                <i class="fa fa-arrow-down"></i> @{{x.nombre}}
                            </button>
                        </div>
                    </div>
                    <div ng-if="mitodos.length == 0">
                        <small>No se han configurado m&eacute;todos de ingreso</small>
                    </div>
                </div>
                <div class="setings-item admin">
                    <div class="sidebar-content">
                        <h4>{{ __('ui.admin')}}:</h4>
                        <div class="small">
                            {{ __('ui.inputMethodSelectInfo')}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 439.386px;">

        </div>
        <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.4; z-index: 90; right: 1px;">
        </div>
    </div>
</div>
</div>