@verbatim
<div class="wrapper wrapper-content animated fadeInRight">
<div class="row">
    <div class="col-lg-offset-1 col-lg-2">
        <div class="row m-b-lg m-t-lg" id="infoCliente" ng-show="cliente">
            <div>
                <h2 class="no-margins">
                    {{cliente.nombre}} {{cliente.apellido}}
                </h2>
                <p>
                    <a class="btn btn-xs btn-info" ng-click="goToClient(cliente)">
                        Ver cliente
                    </a>
                </p>
                <p>
                    <strong>{{cliente.cantidadCompras}}</strong> 
                    <span class="text-muted">Compras realizadas</span>
                </p> 
                <p>
                    <strong>{{cliente.cantidadCanjes}}</strong>
                    <span class="text-muted">
                        Canjes realizados
                    </span>
                </p>
                <p>
                    <strong>{{cliente.puntosCanjeados}}</strong> 
                    <span class="text-muted">Puntos canjeados</span>
                </p>

                <p>
                    <small>Puntos acumulados</small>
                    <h2 class="no-margins">{{cliente.puntos}}</h2>    
                </p>
            </div>
        </div>
    </div>
    <div class="col-lg-9">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Ingreso de operaci&oacute;n</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <div id="wizard">
                    <h1>Cliente</h1>
                    <div class="step-content">
                        <div class="tabs-container">

                            <div class="tabs-left operaciones">
                                <ul class="nav nav-tabs compracanje">
                                    <li style="list-style: none;" ng-click="getCleanInput()" class="active"><a data-toggle="tab" href="!#tab-1" aria-expanded="false" target="_self"> Tarjeta</a></li>
                                    <li style="list-style: none;" ng-click="getCleanInput()" class=""><a data-toggle="tab" href="!#tab-2" aria-expanded="true" target="_self">Otros par&aacute;metros</a></li>
                                </ul>
                                <div class="tab-content taboperacion">
                                    <div id="tab-1" class="tab-pane active">
                                        <div class="panel-body">
                                            <div card-input="inpCliente.idMagnetico" value-read="valueRead" manual-card-input="manualCardInput" search-callback="getCliente()"></div>
                                        </div>
                                    </div>
                                    <div id="tab-2" class="tab-pane" >
                                        <div class="panel-body">
                                            <label>Nombre </label>
                                            <div class="form-group">
                                                <input list="sugerenciascliente" ng-model="inpCliente.nombre" type="text" class="form-control" ng-change="getClienteSugerencias(inpCliente.nombre)"
                                                ng-model-options="{ debounce: 600 }">
                                                <datalist id="sugerenciascliente">
                                                    <option ng-repeat="cliente in clientesSugerencias" value="{{cliente.nombre}} {{cliente.apellido}}">
                                                    </datalist>
                                                </div>
                                                <label>DNI </label>
                                                <div class="form-group">
                                                    <input name="tarjeta" ng-model="inpCliente.dni" id="dniInpSearch" class="form-control" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button style="margin-top:10px" ng-click="getCliente()" class="btn btn-sm btn-primary pull-right m-t-n-xs siguienteSteps"><strong>Buscar</strong></button>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <h1>Datos</h1>
                        <div class="step-content">
                            <div class="tabs-container">

                                <div class="tabs-left operaciones">
                                    <ul class="nav nav-tabs compracanje">
                                        <li style="list-style: none;" ng-click="setOperationType('compra')" class="active"><a data-toggle="tab" href="!#tab-6" aria-expanded="false" target="_self"> Compra</a></li>
                                        <li style="list-style: none;" ng-click="setOperationType('canje')" class=""><a data-toggle="tab" href="!#tab-7" aria-expanded="true" target="_self">Canje</a></li>
                                    </ul>
                                    <div class="tab-content taboperacion">
                                        <div id="tab-6" class="tab-pane active">
                                            <div class="panel-body operacionbody">
                                                <table class="table table-hover" style="margin-bottom: 10px;">
                                                    <thead>
                                                        <tr>
                                                            <th width="50%">Descripción</th>
                                                            <th width="15%">Cantidad</th>
                                                            <th>Monto</th>
                                                            <th width="15%">Total</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr ng-repeat="item in purchaseItems">
                                                            <td>{{item.descripcion}}</td>
                                                            <td>{{item.cantidad}}</td>
                                                            <td>{{item.monto}}</td>
                                                            <td>{{item.cantidad * item.monto}} </td>
                                                            <td>
                                                                <button ng-click="deletePurchaseItem($index)" class="btn btn-xs btn-warning" style="margin-bottom:0;">
                                                                    <i class="fa fa-times"></i>
                                                                </button>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <input list="descripciones" autofocus placeholder="Descripción" ng-model="itemCompra.descripcion" name="descripcion" ng-class="{'has-error':purchaseItemError.descripcion}" id="descripcionInp" type="text" class="form-control"
                                                                ng-model-options="{ debounce: 400 }" ng-change="getDescripciones(itemCompra.descripcion)">
                                                                <datalist id="descripciones">
                                                                    <option ng-repeat="descripcion in descripciones" data-monto="{{descripcion.monto}}" value="{{descripcion.descripcion}}">
                                                                    </datalist>
                                                                </td>
                                                                <td>
                                                                    <input ng-class="{'has-error':purchaseItemError.cantidad}" ng-model="itemCompra.cantidad" name="cantidad" id="cantidadInp" type="number" min="1" step="1" class="form-control">
                                                                </td>
                                                                <td>
                                                                    <input ng-class="{'has-error':purchaseItemError.monto}" placeholder="Monto" ng-model="itemCompra.monto" name="importe" ng-enter="addPurchaseItem()" id="importeInp" type="text" class="form-control">
                                                                </td>
                                                                <td>{{itemCompra.cantidad * itemCompra.monto | number}} </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <div class="text-center">
                                                        <button tabindex="0"  ng-click="addPurchaseItem()" class="btn btn-info btn-xs" type="submit"><i class="fa fa-plus"></i> Agregar</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="tab-7" class="tab-pane" >
                                                <div class="panel-body">
                                                 <div class="text-center">
                                                    <button type="button" class="btn btn-primary btn-xs mt-4" style="margin-top:20px;margin-bottom:30px;" data-toggle="modal" ng-click="showSelectCanje()">
                                                        <i class="fa fa-plus"></i>
                                                        Seleccionar art&iacute;culos
                                                    </button>

                                                    <div class="ibox float-e-margins" ng-show="swapItems.length!=0">
                                                        <div class="ibox-title">
                                                            <h5>Art&iacute;culos seleccionados
                                                                <table class="table table-hover" style="margin-bottom: 10px;">
                                                                    <thead>
                                                                        <tr>
                                                                            <th width="70%">Nombre</th>
                                                                            <th>Puntos</th>
                                                                            <th>Cantidad</th>
                                                                            <th width="10%"></th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr ng-repeat="item in swapItems">
                                                                            <td>{{item.nombre}}</td>
                                                                            <td>{{item.puntos}}</td>
                                                                            <td>
                                                                                <input  ng-model="item.cantidad" name="cantidad" type="number" min="1" step="1" class="form-control">
                                                                            </td>
                                                                            <td>
                                                                                <button ng-click="deleteSwapItem($index)" class="btn btn-xs btn-warning" style="margin-bottom:0;">
                                                                                    <i class="fa fa-times"></i>
                                                                                </button>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <button style="margin-top:10px" ng-disabled="doingCompra" class="btn btn-sm btn-primary pull-right m-t-n-xs" ng-click="setOperacion()"><strong>Siguiente</strong></button>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <h1>Recibo</h1>
                            <div class="step-content">
                                <div class="text-center m-t-md">
                                    <h2>Operaci&oacute;n ingresada</h2>
                                    <p>
                                        Se gener&oacute; el recibo correspondiente.
                                    </p>
                                    <button ng-click="verComprobante()" class="btn btn-primary"><i class="fa fa-print"></i> Ver comprobante </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">

        toastr.options = 
        {
          "progressBar": false,
          "preventDuplicates": true,
          "onclick": null,
          "showDuration": "400",
          "hideDuration": "1000",
          "timeOut": "7000",
          "extendedTimeOut": "1000",
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
      }

      $(document).ready(function()
      {
        var settings = {
            enablePagination:false,
            labels: {
                current: "Paso actual",
                pagination: "Paginación",
                finish: "Finalizar",
                next: "Siguiente",
                previous: "Atrás",
                loading: "Cargando ...",
                cancel: "Cancelar",
            }
        };
        $("#wizard").steps(settings);
        $('ul[role="tablist"]').hide();
    });
</script>
@endverbatim
