 <div style="padding-top: 10px;" class="wrapper border-bottom white-bg page-heading">
    <h2>{{ __('ui.branchOffices')}}</h2>
    <ol class="breadcrumb">
        <li>
            <a href="#!/sucursales">{{ __('ui.branchOffices')}}</a>
        </li>
        <li class="active">
            <strong>{{ __('ui.creation')}}</strong>
        </li>
    </ol>
</div>
<div class="wrapper wrapper-content">
    <div class="row animated fadeInRight">
        <div class="col-sm-6 col-sm-offset-3 b-r">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>{{ __('ui.dataFrom')}} {{ __('ui.branchOffice')}}</h5>
                </div>
                <div class="ibox-content profile-content">
                    <div class="form-group">
                        <div class="form-group">
                            <label>{{ __('ui.name')}}</label> 
                            <input type="text" class="form-control" ng-model="sucursal.nombre">
                        </div>
                        <div location-suggestion suggestions="locationSuggestions" location-model="sucursal.ciudad_id" model-change="suggestLocation"></div>
                         <div class="form-group">
                            <label>{{ __('ui.street')}}</label> 
                            <input type="text" class="form-control" ng-model="sucursal.calle">
                        </div>
                        <div class="form-group">
                            <label>{{ __('ui.streetNumber')}}</label> 
                            <input type="text" class="form-control" ng-model="sucursal.altura">
                        </div>
                        <div class="form-group">
                            <label>{{ __('ui.extra')}}</label> 
                            <input type="text" class="form-control" ng-model="sucursal.extra">
                        </div>
                    </div>
                    <div class="user-button">
                        <div class="text-center">
                            <button class="btn btn-primary" type="button" ng-click="alta()" ng-disabled="saving==true">
                                <i class="fa fa-save" >
                                </i>&nbsp;&nbsp;<span class="bold">Guardar</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
