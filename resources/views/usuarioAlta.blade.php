 <div style="padding-top: 10px;" class="wrapper border-bottom white-bg page-heading">
    <h2>Usuarios</h2>
    <ol class="breadcrumb">
        <li>
            <a href="#!/usuarios">{{ __('ui.users')}}</a>
        </li>
        <li class="active">
            <strong>{{ __('ui.creation')}}</strong>
        </li>
    </ol>


</div>
<div class="wrapper wrapper-content">
    <div class="row animated fadeInRight">
        <div class="col-md-offset-3 col-sm-6 b-r">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>{{ __('ui.dataFrom')}} {{ __('ui.user')}}</h5>
                </div>
                <div>
                    <div class="ibox-content profile-content">
                        <div class="form-group">
                            <div class="form-group">
                                <label>{{ __('ui.nickname')}}</label> 
                                <input type="text" class="form-control" ng-model="usuario.alias">
                            </div>
                            <div class="form-group">
                                <label>{{ __('ui.password')}}</label> 
                                <input type="text" class="form-control" ng-model="usuario.password">
                            </div>
                            <div class="form-group">
                                <label>{{ __('ui.name')}}</label> 
                                <input type="text" class="form-control" ng-model="usuario.nombre">
                            </div>
                            <div class="form-group">
                                <label>{{ __('ui.surname')}}</label> 
                                <input type="text" class="form-control" ng-model="usuario.apellido">
                            </div>
                            <div class="form-group">
                                <label>{{ __('ui.mail')}}</label> 
                                <input type="email" class="form-control" ng-model="usuario.mail">
                            </div>
                            <div class="form-group">
                                <label>{{ __('ui.phone')}}</label> 
                                <input type="text" class="form-control" ng-model="usuario.telefono">
                            </div>
                            <div class="form-group">
                                <label>{{ __('ui.branchOffice')}}</label> 
                                {!! Helpers::createSelectFromArray($branchOffices,"usuario.sucursal_id","usuario.sucursal_id","id","nombre","onReady()") !!}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>{{ __('ui.dataFrom')}} {{ __('ui.configuration')}}</h5>
                </div>
                <div>
                    <div class="ibox-content profile-content">
                        <div class="form-group">
                            <label>{{ __('ui.inputMethod')}}</label> 
                            {!! Helpers::createSelect($inputMethods,"usuario.inputMethodId","usuario.inputMethodId","onReady()") !!}
                        </div>
                          <div class="form-group">
                            <label>{{ __('ui.userType')}}</label> 
                            {!! Helpers::createSelect($userTypes,"usuario.tipoId","2","onReady()") !!}
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-offset-4 col-md-4">
                                    <button class="btn btn-primary btn-block" type="button" ng-click="alta()" ng-disabled="saving==true">
                                        <i class="fa fa-save" ></i>&nbsp;&nbsp;
                                        <span class="bold">Guardar
                                        </span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>