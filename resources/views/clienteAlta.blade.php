 <div id="cliente-alta" ng-controller="clienteAlta">
    <div style="padding-top: 10px;" class="wrapper border-bottom white-bg page-heading">
        <h2>{{ __('ui.clients')}}</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#!/clientes">{{ __('ui.clients')}}</a>
            </li>
            <li class="active">
                <strong>{{ __('ui.creation')}}</strong>
            </li>
        </ol>
    </div>
    <div class="wrapper wrapper-content">
        <div class="row animated fadeInRight">
            <div class="col-md-offset-3 col-md-6 b-r">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>{{ __('ui.clientData')}}</h5>
                    </div>
                    <div>
                        <div class="ibox-content profile-content">
                            <div class="form-group">
                                <label>{{ __('ui.name')}}</label> 
                                <input type="text" class="form-control" ng-model="cliente.nombre">
                            </div>
                            <div class="form-group">
                                <label>{{ __('ui.surname')}}</label> 
                                <input type="text" class="form-control" ng-model="cliente.apellido">
                            </div>
                            <div class="form-group">
                                <label>{{ __('ui.mail')}}</label> 
                                <input type="email" class="form-control" ng-model="cliente.mail">
                            </div>
                            <div class="form-group">
                                <label>{{ __('ui.phone')}}</label> 
                                <input type="text" class="form-control" ng-model="cliente.telefono">
                            </div>
                            <div class="form-group col-sm-8" style="padding-left: 0">
                                <label>{{ __('ui.street')}}</label> 
                                <input type="text" class="form-control" ng-model="cliente.calle">
                            </div>
                            <div class="form-group col-sm-4"  style="padding-right: 0;">
                                <label>{{ __('ui.streetNumber')}}</label> 
                                <input type="text" class="form-control" ng-model="cliente.altura">
                            </div>
                            <div class="form-group col-md-4"  style="padding-left: 0">
                                <label>{{ __('ui.floor')}}</label> 
                                <input type="text" class="form-control" ng-model="cliente.piso">
                            </div>
                            <div class="form-group col-md-4">
                                <label>{{ __('ui.dept')}}</label> 
                                <input type="text" class="form-control" ng-model="cliente.depto">
                            </div>
                            <div class="form-group col-md-4"  style="padding-left: 0">
                                <label>{{ __('ui.postalCode')}}</label> 
                                <input type="text" class="form-control" ng-model="cliente.codigoPostal">
                            </div>
                            <div location-suggestion suggestions="locationSuggestions" location-model="cliente.ciudad_id" model-change="suggestLocation"></div>
                            <div class="form-group">
                                <label>{{ __('ui.gender')}}</label> 
                                {!! Helpers::createSelect($genders,"cliente.genero","cliente.genero","onReady()") !!}</p>
                            </div>
                            <div class="form-group">
                                <label>{{ __('ui.civilState')}}</label> 
                            {!! Helpers::createSelect($civilStates,"cliente.estadoCivil","cliente.estadoCivil","onReady()") !!}
                            </div>
                            <div class="form-group">
                                <label>{{ __('ui.birthDate')}}</label> 
                                <input type="date" class="form-control" ng-model="cliente.nacimiento">
                            </div>
                                <div class="form-group">
                                <label>{{ __('ui.documentNb')}}</label> 
                                <input type="text" class="form-control" ng-model="cliente.dni">
                            </div>
                            <br>
                            <h5>{{ __('ui.cardAssignment')}}</h5>
                            <div class="form-group">
                                <div class="text-center m-t-md"id="manual">
                                    <div class="form-group">
                                        <div>
                                           <div card-input="cliente.idmagtarjeta" value-read="cliente.idmagtarjeta" manual-card-input="manualCardInput"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                             <div class="row">
                                <div class="col-md-offset-4 col-md-4">
                                    <button class="btn btn-primary btn-block" type="button" ng-click="alta()" ng-disabled="saving==true">
                                        <i class="fa fa-save" ></i>&nbsp;&nbsp;

                                        <span class="bold">Guardar</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>