    <footer class="page-footer">
      <div class="container">
        <div class="row">
          <div class="col l6 s12">
            <h5 class="white-text">{{$instancia->nombreComercio}}</h5>
            <p class="grey-text text-lighten-4">{{$instancia->telefono}}</p>
            <p class="grey-text text-lighten-4">{{$instancia->direccionLinea1}}<br>{{$instancia->direccionLinea2}}</p>
          </div>
          <div class="col l4 offset-l2 s12">
            <p class="white-text"><small>Contacto</small></p>
            <div class="collection">
                <a class="collection-item" href="{{$instancia->fullFb}}">
                    <img width="20px" style="vertical-align: middle;" src="{{URL::asset('/img/facebook.png')}}"> 
                    FACEBOOK
                  </a>
                <a class="collection-item" href="{{$instancia->fullIg}}">
                    <img width="20px" style="vertical-align: middle;" src="{{URL::asset('/img/ig.png')}}"> 
                    INSTAGRAM
                  </a>
                <a class="collection-item" href="{{$instancia->FULLTW}}">
                    <img width="20px" style="vertical-align: middle;" src="{{URL::asset('/img/twitter.png')}}"> 
                    TWITTER
                  </a>
              </div>
          </div>
        </div>
      </div>
      <div class="footer-copyright">
        <div class="container">
        © {{date('Y')}}
        <a class="grey-text text-lighten-4 right" href="#!"></a>
        </div>
      </div>
    </footer>