 <div style="padding-top: 10px;" class="wrapper border-bottom white-bg page-heading">
  
  <h2>{{ __('ui.clients')}}</h2>
  <ol class="breadcrumb">
    <li>
      <a href="#!/clientes">{{ __('ui.clients')}}</a>
    </li>
    <li class="active">
      <strong>{{ __('ui.edit')}}</strong>
    </li>
  </ol>
</div>
<div class="wrapper wrapper-content">
  <div class="row animated fadeInRight">
    <div class="col-md-7">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5>{{ __('ui.personalInformation')}}</h5>
        </div>
        <div>
          <div class="ibox-content profile-content">
            <h4><strong>@{{cliente.nombre}} @{{cliente.apellido}}</strong></h4>
            <p><i class="fa fa-map-marker"></i>&nbsp;&nbsp;
              <strong>{{ __('ui.street')}}</strong>
              <editablespan 
              model="cliente.calle" 
              on-ready="onReady(item)" 
              span-class="info" 
              input-class="info-input">
            </editablespan></p>
            <div class="form-group row m-t-lg" style="margin-top: 0;">
              <div class="col-sm-4">
                <p><i class="fa fa-compass"></i>&nbsp;&nbsp;
                  <strong>{{ __('ui.streetNumber')}}</strong>
                  <editablespan 
                  model="cliente.altura" 
                  on-ready="onReady(item)" 
                  span-class="info" 
                  input-class="info-input">
                </editablespan></p>
              </div>
              <div class="col-sm-4">
                <p><i class="fa fa-building"></i>&nbsp;&nbsp;
                  <strong>{{ __('ui.floor')}}</strong>
                  <editablespan 
                  model="cliente.piso" 
                  on-ready="onReady(item)" 
                  span-class="info" 
                  input-class="info-input">
                </editablespan></p>
              </div>
              <div class="col-sm-4">
                <p><i class="fa fa-bell"></i>&nbsp;&nbsp;
                  <strong>{{ __('ui.dept')}}</strong>
                  <editablespan 
                  model="cliente.depto" 
                  on-ready="onReady(item)" 
                  span-class="info" 
                  input-class="info-input">
                </editablespan></p>
              </div>
            </div>
            <div class="form-group"><i class="fa fa-flag"></i>&nbsp;&nbsp;
              @{{cliente.ubicacion}}
              <span class="pull-right">
                <a ng-click="changeUbication=true">
                  <i class="fa fa-pencil"></i> Editar
                </a>
              </span>
              <div ng-show="changeUbication" class="form-group">
                <div location-suggestion suggestions="locationSuggestions" location-model="cliente.ciudad_id" model-change="suggestLocation"></div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-6">
                <p><i class="fa fa-user"></i>&nbsp;&nbsp;
                  <strong>{{ __('ui.gender')}}</strong>
                  {!! Helpers::createSelect($genders,"cliente.genero","cliente.genero","onReady()") !!}</p>
                </div>
                <div class="col-sm-6">
                  <p><i class="fa fa-heart"></i>&nbsp;&nbsp;
                    <strong>Estado Civil</strong>
                    {!! Helpers::createSelect($civilStates,"cliente.estadoCivil","cliente.estadoCivil","onReady()") !!}
                  </p>
                </div>
              </div>
              <p><i class="fa fa-at"></i>&nbsp;
                <strong>{{ __('ui.mail')}}</strong> 
                <editablespan 
                model="cliente.mail" 
                on-ready="onReady(item)" 
                span-class="info" 
                input-class="info-input">
              </editablespan></p>
              <p><i class="fa fa-credit-card"></i>&nbsp;
                <strong>{{ __('ui.card')}}</strong> 
                <div card-input="cliente.idmagtarjeta" value-read="cliente.idmagtarjeta" manual-card-input="manualCardInput"></div>
              </p>
              <div class="row" style="margin-top:1em;">
                <div class="col-sm-4">
                  <p><i class="fa fa-phone"></i>&nbsp;
                    <strong>{{ __('ui.phone')}}</strong> 
                    <editablespan 
                    model="cliente.telefono" 
                    on-ready="onReady(item)" 
                    span-class="info" 
                    input-class="info-input">
                  </editablespan></p>
                </div>
                <div class="col-sm-4">
                  <p style="margin-bottom:0;"><i class="fa fa-birthday-cake"></i>&nbsp;
                    <strong>{{ __('ui.birthDate')}}</strong> </p>
                    <div class="form-group">
                      <input style="padding: 0;height: 25px;" class="form-control" type="date" value="@{{cliente.nacimiento | asDate | date:'yyyy-MM-dd'}}" ng-model="cliente.nacimiento" name="">  
                    </div>
                  </editablespan>
                </div>
                <div class="col-sm-4">
                  <p><i class="fa fa-cube"></i>&nbsp;
                    <strong>{{ __('ui.documentNb')}}</strong> 
                    <editablespan 
                    model="cliente.dni" 
                    on-ready="onReady(item)" 
                    span-class="info" 
                    input-class="info-input">
                  </editablespan></p>
                </div>
              </div>
                <p><i class="fa fa-calendar-o"></i>&nbsp;
                  <strong>{{ __('ui.created')}}</strong>
                  <br />@{{cliente.created_at | asDate | date:'dd/MM/yyyy'}} </p>
                  <div class="widget style1">
                    <div class="row">
                      <div class="col-xs-4 text-center">
                        <i class="fa fa-star fa-5x"></i>
                      </div>
                      <div class="col-xs-8 text-right">
                        <span> {{ __('ui.points')}} </span>
                        <h2 class="font-bold">@{{cliente.puntos}}</h2>
                      </div>
                    </div>
                  </div>
                  <div class="row m-t-lg">
                    
                    <div class="col-md-4">
                      <h5><strong>@{{cliente.cantidadCompras}}</strong>&nbsp;&nbsp;{{ __('ui.boughtItems')}}</h5>
                    </div>
                    <div class="col-md-4">
                      <h5><strong>@{{cliente.cantidadCanjes}}</strong>&nbsp;&nbsp;{{ __('ui.swaps')}}</h5>
                    </div>
                    <div class="col-md-4">
                      <h5><strong>@{{cliente.puntosCanjeados}}</strong>&nbsp;&nbsp;{{ __('ui.swappedPoints')}}</h5>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-offset-4 col-md-4">
                      <div class="form-group">
                        <button class="btn btn-primary btn-block" type="button" ng-click="guardar()"><i class="fa fa-save" ></i>&nbsp;&nbsp;<span class="bold">{{ __('ui.save')}}</span>
                        </button>
                      </div>
                      <a ng-click="delete()" confirmation-needed class="btn btn-danger btn-block" type="button">
                        <i class="fa fa-times-circle" ></i>&nbsp;&nbsp;<span class="bold">{{ __('ui.delete')}}</span>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-5">
            <div class="ibox float-e-margins">
              
              <div class="ibox-title">  
                <div class="row">
                  <div class="col-md-5">
                    <h5>{{ __('ui.lastActivity')}}</h5>
                  </div>
                  <div class="col-md-7">
                    <div class="pull-right">
                      <button class="btn btn-xs btn-info" ng-click="newOperation()"><i class="fa fa-book"></i> Ingresar operación</button>
                      <button class="btn btn-xs btn-info" ng-show="admin" ng-click="goToTransactions()">Ver todo</button>
                    </div>
                  </div>
                </div>
              </div>
              <div class="ibox-content">
                <div>
                  <div class="feed-activity-list">
                    <li ng-hide="actividades.length">
                      {{ __('ui.noRecentActivity')}}
                    </li>
                    <div ng-repeat="actividad in actividades" class="feed-element">
                      <div class="media-body ">
                        <small class="text-navy">@{{actividad.idCompra?"Compra":"Canje"}}</small>
                        <span ng-if="actividad.deleted_at"> - 
                          <small class="text-danger">Anulada</small>
                        </span>
                        <span class="pull-right">@{{actividad.puntos}}</span> <br>
                        <small class="text-muted">@{{actividad.created_at | asDate | date:'dd/MM/yyyy H:mm:s'}}</small>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        