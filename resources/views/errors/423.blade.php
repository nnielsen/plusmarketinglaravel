@extends('errors.errorLayout')
@section('title','Instancia en pausa')
@section('subtitle','Se ha inhabilitado la instancia debido al cumplimiento del tiempo de prueba o por otros motivos')
@section('content')
	Todas las funcionalidades estan temporalmente fuera de servicio y no es posible acceder a la informacion.
	Contacte al soporte.
@endsection