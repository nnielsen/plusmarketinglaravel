 <div style="padding-top: 10px;" class="wrapper border-bottom white-bg page-heading">
    <h2>Usuarios</h2>
    <ol class="breadcrumb">
        <li>
            <a href="#!/usuarios">{{ __('ui.users')}}</a>
        </li>
        <li class="active">
            <strong>{{ __('ui.edit')}}</strong>
        </li>
    </ol>


</div>
<div class="wrapper wrapper-content">
    <div class="row animated fadeInRight">
        <div class="col-md-offset-3 col-sm-6">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Datos de @{{usuario.nombre}} @{{usuario.apellido}}</h5>
                </div>
                <div>
                    <div class="ibox-content profile-content">
                          <p><i class="fa fa-coffee"></i>&nbsp;
                            <strong>{{ __('ui.nickname')}}</strong> 
                            <editablespan 
                            model="usuario.alias" 
                            on-ready="onReady(item)" 
                            span-class="info" 
                            input-class="info-input"
                            input-type="text">
                            </editablespan></p>
                        <p><i class="fa fa-at"></i>&nbsp;
                            <strong>{{ __('ui.mail')}}</strong> 
                            <editablespan 
                            model="usuario.mail" 
                            on-ready="onReady(item)" 
                            span-class="info" 
                            input-class="info-input"
                            input-type="email">
                            </editablespan></p>
                        <p><i class="fa fa-phone"></i>&nbsp;
                            <strong>{{ __('ui.phone')}}</strong> 
                            <editablespan 
                            model="usuario.telefono" 
                            on-ready="onReady(item)" 
                            span-class="info" 
                            input-class="info-input">
                        </editablespan></p>
                        <p>
                            <i class="fa fa-calendar-o"></i>&nbsp;
                            <strong>{{ __('ui.created')}}</strong>
                            <br />@{{usuario.created_at}} 
                        </p>
                    </div>
                </div>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>{{ __('ui.dataFrom')}} {{__('ui.configuration')}}</h5>
                </div>
                <div>
                    <div class="ibox-content profile-content">
                        <div class="form-group">
                            <i class="fa fa-user"></i>
                            <label>{{ __('ui.userType')}}</label> 
                            {!! Helpers::createSelect($userTypes,"usuario.tipoId","usuario.tipoId","onReady()") !!}
                        </div>
                        <p>
                            <i class="fa fa-keyboard-o"></i>
                            <strong>{{ __('ui.inputMethod')}}</strong>
                            <br />@{{usuario.metodoIngreso}} 
                        </p>
                        <div class="form-group">
                                <label>{{ __('ui.branchOffice')}}</label> 
                                {!! Helpers::createSelectFromArray($branchOffices,"usuario.sucursal_id","usuario.sucursal_id","id","nombre","onReady()") !!}</p>
                            </div>
                    </div>
                </div>
            </div>
            <div class="ibox-content profile-content">
                <div class="row">
                    <div class="col-md-offset-2 col-md-8">
                        <div ng-show="cambio">
                            <button style="margin-bottom: 7px;" class="btn btn-primary btn-block" type="button" ng-click="guardar()"><i class="fa fa-save" ></i>&nbsp;&nbsp;<span class="bold">Guardar</span>
                            </button>
                        </div>
                        <a ng-href="#!/cambiarpass/@{{usuario.id}}" class="btn btn-warning btn-block" type="button"><i class="fa fa-times-circle" ></i>&nbsp;&nbsp;<span class="bold">Cambiar password</span></a>
                        <a ng-click="delete(usuario)" confirmation-needed class="btn btn-danger btn-block" type="button"><i class="fa fa-times-circle" ></i>&nbsp;&nbsp;<span class="bold">Eliminar</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>