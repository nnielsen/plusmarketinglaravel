@include('instanceManagerHeader')
    <body>
    	@yield('content')
    </body>
</html>
@include('instanceManagerFooter')
@include('instanceManagerScripts')
@yield('scripts')