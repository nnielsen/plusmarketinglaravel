<script type="text/javascript" src="{{ asset('js/jquery-2.1.1.js') }}"></script>
<script src="{{ asset('js/jqblock.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/materialize.min.js') }}"></script>
<script type="text/javascript">
  // Initialize collapsible (uncomment the lines below if you use the dropdown variation)
  // var collapsibleElem = document.querySelector('.collapsible');
  // var collapsibleInstance = M.Collapsible.init(collapsibleElem, options);

  // Or with jQuery

  $(document).ready(function(){
    $('.sidenav').sidenav();
    $('.collapsible').collapsible();
  });


  $('.dropdown-trigger').dropdown();
</script>
