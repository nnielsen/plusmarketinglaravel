<div ng-controller="dash">
<div class="row">
    <div class="col-md-offset-3 col-md-6 b-r text-center" 
     ng-init="showReferences=true" ng-show="showReferences==true">
        <h5 class="text-muted">
            <p>
                <i class="fa fa-user text-muted"></i>
                <span > @{{usuarioFullName}}</span>
            </p>
            <p>
            <i class="fa fa-users text-muted"></i>
            <span>Global</span>
            </p>
            <button class="text-info" ng-click="showReferences=false" style="margin-top:1em;">
                <i class="fa fa-times"></i>
                Ocultar
            </button>
        </h5>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div style="margin:1em;">
                <h5 class="m-b-xs">Compras</h5>
            </div>
        <div class="row m-t-xs">  
            <div class="col-xs-6">
              
                <h1 class="no-margins text-success">@{{dash.compra.usr.siempre.COUNT}} <small>
                <i class="fa fa-user text-success"></i></h1>
                    <span class="label label-success pull-right">Total</span>
                <div class="font-bold text-navy text-success">@{{dash.compra.global.siempre.COUNT}} 
                    <i class="fa fa-users text-success"></i></div>
            </div>
            <div class="col-xs-6">
                <h1 class="no-margins text-warning">@{{dash.compra.usr.lastMonth.COUNT}} <small>
                    <i class="fa fa-user text-warning"></i></small></h1>
                    <span class="label label-warning pull-right">Ultimo mes</span>
                <div class="font-bold text-navy text-warning">@{{dash.compra.global.lastMonth.COUNT}} 
                    <i class="fa fa-users text-warning"></i> </div>
            </div>
        </div>
        <table class="table small m-t-sm">
            <h4>Montos</h4>
            <tbody>
                <tr>
                   <td colspan="2" style="    padding-left: 0px;">
                        Promedio
                        <chartist class="ct-chart" chartist-data="chartsData.buysAvgs.data" chartist-chart-type="Bar" chartist-events="chartsData.buysAvgs.events"></chartist>
                   </td> 
                </tr>
                <tr>
                    <td class="text-success">
                        <strong>@{{(dash.compra.global.siempre.AVG || 0) | number:2}}</strong> Prom

                    </td>
                    <td class="text-warning">
                        <strong>@{{(dash.compra.global.lastMonth.AVG ||0) | number:2}}</strong> Prom
                    </td>
                </tr>
                <tr>
                    <td class="text-success">
                        <strong>@{{dash.compra.global.siempre.MAX || 0}}</strong> Max
                    </td>
                    <td  class="text-warning">
                        <strong>@{{dash.compra.global.lastMonth.MAX || 0}}</strong> Max
                    </td>
                </tr>
                <tr>
                    <td class="text-success">
                        <strong>@{{dash.compra.global.siempre.SUM || 0}}</strong> Total
                    </td>
                    <td class="text-warning">
                        <strong>@{{dash.compra.global.lastMonth.SUM || 0}}</strong> Total
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
      <div class="col-sm-6">
       <div style="margin:1em;">
            <h5 class="m-b-xs">Canjes</h5>          
        </div>
        <div class="row m-t-xs">
           
            <div class="col-xs-6">
                <h1 class="no-margins text-success">@{{dash.canje.usr.siempre.COUNT || 0}} <small>
                    <i class="fa fa-user text-success"></i> </small></h1>
                    <span class="label label-success pull-right">Total</span>
                <div class="font-bold text-navy text-success">@{{dash.canje.global.siempre.COUNT || 0}} 
                    <i class="fa fa-users text-success"></i> </div>
            </div>
            <div class="col-xs-6">
                <h1 class="no-margins text-warning">@{{dash.canje.usr.lastMonth.COUNT || 0}} <small>
                    <i class="fa fa-user text-warning"></i></small></h1>
                    <span class="label label-warning pull-right">Ultimo mes</span>
                <div class="font-bold text-navy text-warning">@{{dash.canje.global.siempre.COUNT || 0}} 
                    <i class="fa fa-users text-warning"></i> </div>
            </div>
        </div>
        <table class="table small m-t-sm">
            <h4>Puntos</h4>
            <tbody>
                <tr>
                    <td colspan="2" style="    padding-left: 0px;">
                        Promedio
                       <chartist class="ct-chart" chartist-data="chartsData.swapsAvgs.data" chartist-chart-type="Bar" chartist-events="chartsData.swapsAvgs.events"></chartist> 
                    </td>
                </tr>
                <tr>
                    <td class="text-success">
                        <strong>@{{(dash.canje.usr.siempre.AVG || 0) | number:2}}</strong> Prom

                    </td>
                    <td class="text-warning">
                        <strong>@{{(dash.canje.usr.lastMonth.AVG || 0) | number:2}}</strong> Prom
                    </td>
                </tr>
                <tr>
                    <td class="text-success">
                        <strong>@{{dash.canje.usr.siempre.MAX || 0}}</strong> Max
                    </td>
                    <td class="text-warning">
                        <strong>@{{dash.canje.usr.lastMonth.MAX || 0}}</strong> Max
                    </td>
                </tr>
                <tr>
                    <td class="text-success">
                        <strong>@{{dash.canje.usr.siempre.SUM || 0}}</strong> Total
                    </td>
                    <td class="text-warning">
                        <strong>@{{dash.canje.usr.lastMonth.SUM || 0}}</strong> Total
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-sm-3">
        <div class="ibox float-e-margins">
             <div class="ibox-title">
                <span class="label label-success pull-right">Hoy</span>
                <h5>Puntos asignados</h5>
            </div>
            <div class="ibox-content">
                <h1 class="no-margins">@{{dash.compra.global.hoy || 0}}</h1>
                <small>Global</small>
            </div>
        </div>
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <span class="label label-primary pull-right">Global</span>
                <h5>Plataforma</h5>
            </div>
            <div class="ibox-content">
                <h1 class="no-margins">@{{dash.articulos.count || 0}}</h1>
                <small>Articulos</small>
                <h1 class="no-margins">@{{dash.clientes.count || 0}}</h1>
                <small>Clientes</small>
            </div>
        </div>
    </div>
    <div class="col-sm-5">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                Cantidad de compras
                <span class="label label-info pull-right">Comparativa</span>
            </div>
            <div class="ibox-content">
                <chartist class="ct-chart" chartist-data="chartsData.monthCompare.data" chartist-chart-type="Line" chartist-chart-options="chartsData.monthCompare.option"></chartist> 
            </div>
        </div>
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                TOP busquedas del catálogo
            </div>
            <div class="ibox-content text-center">
                <div class="row">
                    <div class="col-sm-offset-3 col-sm-6">
                        <h4>@{{dash.catalogoQueries[0].query}}</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        @{{dash.catalogoQueries[1].query}}<br>
                        @{{dash.catalogoQueries[2].query}}
                    </div>
                    <div class="col-sm-6">
                        @{{dash.catalogoQueries[3].query}}<br>
                        @{{dash.catalogoQueries[4].query}}
                    </div>
                </div>
            </div>
        </div>
    </div>
        <div class="col-sm-4">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                 <span class="text-warning">Ultimo mes</span>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-sm-6">
                        <h1 class="no-margins">@{{dash.canje.usr.siempre.SUM || 0}}</h1>
                        <small>Puntos usuario canjeados</small>
                        <br>
                        <h1 class="no-margins">@{{dash.canje.global.siempre.SUM || 0}}</h1>
                        <small>Puntos globales canjeados</small>
                    </div>
                    <div class="col-sm-6">
                        <h2>
                            @{{(((dash.canje.usr.siempre.COUNT / dash.canje.global.siempre.COUNT) * 100) || 0) | number:2}}%
                        </h2>
                        <chartist class="ct-chart" chartist-data="chartsData.userGlobal.data" chartist-chart-type="Pie"
                        chartist-chart-options="chartsData.userGlobal.options"></chartist>
                        <small>usuario/global</small>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>