 <div style="padding-top: 10px;" class="wrapper border-bottom white-bg page-heading">
    <h2>{{ __('ui.articles')}}</h2>
    <ol class="breadcrumb">
        <li>
            <a href="#!/articulos">{{ __('ui.articles')}}</a>
        </li>
        <li class="active">
            <strong>{{ __('ui.edit')}}</strong>
        </li>
    </ol>
</div>
<div class="wrapper wrapper-content">
    <div class="row animated fadeInRight">
        <div class="col-sm-6 b-r">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>{{ __('ui.dataFrom')}} @{{articulo.nombre}}</h5>
                </div>
                <div>
                    <div class="ibox-content profile-content">
                        <div categoria-item="articulo.categoriaTree" 
                        selected-id="articulo.categoriaId"></div>
                        <p><i class="fa fa-tag"></i>&nbsp;
                            <strong>{{ __('ui.name')}}</strong> 
                            <editablespan 
                            model="articulo.nombre" 
                            on-ready="onReady(item)" 
                            span-class="info" 
                            input-class="info-input"
                            input-type="email">
                        </editablespan></p>
                        <p><i class="fa fa-adjust"></i>&nbsp;
                            <strong>{{ __('ui.description')}}</strong> 
                            <textarea
                            ng-change="onReady(item)"
                            ng-model="articulo.descripcion"
                            class="form-control"
                            style="resize: vertical; width: 100%;" 
                            ></textarea> 
                        </editablespan></p>
                        <p><i class="fa fa-star"></i>&nbsp;
                            <strong>{{ __('ui.points')}}</strong> 
                            <editablespan 
                            model="articulo.puntos" 
                            on-ready="onReady(item)" 
                            span-class="info" 
                            input-class="info-input">
                        </editablespan></p>
                        <p><i class="fa fa-calculator"></i>&nbsp;
                            <strong>{{ __('ui.stock')}}</strong> 
                            <editablespan 
                            model="articulo.stock" 
                            on-ready="onReady(item)" 
                            span-class="info" 
                            input-class="info-input">
                        </editablespan></p>
                        <p><i class="fa fa-calendar-o"></i>&nbsp;
                            <strong>{{ __('ui.created')}}</strong>
                            <br />@{{articulo.created_at | asDate | date:'dd/MM/yyyy'}} </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 b-r">
              <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <div class="col-sm-6">
                        <h5>{{ __('ui.image')}}</h5>
                    </div>
                    <div style="position: relative;float: right;">
                        <form enctype="multipart/form-data">
                            <input type="file" name="file" id="file" class="inputfile" onchange="angular.element(this).scope().uploadFile(this.files)"/>
                            <input type="file" name="file" id="file" class="inputfile" onchange="angular.element(this).scope().uploadFile(this.files)"/>
                            <label id="inpLabel" for="file" class="btn btn-xs btn-primary">{{ __('ui.updateImage')}}</label><br>
                        </form>
                    </div>
                </div>
                <div>
                    <div class="ibox-content profile-content">
                        <div class="imgContainer text-center">
                            <img ng-controller="articuloEdit" ng-if="articulo.imgUrl && !uploading" id="imgArticulo" ng-src="@{{articulo.imgUrl}}" alt="IMAGEN" class="imgArticuloEdit">
                            <i ng-if="uploading" class="fa fa-gears fa-4x"></i>
                            <div  ng-hide="articulo.imgUrl != null"><h4>{{ __('ui.noImage')}}</h4></div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-sm-offset-4 col-sm-4">
            <div class="ibox-content profile-content">
                <div class="form-group">
                    <div class="user-button">
                        <div ng-show="cambio">
                            <button class="btn btn-primary btn-block" type="button" ng-click="guardar()"><i class="fa fa-save" ></i>&nbsp;&nbsp;<span class="bold">{{ __('ui.save')}}</span>
                            </button>
                        </div>         
                    </div>
                </div>
                <a ng-click="delete()" confirmation-needed class="btn btn-danger btn-block" type="button"><i class="fa fa-times-circle" ></i>&nbsp;&nbsp;
                    <span class="bold">{{ __('ui.delete')}}</span>
                </a>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function cambiarImagen(id)
        {      
            $("#imgArticulo").fadeOut(1000,function()
            {
                angular.element('#imgArticulo').scope().setImagen(id); 
                $("#imgArticulo").attr("src", 'downImg/'+id);
                $("#imgArticulo").fadeIn(1000);
                $("#artSinImg").hide();
            });
        }
    </script>