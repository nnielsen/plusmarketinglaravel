
<div  style="padding-top:10px" class="row wrapper border-bottom white-bg page-heading">
    <div class="col-md-10">
        <h2>{{ __('ui.articles')}}</h2>
    </div>
    <div class="col-md-2">
        <h2>
        <a class="btn btn-primary btn-circle" type="button" ng-href="#!/articulo/alta" href="#!/cliente/alta"><i class="fa fa-plus"></i>
        </a>
        <h2>
    </div>
</div>
<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="ibox">
        <div class="ibox-content">
            @include('table-header')
            <div class="clients-list">
                <ul class="nav nav-tabs">
                    <span class="pull-right small text-muted">@{{(todo | filter:searchString).length}} {{ __('ui.article')}}@{{(todo | filter:searchString).length>1?"s":""}}</span>
                </ul>
                <div class="results-for" ng-show="searchString" ng-click="clearSearch()">
                        <h4>
                            Resultados para: <span class="text-navy">@{{searchString}}<span class="bg-primary" style="border-radius: 100px;padding:4px;margin-left: 10px;"><i class="fa fa-times"></i></span></span>
                        </h4>
                    </div>
                <div class="tab-content">
                    <div id="tab-1" class="tab-pane active">
                        <div class="full-height-scroll">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover">
                                    <thead>
                                        <th>{{ __('ui.article')}}</th>
                                        <th>{{ __('ui.points')}}</th>
                                        <th>{{ __('ui.stock')}}</th>
                                        <th></th>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="articulo in todo | filter:searchString">
                                            <td width="70%">
                                                <a ng-click="edit(articulo)" class="pull-left">
                                                    <img ng-if="articulo.imgUrl" src="@{{articulo.thumbUrl}}" alt="IMAGEN" class="img-circle imgMin">
                                                    <img src="imgs/NA.png" style="opacity: 0.2;" ng-if="!articulo.imgUrl" class="img-circle imgMin">
                                                </a>
                                                <a data-toggle="tab" ng-click="edit(articulo)" class="client-link descripArt">
                                                @{{articulo.nombre}}</a>
                                            </td>
                                            <td width="10%">
                                                @{{articulo.puntos}}
                                            </td>
                                            <td>
                                                @{{articulo.stock}}
                                            </td>
                                            <td>
                                                <a class="btn btn-success btn-circle" type="button" ng-click="edit(articulo)"><i class="fa fa-pencil"></i>
                                                </a>
                                                <a class="btn btn-danger btn-circle" type="button" confirmation-needed ng-click="delete(articulo,$index)"><i class="fa fa-times-circle"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <button ng-show="lastPage > params.page" ng-click="showMore()" class="btn btn-block m-t">
                                    <i class="fa fa-arrow-down"></i> Ver más
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
