		<div style="padding: 0 25px; background: #fff; text-align: center;">
			<table class="contactate">       
				<tbody>                                     
					<tr>
						<td rowspan="2">
							<p><strong><span style="font-size: 12pt;">CONECTATE CON NOSOTROS:</span></strong></p>
							<table style="width: 100%;">
								<tbody>
									<tr style="height: 23px;background: #4266b2;margin-bottom: 21px;">
										<td style="text-align: center; height: 23px;">
											<strong>
											<a style="text-decoration:none;" href="{{$configInstancia->fullFb}}">
													<span style="font-size: 14pt;line-height: 41px;color: white;">Facebook
													</span>
												</a>
											</strong>
										</td>
									</tr>
									<tr style="height: 23px;background: #41a1f2;margin-bottom: 21px;">
										<td style="text-align: center; height: 23px;">
											<strong>
												<a href="{{$configInstancia->fullTw}}" style="text-decoration:none;">
													<span style="font-size: 14pt;line-height: 41px;color: white ;">Twitter</span>
												</a>
											</strong>
										</td>
									</tr>
									<tr style="height: 23px; background:#E91E63;">
										<td style="text-align: center; height: 23px;">
											<strong>
												<a href="{{$configInstancia->fullIg}}" style="text-decoration:none;">
													<span style="font-size: 14pt;line-height: 41px;color: white;">Instagram
													</span>
												</a>
											</strong>
										</td>
									</tr>
								</tbody>
							</table>
							<p>
								<strong>
									<span style="font-size: 8pt;">DIRECCION</span>
									<span style="font-size: 10pt;">
										{{$configInstancia->direccionLinea1}}<br>
										{{$configInstancia->direccionLinea2}}
									</span>
								</strong>
							</p>
							<p>
								<span style="font-size: 12pt;">
									<span style="font-size: 10pt;">
										<strong>Tel</strong>
									</span>
									<span style="font-size: 10pt;"> 
										<a href="tel:{{$configInstancia->telefono}}">
											{{$configInstancia->telefono}}
										</a>
									</span>
								</span>
							</p>
						</td>
					</tr>            
				</tbody>
			</table>
		</div>
	</td>
</tr>
<tr>
	<td>
		<table width="100%" style="border-collapse: collapse;" cellspacing="0" cellpadding="0" border="0">
			<tr>
				<td style="padding: 25px; text-align: center;" style="border-collapse: collapse;" cellspacing="0" cellpadding="0" border="0" valign="top"> 
					<p style="text-align: center;">
						<a href="{{$catalogoUrl}}" style="text-decoration:none;">
							<span style="font-size: 12pt; color: #999999;line-height: 30px;">Acced&eacute; al 
								<span style="    background-color: #6983f9;
								color: #ffffff;
								font-size: 12pt;
								border-radius: 4px;
								margin:5px;
								padding: 6px;">Cat&aacute;logo &gt;&gt;
							</span>
						</a> de nuestros productos y a disfrutar...
					</p>
				</td>
			</tr>
		</table>
	</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</body>
</html>