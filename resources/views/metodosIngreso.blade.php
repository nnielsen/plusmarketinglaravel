
<div  style="padding-top:10px" class="row wrapper border-bottom white-bg page-heading">
    <div class="col-md-10">

        <h2>{{ __('ui.inputMethods')}}</h2>
    </div>
    <div class="col-md-2">
        <h2>
        <a class="btn btn-primary btn-circle" type="button" href="#!/mi/alta"><i class="fa fa-plus"></i>
        </a>
        <h2>
    </div>
</div>

<div class="wrapper wrapper-content  animated fadeInRight">

    <div class="ibox">
        <div class="ibox-content">
            <div class="clients-list">
                <ul class="nav nav-tabs">
                    <span class="pull-right small text-muted">@{{todo.length}} {{ __('ui.method')}}@{{todo.length!=1?"s":""}}</span>
                </ul>
                <div class="tab-content">
                    <div id="tab-1" class="tab-pane active">
                        <div class="full-height-scroll">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover">
                                    <tbody>
                                        <thead>
                                            <td width="40%">{{ __('ui.name')}}</td>
                                            <td width="10%">{{ __('ui.start')}}</td>
                                            <td width="10%">{{ __('ui.end')}}</td>
                                            <td width="10%"></td>
                                        </thead>
                                        <tr ng-repeat="metodo in todo">
                                            <td><a ng-href="#!/mi/edit/@{{metodo.id}}" data-toggle="tab" class="client-link">@{{metodo.nombre}}</a></td>
                                            <td>@{{metodo.inicio}}</td>
                                            <td>@{{metodo.cierre}}</td>
                                             <td>
                                                <a class="btn btn-success btn-circle" type="button" ng-href="#!/mi/edit/@{{metodo.id}}"><i class="fa fa-pencil"></i>
                                                </a>
                                                <a class="btn btn-danger btn-circle" type="button" ng-click="delete(metodo)" confirmation-needed><i class="fa fa-times-circle"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
