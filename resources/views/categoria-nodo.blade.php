
<ul class="directory-list">
    <li class="folder" ng-class="{'selected':selected==parent}"  
    	ng-repeat="parent in categorias | filter:{parentId:parentId}:true">
		<span class="category-title" ng-click="setSelected(parent)"  ng-class="{'invert':selected==parent}">@{{parent.title}} </span>
		<a class="btn btn-xs" ng-class="{'invert':selected==parent}" 
		ng-click="parent.createChild = true"> + <small class="text-muted">Agregar subcategoría</small></a>
        <a class="btn btn-xs" ng-class="{'invert':selected==parent}" 
                                        confirmation-needed
                                        ng-if="manager"
                                        ng-click="deleteNode(parent)">
                                         <span class="text-warning"><i class="fa fa-times"></i></span>
                                        </a>
		<div ng-if="parent.createChild == true">
            <div class="input-group m-b" style="display:inline-flex;width:100%;">
            	<span class="input-group-prepend">
	                <button type="button" ng-click="createChild(parent,inpCategory)" ng-disabled="!inpCategory || inpCategory==''" class="btn btn-primary" style="border-radius:0;">+</button> 
	            </span> 
                <input type="text" placeholder="Nueva categoría" ng-model="inpCategory" class="form-control">
            </div>
        </div>
        <div ng-if="(categorias | filter:{parentId:parentId}:true).length > 0" ng-init="parentId = parent.id">
        	<div ng-include="'view/categoria-nodo'"></div>
        </div>
    </li>
</ul>