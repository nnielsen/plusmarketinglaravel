 <div style="padding-top: 10px;" class="wrapper border-bottom white-bg page-heading">
    <h2>{{ __('ui.articles')}}</h2>
    <ol class="breadcrumb">
        <li>
            <a href="#!/articulos">{{ __('ui.articles')}}</a>
        </li>
        <li class="active">
            <strong>{{ __('ui.creation')}}</strong>
        </li>
    </ol>
</div>
<div class="wrapper wrapper-content">
    <div class="row animated fadeInRight">
        <div class="col-sm-6 b-r">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>{{ __('ui.articleData')}}</h5>
                </div>
                <div class="ibox-content profile-content">
                    <div class="form-group">
                        <div class="form-group">
                            <label>{{ __('ui.name')}}</label> 
                            <input type="text" class="form-control" ng-model="articulo.nombre">
                        </div>
                        <div class="form-group">
                            <label>{{ __('ui.description')}}</label> 
                            <textarea class="form-control" ng-model="articulo.descripcion" style="resize: vertical; width: 100%;"></textarea>
                        </div>
                        <div class="row" style="margin:0">
                            <div class="form-group col-md-6" style="padding-left:0">
                                <label>{{ __('ui.points')}}</label> 
                                <input type="text" class="form-control" ng-model="articulo.puntos">
                            </div>
                            <div class="form-group col-md-6" style="padding-right:0px">
                                <label>{{ __('ui.stock')}}</label> 
                                <input type="text" class="form-control" ng-model="articulo.stock">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-6 b-r">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>{{ __('ui.image')}}</h5>
                </div>
                <div>
                    <div class="ibox-content profile-content">
                        <form enctype="multipart/form-data">
                            <input type="file" name="file" id="file" class="inputfile"/>
                            <input type="file" name="file" id="file" class="inputfile" onchange="angular.element(this).scope().uploadFile(this.files)"/>
                            <label id="inpLabel" for="file" class="btn btn-primary">{{ __('ui.uploadImage')}}</label><br>
                            <span id="upTexto"></span>
                        </form>
                    </div>
                    
                </div>
            </div>
          
        </div>
    </div>
    <div class="col-sm-offset-4 col-sm-4">
        <div class="ibox-content profile-content">
            <div class="user-button">
                <button class="btn btn-primary btn-block" type="button" ng-click="alta()" ng-disabled="saving==true">
                    <i class="fa fa-save"></i>&nbsp;&nbsp;
                    <span class="bold">{{ __('ui.save')}}</span>
                </button>
            </div>
        </div>
    </div>
   
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#file').on("change", function(){ $("#upTexto").html('Imagen Seleccionada'); });
    });

</script>
