<div class="middle-box text-center animated fadeInDown">
        <h3>Cambio de password</h3>
        <h5 class="font-bold">@{{usuario.nombre}} @{{usuario.apellido}}</h5>

        <div class="form-group has-feedback" ng-class="{'has-error':inpPassword.password !=passwordRepeater && blurRepeater==true,'has-success':inpPassword.password == passwordRepeater && passwordRepeater != ''}">
        	<input type="password" ng-model="inpPassword.password" placeholder="Nueva password" class="form-control">
        </div>
        <div class="form-group has-feedback" ng-class="{'has-error':inpPassword.password !=passwordRepeater && blurRepeater==true,'has-success':inpPassword.password == passwordRepeater && passwordRepeater != ''}">
        	<input type="password" ng-model="passwordRepeater" ng-init="blurRepeater=false" ng-blur="blurRepeater=true" placeholder="Confirme nueva password" class="form-control">
        </div>
        <div class="text-danger" ng-show="inpPassword.password != passwordRepeater && blurRepeater==true">
        	Las contraseñas deben coincidir
        </div>
           <a ng-click="cambiar()" class="btn btn-primary m-t">Aceptar</a>
        </div>
</div>
