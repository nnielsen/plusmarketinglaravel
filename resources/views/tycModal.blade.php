<div class="modal inmodal in" id="modalTerminos" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" style="z-index: 2051;">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Cerrar</span></button>
                    <h4 class="modal-title">{{ __('ui.termsAndConditions')}}</h4>
                    <small class="font-bold">{{ __('ui.termsAndConditions')}}</small>
                </div>
                <div class="modal-body">
                    <div class="ibox">
                        <div class="ibox-content">
                            @{{tyc}}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" data-dismiss="modal" ng-click="aceptarTermCond()">{{ __('ui.accept')}} {{ __('ui.termsAndConditions')}}</button>
                    <button type="button" class="btn btn-white" data-dismiss="modal">Despu&eacute;s</button>
                </div>
            </div>
        </div>
    </div>
    <button type="button" id="btnModalTerminos" class="btn btn-primary" data-toggle="modal" data-target="#modalTerminos" style="display: none;"></button>