
<div  style="padding-top:10px" class="row wrapper border-bottom white-bg page-heading">
    <div class="col-md-10">
        <h2>{{ __('ui.branchOffices')}}</h2>
    </div>
    <div class="col-md-2">
        <h2>
        <a class="btn btn-primary btn-circle" type="button" href="#!/sucursal/alta"><i class="fa fa-plus"></i>
        </a>
        <h2>
    </div>
</div>
<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="ibox">
        <div class="ibox-content">
            @include('table-header')
            <div class="clients-list">
                <ul class="nav nav-tabs">
                    <span class="pull-right small text-muted">@{{(todo | filter:searchString).length}} {{ __('ui.branchOffice')}}@{{(todo | filter:searchString).length>1?"es":""}}</span>
                </ul>
                <div class="results-for" ng-show="searchString" ng-click="clearSearch()">
                        <h4>
                            Resultados para: <span class="text-navy">@{{searchString}}<span class="bg-primary" style="border-radius: 100px;padding:4px;margin-left: 10px;"><i class="fa fa-times"></i></span></span>
                        </h4>
                    </div>
                <div class="tab-content">
                    <div id="tab-1" class="tab-pane active">
                        <div class="full-height-scroll">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover">
                                    <thead>
                                        <th>{{ __('ui.name')}}</th>
                                        <th>{{ __('ui.city')}}</th>
                                        <th></th>
                                    </thead>
                                    <tbody ng-repeat="sucursal in todo | filter:searchString" ng-init="sucursal.edit=false">
                                        <tr >
                                            <td width="50%">
                                                <div ng-if="!sucursal.edit">
                                                    @{{sucursal.nombre}}
                                                </div>
                                                <div ng-if="sucursal.edit == true">
                                                    <editablespan 
                                                        model="sucursal.nombre" 
                                                        on-ready="onReady(item)" 
                                                        span-class="info" 
                                                        input-class="info-input">
                                                    </editablespan>
                                                </div>
                                            </td>
                                            <td>
                                                <div ng-if="!sucursal.edit">
                                                    @{{sucursal.ubicacion}}
                                                </div>
                                                <div ng-if="sucursal.edit">
                                                     <div location-suggestion suggestions="locationSuggestions" location-model="sucursal.ciudad_id" model-change="suggestLocation"></div>
                                                </div>
                                            </td>
                                            <td>
                                                <a class="btn btn-success btn-circle" type="button" ng-click="edit(sucursal)" ng-if="sucursal.edit==false">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                                <a class="btn btn-warning btn-circle" type="button" ng-click="save(sucursal)" ng-if="sucursal.edit==true"><i class="fa fa-save"></i>
                                                </a>
                                                <a class="btn btn-danger btn-circle" type="button" confirmation-needed ng-click="delete(sucursal,$index)">
                                                    <i class="fa fa-times-circle"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr ng-if="sucursal.edit==true">
                                            <td colspan="3">
                                                <div class="row">
                                                    <div class="col-md-offset-4 col-md-2">
                                                        <div class="form-group">
                                                            <label>{{__('ui.street')}}</label>
                                                            <editablespan 
                                                            model="sucursal.calle" 
                                                            on-ready="onReady(item)" 
                                                            span-class="info" 
                                                            input-class="info-input">
                                                            </editablespan>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>{{__('ui.streetNumber')}}</label>
                                                            <editablespan 
                                                            model="sucursal.altura" 
                                                            on-ready="onReady(item)" 
                                                            span-class="info" 
                                                            input-class="info-input">
                                                            </editablespan>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label>{{__('ui.extra')}}</label>
                                                            <editablespan 
                                                            model="sucursal.extra" 
                                                            on-ready="onReady(item)" 
                                                            span-class="info" 
                                                            input-class="info-input">
                                                            </editablespan>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <button ng-show="lastPage > params.page" ng-click="showMore()" class="btn btn-block m-t">
                                    <i class="fa fa-arrow-down"></i> Ver más
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
