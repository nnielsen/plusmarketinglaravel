<nav class="navbar-default navbar-static-side" role="navigation"  style="position: fixed;">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element"> <span>
                    <a href="#!/main">
                        <img src="imgs/logo?u={{uniqid()}}" style="margin-top: 5px;max-width: 173px; max-height: 53px;" id="navbar-logo">
                    </a>
                </span>
                <a data-toggle="dropdown" class="dropdown-toggle">
                    <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">@{{usuarioFullName}}</strong>
                    </span> <span class="text-muted text-xs block">@{{comercioName}} <b class="caret"></b></span> </span> </a>
                    <ul class="dropdown-menu animated m-t-xs">
                        <li><a href="#!/mi">{{ __('ui.inputMethods')}}</a></li>
                        <li><a href="#!/sucursales" ng-show="admin">{{ __('ui.branchOffices')}}</a></li>
                        <li class="divider" ng-show="admin"></li>
                        <li ng-show="admin"><a href="#!/usuarios">{{ __('ui.users')}}</a></li>
                        <li ng-show="admin"><a href="#!/config">{{ __('ui.configuration')}}</a></li>
                        <li class="divider"></li>
                        <li><a href="#!/contacto">{{ __('ui.contactAndSupport')}}</a></li>
                        <li><a ng-click="logout()">{{ __('ui.logout')}}</a></li>
                    </ul>
                </div>
                <div class="logo-element">
                    <a href="#!/main"> PM </a>
                </div>
            </li>
            <li>
                <a href="index.html">
                    <div class="icon-wrapper">
                        <i class="fa fa-briefcase"></i> 
                    </div>
                    <span class="nav-label">{{ __('ui.business')}}</span> 
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="#!/reporte">{{ __('ui.report')}}</a></li>
                    <li><a href="#!/ranking">{{ __('ui.ranking')}}</a></li>
                </ul>
            </li>
            <li>
                <a href="#!/clientes">
                    <div class="icon-wrapper">
                        <i class="fa fa-users"></i> 
                    </div>
                    <span class="nav-label">{{ __('ui.clients')}}</span>
                </a>
            </li>
            <li>
                <a href="#!/operacion">
                    <div class="icon-wrapper">
                        <i class="fa fa-credit-card"></i> 
                    </div>
                    <span class="nav-label">{{ __('ui.operation')}}</span>
                </a>
            </li>
            <li>
                <a href="#!/articulos">
                    <div class="icon-wrapper">
                        <i class="fa fa-gift"></i> 
                    </div>
                    <span class="nav-label">{{ __('ui.articles')}}</span>
                </a>
            </li>
            <li>
                <a href="catalogo" target="_blank">
                    <div class="icon-wrapper">
                        <i class="fa fa-trello"></i> 
                    </div>
                    <span class="nav-label">{{ __('ui.catalog')}}</span>
                </a>
            </li>
        </ul>
    </div>
</nav>
<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary ">
                <i class="fa fa-bars"></i> 
            </a>
        </div>
        <ul class="nav navbar-top-links navbar-right">
            <li>
                <a ng-click="logout()">
                    <i class="fa fa-sign-out"></i> {{ __('ui.logout')}}
                </a>
            </li>
            <li>
                <a class="right-sidebar-toggle">
                    <i class="fa fa-gear"></i>
                </a>
            </li>
        </ul>
    </nav>
</div>