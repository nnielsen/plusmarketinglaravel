<div style="padding-top:10px" class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-6">
        <h2>Ranking de puntos actuales por cliente</h2>
    </div>
    <div class="col-sm-6">
          <div class="pull-right">
                Exportar
                <div class="btn-group">
                    <button ng-click="export('csv')" class="btn btn-white" type="button">CSV</button>
                    <button ng-click="export('html')" class="btn btn-white" type="button">HTML</button>
                    <button ng-click="export('xls')" class="btn btn-white" type="button">XLS</button>
                    <button ng-click="export('xlsx')" class="btn btn-white" type="button">XLSX</button>
                </div>
            </div>
    </div>
</div>
<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="ibox">
        <div class="ibox-content">
            <div class="clients-list">
                <div class="tab-content">
                    <div id="tab-1" class="tab-pane active">
                        <div class="full-height-scroll">
                            <div class="table-responsive">
                                <table ng-show="todo.length > 0" class="table table-striped table-hover">
                                        <thead>
                                            <td><strong>Nombre cliente</strong></td>
                                            <td><strong>Puntos asignados</strong></td>
                                            <td width="10%"></td>
                                        </thead>
                                    <tbody ng-repeat="cliente in todo">
                                        <tr>
                                            <td> @{{cliente.nombre}} @{{cliente.apellido}}</td>
                                            <td> @{{cliente.puntos_totales}}</td>
                                            <td> 
                                                <a ng-init="cliente.expand=false" ng-click="cliente.expand=!cliente.expand"><small>Ver detalles</small>
                                                    <i class="fa fa-caret-square-o-down" ng-show="cliente.expand==false"></i>
                                                    <i class="fa fa-caret-square-o-up" ng-show="cliente.expand==true"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr ng-show="cliente.expand==true">
                                            <td colspan="3">
                                                <div class="ibox-content profile-content">
                                                    <div class="row">
                                                        <div class="col-sm-3">
                                                             <h4><strong>@{{cliente.mail}}</strong></h4> <button class="btn btn-info btn-xs" ng-click="goToClient(cliente)">Ver cliente</button>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <p><i class="fa fa-map-marker"></i> @{{cliente.calle}} @{{cliente.altura}}<br><span class="text-muted">@{{cliente.piso}} @{{cliente.depto}}</span>
                                                            <br><span class="text-muted">@{{cliente.codigoPostal}} @{{cliente.ciudad}} @{{cliente.pais}}</span></p>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <p><i class="fa fa-phone"></i> @{{cliente.telefono}}</p>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <p><i class="fa fa-calendar-o"></i> @{{cliente.created_at | asDate | date:'dd/MM/yyyy'}}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div ng-show="todo.length == 0" class="text-center">
                                    <h3 class="text-muted">Sin resultados</h3>
                                </div>
                                <button ng-show="lastPage > params.page" ng-click="showMore()" class="btn btn-block m-t">
                                    <i class="fa fa-arrow-down"></i> Ver más
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
