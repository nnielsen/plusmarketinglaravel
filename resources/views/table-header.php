<div class="row">
                <div class="col-md-6">
                    <div class="input-group">
                        <input type="text" placeholder="Nombre" ng-enter="search(inpSearch)" class="input form-control" data-ng-model="inpSearch">
                        <span class="input-group-btn">
                            <button type="button" ng-click="search(inpSearch)" class="btn btn btn-primary"> <i class="fa fa-search"></i> {{ __('ui.search')}}</button>
                        </span>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="pull-right">
                        Exportar
                        <div class="btn-group">
                            <button ng-click="export('csv')" class="btn btn-white" type="button">CSV</button>
                            <button ng-click="export('html')" class="btn btn-white" type="button">HTML</button>
                            <button ng-click="export('xls')" class="btn btn-white" type="button">XLS</button>
                            <button ng-click="export('xlsx')" class="btn btn-white" type="button">XLSX</button>
                        </div>
                    </div>
                </div>
            </div>