@extends('instanceManagerMainLayout')

@section('content')
  	<div class="container" style="min-height: 300px;">
  		<div class="row" style="margin-top: 3em;">
	  		<table>
	        <thead>
	          <tr>
	              <th width="30%">Nombre</th>
	              <th width="40%">Url</th>
	              <th width="20%">Estado</th>
	              <th>Acciones</th>
	          </tr>
	        </thead>
	        <tbody>
	        	@foreach($instances as $instance)
	          <tr>
	            <td>{{$instance->business_name}}</td>
	            <td>
	            	<a href="{{$instance->url}}" target=”_blank” >
	            		{{$instance->url}}	
	            	</a>
	            </td>
	            <td>
	            	@if($instance->state ==1)
	            		<i class="inline-icon material-icons">check</i>
	            		OK
	            	@else
	            		<i class="inline-icon material-icons">pause</i>
	            		PAUSA
		        	@endif
		        </td>
	            <td>
	            	<a class='waves-effect waves-teal btn-flat dropdown-trigger' href='#' data-target='dropdown{{$loop->index}}'>
	            		<i class="material-icons">settings</i>
	            	</a>
					  <!-- Dropdown Structure -->
					  <ul id='dropdown{{$loop->index}}' class='dropdown-content'>
					  	<li>
					    @if($instance->state ==1)
		            	<a href="admin/instance/{{$instance->id}}?state=pause">
		            		<i class="inline-icon material-icons">pause</i>Pausar
		            	</a>
		            	@else
			            <a href="admin/instance/{{$instance->id}}?state=play">
		            		<i class="inline-icon material-icons">play_arrow</i>
		            		Reanudar
		            	</a>
			        	@endif
			        	</li>
			        	<li class="divider" tabindex="-1"></li>
			        	<!--
			        	<li>
						    <a href="admin/instance/{{$instance->id}}">
			            		<i class="inline-icon material-icons">blur_circular</i>Detalles
			            	</a>
			            </li>
			        	-->
					  </ul>
	            </td>
	          </tr>
	          @endforeach
	        </tbody>
	      </table>
	  </div>
  	</div>
@stop
@section('scripts')
@stop