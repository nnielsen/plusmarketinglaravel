 <div style="padding-top: 10px;" class="wrapper border-bottom white-bg page-heading">
    <h2>Metodos de ingreso</h2>
    <ol class="breadcrumb">
        <li>
            <a href="#!/mi">{{ __('ui.inputMethods')}}</a>
        </li>
        <li class="active">
            <strong>{{ __('ui.edit')}}</strong>
        </li>
    </ol>


</div>
<div class="wrapper wrapper-content">
    <div class="row animated fadeInRight">
        <div class="col-md-offset-3 col-md-6 b-r">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>{{ __('ui.dataFrom')}} {{ __('ui.inputMethods')}}</h5>
                </div>
                <div>
                    <div class="ibox-content profile-content">
                        <p><i class="fa fa-at"></i>&nbsp;
                            <strong>{{ __('ui.name')}}</strong> 
                            <editablespan 
                            model="mi.nombre" 
                            on-ready="onReady(item)" 
                            span-class="info" 
                            input-class="info-input"
                            input-type="email">
                        </editablespan></p>
                        <p><i class="fa fa-phone"></i>&nbsp;
                            <strong>{{ __('ui.start')}}</strong> 
                            <editablespan 
                            model="mi.inicio" 
                            on-ready="onReady(item)" 
                            span-class="info" 
                            input-class="info-input">
                        </editablespan></p>
                          <p><i class="fa fa-phone"></i>&nbsp;
                            <strong>{{ __('ui.end')}}</strong> 
                            <editablespan 
                            model="mi.cierre" 
                            on-ready="onReady(item)" 
                            span-class="info" 
                            input-class="info-input">
                        </editablespan></p>
                        <div class="user-button">
                            <div ng-show="cambio">
                                <button class="btn btn-primary btn-sm btn-block" type="button" ng-click="guardar()"><i class="fa fa-save" ></i>&nbsp;&nbsp;<span class="bold">Guardar</span>
                                </button>
                            </div>
                            <a ng-click="delete()" confirmation-needed class="btn btn-danger btn-sm btn-block" type="button"><i class="fa fa-times-circle" ></i>&nbsp;&nbsp;<span class="bold">Eliminar</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>