
<div  style="padding-top:10px" class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-10">
        <h2>Reporte (puntos canjeados)</h2>
    </div>
    <div class="col-md-9">
        <div class="row">
            <div class="col-sm-5">
                <div class="form-group">
                    <label class="font-noraml">Desde</label>
                    <div class="input-group date">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        <input type="date" class="form-control" ng-model="inpSearch.fromDate">
                    </div>
                </div>
            </div>
            <div class="col-sm-5">
                <div class="form-group">
                    <label class="font-noraml">Hasta</label>
                    <div class="input-group date">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        <input type="date" class="form-control" ng-model="inpSearch.toDate">
                    </div>
                </div>
            </div>
            <div class="col-sm-2" style="z-index: 1000;">
                <h2>
                <button class="btn btn-info" type="button" ng-click="actualizar()"><i class="fa fa-refresh"></i>Filtrar
                </button>
                <h2>
            </div>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="ibox">
        <div class="ibox-content">
            <div class="clients-list">
                <div class="tab-content">
                    <div id="tab-1" class="tab-pane active">
                        <div class="full-height-scroll">
                            <div class="table-responsive">
                                <table ng-show="todo.length > 0" class="table table-striped table-hover">
                                    <tbody>
                                        <thead>
                                            <td><strong>Nombre cliente</strong></td>
                                            <td><strong>Puntos canjeados</strong></td>
                                        </thead>
                                        <tr ng-repeat="cliente in todo">
                                            <td><a data-toggle="tab" ng-href="#!/cliente/edit/@{{cliente.id}}" class="client-link">
                                            @{{cliente.nombre}} @{{cliente.apellido}}</a></td>
                                            <td> @{{cliente.puntos_canjeados_totales}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div ng-show="!todo || todo.length == 0" class="text-center">
                                    <h3 class="text-muted">Sin resultados</h3>
                                </div>
                                <button ng-show="lastPage > params.page" ng-click="showMore()" class="btn btn-block m-t">
                                    <i class="fa fa-arrow-down"></i> Ver más
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<link href="css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<script src="js/plugins/datapicker/bootstrap-datepicker.js"></script>
<script src="js/plugins/datapicker/bootstrap-datepicker.es.js"></script>   
