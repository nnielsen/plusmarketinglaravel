 <div style="padding-top: 10px;" class="wrapper border-bottom white-bg page-heading">
  <h2>{{ __('ui.clients')}}</h2>
  <ol class="breadcrumb">
    <li>
      <a href="#!/clientes">{{ __('ui.clients')}}</a>
    </li>
    <li>
      <a href="#!/cliente/edit/@{{cliente.id}}">@{{cliente.nombre}} @{{cliente.apellido}}</a>
    </li>
    <li class="active">
      <strong>{{ __('ui.transactions')}}</strong>
    </li>
  </ol>
</div>
<div class="wrapper wrapper-content">
  <div class="row animated fadeInRight">
    <div class="col-md-offset-2 col-md-8">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h2>{{ __('ui.transactions')}}</h2>
            <div class="row">
                <div class="col-sm-5">
                    <div class="form-group">
                        <label class="font-noraml">Desde</label>
                        <div class="input-group date">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <input type="date"  ng-change="actualFilter = null" class="form-control" ng-model="inpSearch.fromDate">
                        </div>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="form-group">
                        <label class="font-noraml">Hasta</label>
                        <div class="input-group date">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <input type="date" ng-change="actualFilter = null" class="form-control" ng-model="inpSearch.toDate">
                        </div>
                    </div>
                </div>
                <div class="col-sm-2" style="z-index: 1000;">
                    <h2>
                    <button class="btn btn-info" type="button" ng-click="filtrar(inpSearch)"><i class="fa fa-refresh"></i> Filtrar
                    </button>
                    <h2>
                </div>
            </div>
        </div>
        <div class="ibox-content">
          <div ng-if="actualFilter">
          <div class="row">
            <div class="col-md-9">
              <span class="text-info">Resultados para las fechas establecidas (
                <span ng-if="actualFilter.fromDate">desde @{{actualFilter.fromDate |asDate2 | date:'dd/MM/yyyy'}}</span>
                <span ng-if="actualFilter.toDate"> hasta @{{actualFilter.toDate |asDate2 | date:'dd/MM/yyyy'}} </span>)
              </span>
            </div>
            <div class="col-md-3">
              <span class="pull-right">
                <a href="" ng-click="clearFilter()">
                  <i class="fa fa-times"></i>
                  Limpiar filtro
                </a>
              </span>
            </div>
          </div>
        </div>
          <div class="table-responsive">
            <table class="table table-striped table-hover">
              <thead>
                <th>Tipo</th>
                <th>Puntos</th>
                <th>Fecha</th>
                <th></th>
              </thead>
                <tbody ng-repeat="transaccion in todo" ng-init="transaccion.edit = false">
                    <tr>
                        <td width="15%">@{{transaccion.idCompra?"Compra":"Canje"}}</td>
                        <td> @{{transaccion.puntos}}</td>
                        <td> @{{transaccion.created_at | asDate | date:'dd/MM/yyyy H:mm:ss'}}</td>
                        <td width="20%">
                          <a ng-if="!transaccion.deleted_at" class="btn btn-success btn-circle" type="button" ng-click="edit(transaccion)">
                            <i class="fa fa-pencil"></i>
                          </a>
                          <a ng-if="!transaccion.deleted_at" class="btn btn-danger btn-circle" type="button" confirmation-needed ng-click="delete(transaccion,$index)">
                            <i class="fa fa-times-circle"></i>
                          </a>
                          <span ng-if="transaccion.deleted_at" class="text-warning" type="button">
                            <i class="fa fa-times"></i> Anulad@{{transaccion.idCompra?"a":"o"}}
                          </span>
                        </td>
                    </tr>
                    <tr ng-if="transaccion.edit && !transaccion.deleted_at">
                      <td colspan="4">
                        <div class="row">
                          <div class="col-md-offset-1 col-md-10">
                            <table class="table">
                              <thead>
                                <th>@{{transaccion.idCanje?"Nombre":"Descripcion"}}</th>
                                <th>@{{transaccion.idCanje?"Puntos":"Monto"}}</th>
                                <th>Cantidad</th>
                              </thead>
                              <tbody>
                                <tr ng-repeat="item in transaccion.items">
                                  <td  width="40%">
                                    <div ng-if="item.articuloNombre">
                                       <span ng-class="{'text-muted':item.articuloDeleted==true}">@{{item.articuloNombre}}</span> 
                                       <small ng-if="item.articuloDeleted==true"> - 
                                        <span class="text-warning">
                                          Artículo eliminado del catálogo
                                        </small>
                                    </div>
                                    <div ng-if="item.descripcion">
                                      <editablespan 
                                      model="item.descripcion" 
                                      on-ready="changes(item)">
                                    </div>
                                  </td>
                                  <td width="40%">
                                    <div ng-if="item.articuloNombre">
                                       @{{item.puntos}}
                                    </div>
                                    <div ng-if="item.descripcion">
                                      <editablespan 
                                      model="item.monto" 
                                      on-ready="changes(item)">
                                    </div>
                                  </td>
                                  <td>
                                    <div>
                                      @{{item.cantidad}}
                                    </div>
                                  </td>
                                  <td>
                                    <a ng-if="!item.deleted_at" class="btn btn-xs btn-danger" type="button" confirmation-needed ng-click="deleteItem(transaccion,item,$index)">
                                      <i class="fa fa-times-circle"></i>
                                    </a>
                                    <a ng-if="!item.deleted_at && (item.changes && item.compraId)" class="btn btn-info btn-xs" type="button" ng-click="compraItemEdit(item,transaccion)">
                                      <i class="fa fa-save"></i>
                                    </a>
                                    <span ng-if="item.deleted_at" class="text-warning" type="button">
                                      <i class="fa fa-times"></i> Anulad@{{item.idCompra?"a":"o"}}
                                    </span>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </td>
                    </tr>
                </tbody>
                <tbody>
                  <tr ng-if="todo.length == 0">
                      <td colspan="4">
                        <span class="text-center">Sin transacciones</span>
                      </td>
                    </tr>
                </tbody>
            </table>
            <button ng-show="lastPage > params.page" ng-click="showMore()" class="btn btn-block m-t">
                <i class="fa fa-arrow-down"></i> Ver más
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
