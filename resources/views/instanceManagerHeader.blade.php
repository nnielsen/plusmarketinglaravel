<html>
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>{{ __('ui.card')}} | Manager instancias</title>

        <link href="{{ asset('css/materialize.min.css') }}" rel="stylesheet">
         <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">

    </head>
    <style type="text/css">
      .inline-icon {
         vertical-align: bottom;
         font-size: 18px !important;
      }
      
    </style>

<nav>
    <div class="nav-wrapper container">
        <a href="{{route('instanceManagerIndex')}}" class="brand-logo">
          <small>Manager instancias</small>
        </a>
      <ul class="right hide-on-med-and-down">
        <li>
          <a href="{{route('instanceManagerInstanceCreateForm')}}">
            +
            Crear instancia
          </a>
        </li>
         <li>
          <a href="{{route('instanceManagerLogout')}}">
            Logout
          </a>
        </li>
      </ul>
    </div>
  </nav>
  