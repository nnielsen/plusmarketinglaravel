<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Plusmarketing | Login</title>

    <link href="{{ asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{ asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">

    <link href="{{ asset('css/animate.css')}}" rel="stylesheet">
    <link href="{{ asset('css/style.css')}}" rel="stylesheet">
    <link href="{{ asset('css/plugins/toastr/toastr.min.css')}}" rel="stylesheet">

</head>
<script type="text/javascript"> 
    function status()
    {
        if(window.localStorage.getItem("token") != null)
        {
            location.href = "main";
        }    
    }
    status();
</script>
<body class="gray-bg">

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>

                <h3 class="logo-name"><!--plusmarketing--></h3>

            </div>
            @if (app('request')->input('reject'))
            <div class="alert alert-danger">
                <strong>Sesi&oacute;n</strong> Hubo un problema de autenticaci&oacute;n
            </div>
            @endif
            @if (app('request')->input('recovery'))
            <div class="alert alert-danger">
                <strong>Recuperación de contraseña</strong> Se ha enviado una nueva contraseña a su mail
            </div>
            @endif
            <form class="m-t" role="form" id="login">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Usuario" required="" id="usr">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" placeholder="Password" required="" id="pass">
                </div>
                <button class="btn btn-primary block full-width m-b">Login</button>
            </form>
            <p class="m-t"> <small><a href="passwordRecovery">Recuperar contraseña</a></small> </p>
        </div>
    </div>


    <!-- Mainly scripts -->
    <script src="{{ asset('js/jquery-2.1.1.js')}}"></script>
    <script src="{{ asset('js/jqblock.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>

    <script src="{{ asset('js/plugins/toastr/toastr.min.js') }}"></script>

    <script type="text/javascript">

        $(document).ready(function() {

            toastr.options = {
              "closeButton": true,
              "debug": false,
              "progressBar": true,
              "preventDuplicates": false,
              "positionClass": "toast-bottom-center",
              "onclick": null,
              "showDuration": "400",
              "hideDuration": "1000",
              "timeOut": "7000",
              "extendedTimeOut": "1000",
              "showEasing": "swing",
              "hideEasing": "linear",
              "showMethod": "fadeIn",
              "hideMethod": "fadeOut"
          }
      });
        $("#login").submit(function (e) {
            e.preventDefault();
            var alias = $("#usr").val(), password = $("#pass").val();
            $.ajax({
                type: "POST",
                url: "api/log",
                data: {alias,password},
                dataType: 'JSON',
                success: function (json) {
                    //$.unblockUI();
                    user=json.user;
                    if (user.id) {
                        Object.keys(user).forEach((key)=>
                            {
                                localStorage.setItem(key,user[key]);
                            });
                        localStorage.setItem('termsAndConditions',json.termsAndConditions);
                        localStorage.setItem('token',json.token);
                        setTimeout(()=>location.assign("main"));
                    } else if (json.msj) {
                        toastr.error(json.msj);
                    }
                },
                error: function (rejection, errorThrown) 
                {
                  location.assign("?reject=1")
                },
                beforeSend: function () {
                    $.blockUI({ message: '' });
                }
            });
        });
    </script>
</body>

</html>
