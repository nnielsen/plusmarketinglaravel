@extends('catalogoMainLayout')
@section('content')
<body>
  <div class="container">
    <div class="section">
      <div class="row">
        <div class="col s12 m6" style="min-height:400px">
          <div class="valign-wrapper" style="min-height:400px">
            <div class="center-align" style="margin: auto;">
              @if($articulo->imgUrl)
              <img style="max-width: 100%" src="{{Helpers::instanceRoute($articulo->imgUrl)}}">
              @else
              <p>IMAGEN NO DISPONIBLE</p>
              @endif                  
            </div>
          </div>
        </div>
        <div class="col s12 m6" style="min-height:400px">
          <div class="valign-wrapper" style="min-height: 400px">
          <table>
            <tbody>
              <tr>
                <td width="30%">
                  <span class="grey-text">
                    <strong>Nombre</strong>  
                  </span>
                </td>
                <td>{{$articulo->nombre}}</td>
              </tr>
              <tr>
                <td>
                  <span class="grey-text">
                    <strong>Descripcion</strong>
                  </span>
                </td>
                <td>{{$articulo->descripcion}}</td>
              </tr>
              <tr>
                <td>
                  <span class="grey-text">
                    <strong>Visitas</strong>
                  </span>
                </td>
                <td>{{$articulo->visitasCatalogo}}</td>
              </tr>
              <tr>
                <td>
                  <span class="grey-text">
                    <strong>
                      Puntos                    
                    </strong>
                  </span>
                </td>
                <td><h4 style="margin:0">{{$articulo->puntos}}</h4></td>
              </tr>
              <tr>
                <td colspan="2">
                  <p class="grey-text"><i class="material-icons">share</i> Compartir</p>
                  <div class="row">
                    <div class="col s4 m4 center-align">
                      <a href="https://www.facebook.com/sharer/sharer.php?u={{url()->current()}}">Facebook</a>
                    </div>
                    <div class="col s4 m4 center-align">
                      <a href="https://twitter.com/home?status={{url()->current()}}">Twitter</a>
                    </div>
                    <div class="col s4 m4 center-align">
                      <a href="https://www.linkedin.com/shareArticle?mini=true&url={{url()->current()}}&title=&summary=&source=">Linkedin</a>
                    </div>
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      </div>
    </div>
  </div>
</body>
@stop