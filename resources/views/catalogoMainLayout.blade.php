
@include('catalogoHeader')
<style>
nav,footer,.page-footer,.pagination li.active {
    background-color:#6a8592
}
body {
    display: flex;
    min-height: 100vh;
    flex-direction: column;
  }

  main {
    flex: 1 0 auto;
  }
</style>
    <body>
    	@yield('content')
    </body>
</html>
@include('catalogoFooter')
@include('catalogoScripts')