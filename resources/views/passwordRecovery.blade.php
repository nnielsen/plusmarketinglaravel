<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Plusmarketing | Login</title>
    <link href="{{ asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{ asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{ asset('css/animate.css')}}" rel="stylesheet">
    <link href="{{ asset('css/style.css')}}" rel="stylesheet">
    <link href="{{ asset('css/plugins/toastr/toastr.min.css')}}" rel="stylesheet">
</head>
<body class="gray-bg">
    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>
                <h3 class="logo-name">streambe</h3>
            </div>
            <form class="m-t" role="form" action="passwordRecovery/send" method="post">
                @csrf
                <div class="form-group">
                    <input type="mail" class="form-control" placeholder="Mail" required name="mail">
                </div>
                <button class="btn btn-primary block full-width m-b">Enviar mail de recuperación</button>
            </form>
            <p class="m-t"> <small>Streambe 2018</small> </p>
        </div>
    </div>
    <!-- Mainly scripts -->
    <script src="js/bootstrap.min.js"></script>
</body>
</html>
