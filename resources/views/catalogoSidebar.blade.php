<ul id="slide-out" class="sidenav">
  <li>
    @if(isset($logged))
    <div class="user-view">
      <div class="background">
        <img src="images/office.jpg">
      </div>
      <a href="#user">
        <img class="circle" src="images/yuna.jpg">
      </a>
      <a href="#name">
        <span class="name">John Doe</span>
      </a>
      <a href="#email">
        <span class="email">jdandturk@gmail.com</span>
      </a>
    </div>
    @else
   
    @endif
  </li>
  <div class="user-view">
      <div class="background" style="overflow:initial;">
        <img src="{{Helpers::instanceRoute('imgs/logo')}}" style="opacity: 0.5;
        filter: blur(10px);
        margin-left:-67px;">
      </div>
      <a href="#user">
        {{$instancia->businessName}}
      </a>
      <a href="#email">
      <span class="email"></span>
      </a>
    </div>
  @foreach ($categorias as $categoria)
  <div style="height: 6em">
    <li>
      <a href="{{Helpers::instanceRoute('catalogo')}}?category={{$categoria->id}}">
        <i class="material-icons">blur_on</i>
        {{$categoria->title}}
      </a>
    </li>
  @endforeach
  <li><div class="divider"></div></li>
  <li><a class="subheader"></a></li>
</ul>