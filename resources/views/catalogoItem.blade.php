<div class="row" style="    margin-top: 2em;">
@foreach ($fila as $articulo)
    <div class="col m4 s12">
      @if($articulo->imgUrl)
      <div class="card">
        <div class="card-image">
          <a href="{{Helpers::instanceRoute('catalogo/item/'.$articulo->id)}} ">
            <img src="{{Helpers::instanceRoute($articulo->thumbUrl)}} ">  
            <span class="card-title">{{$articulo->nombre}}</span>
          </a>
        </div>
        <div class="card-content">
          <a href="catalogo/item/{{$articulo->id}}">{{$articulo->descripcion}}</a>
        </div>
      </div>
      @else
        <div class="card blue-grey lighten-1">
          <div class="card-content white-text" style="    min-height: 230px;">
            <a href="{{Helpers::instanceRoute('catalogo/item/'.$articulo->id)}}">
              <span class="card-title white-text">{{$articulo->nombre}}</span>
            </a>
           <p>{{$articulo->descripcion}}</p>
          </div>
          <div class="card-action">
            <a class="btn-floating btn-large waves-effect waves-light" href="{{Helpers::instanceRoute('catalogo/item/'.$articulo->id)}}">
              <i class="material-icons">keyboard_arrow_right</i></a>
          </div>
        </div>
      @endif
    </div>
@endforeach
</div>
