<!DOCTYPE html>
<html data-ng-app="mainApp" ng-controller="sesion">
	@include('header')
<body ng-cloak>
	<div id="wrapper">
		@include('navbar')
		<div id="page-wrapper" class="gray-bg dashbard-1">
			<div data-ng-view=""></div>
			@include('mainSidebar')
		</div>
	@include('tycModal')
	@include('jsScripts')
	</div>
</body>
</html>