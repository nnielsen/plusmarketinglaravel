@include('mailLayout1Header')
<table class="container" style="border-collapse: collapse;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
	<tbody>
		<tr>
			<td>
				<table width="300" align="center" style="border-collapse: collapse; " cellspacing="0" cellpadding="0" border="0" style="margin-top: 20px;">
					<tbody>
						<tr>
							<td style="max-width: 299px;max-height: 160px;">
								<div style="min-height: 100px;max-height: 200px;text-align: center;">
									@if(!empty($envio))
										<img style="max-width: 260px; max-height: 210px;"  src="{{ Helpers::instanceRoute('imgs/logo'.'/'.$envio->id) }}">
									@else
									<img style="max-width: 260px; max-height: 210px;"  src="{{ Helpers::instanceRoute('imgs/logo') }}">
									@endif
								</div>

								<h2>COMPRA REALIZADA</h2>
								Los items que compraste son
								<ul style="list-style:none;padding-left: 10px;
								border-left: 2px solid;">
								@foreach ($itemsCompra as $item)
									<li>{{ $item->descripcion }}</li>
								@endforeach	
								</ul>
								<div style="width: 100%; margin-top:30px;">
									<div>
										<h2 style="margin-bottom:0;">
											PUNTOS
										</h2>
									</div>
									<div style="display: -webkit-box;display: inline-flex;width:100%">
										<div style="width:50%">
											<small>
												<strong>
													ACUMULADOS 
												</strong>
											</small>
											 <span style="font-size: 2em; padding-left:10px;">
												 {{$cliente->puntos}}
											 </span>
										</div>
										<div style="width:50%;text-align: right;">
											<small>
												<strong>
													COMPRA
												</strong>
											</small>
											<span style="font-size: 2em;padding-left:10px;">
												{{$compra->puntos}}
											</span>
											
										</div>
									</div>
								</div>		
@include('mailLayout1Footer')