@extends('catalogoMainLayout')

@section('content')
  	@if($articulos->count() == 0)
	<div class="container">
	  <div class="row container" style="margin-top: 2em;">
	    <div class="col s12 m12">
	      <div class="card blue-grey darken-1">
	        <div class="card-content white-text">
	           <span class="card-title">Sin resultados</span>
	          <p>No se han encontrado articulos que coicidan con su búsqueda.</p>
	        </div>
	        <div class="card-action">
	          <a href="{{Helpers::instanceRoute('catalogo')}}">Ir al inicio</a>
	        </div>
	      </div>
	    </div>
	  </div>
	</div> 
	@else
	@if(isset($query))
	<div class="container row">
		<p>Resultados de b&uacute;squeda para "{{$query}}"</p>
	</div>
	@endif
	@endif
	<div class="container">
	@each('catalogoItem',$articulos->chunk(3),'fila')
	</div>
	<div class="container" style="margin-top: 2em;margin-bottom: 3em">
	    <div class="center-align">
	        {{ $articulos->appends($_GET)->links('paginationCatalog') }}    
	    </div> 
	</div>
@stop