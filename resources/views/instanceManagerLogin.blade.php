<html>
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>{{ __('ui.card')}} | Manager instancias | Login</title>

        <link href="{{ asset('css/materialize.min.css') }}" rel="stylesheet">
         <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">

    </head>
    <style type="text/css">
      .inline-icon {
         vertical-align: bottom;
         font-size: 18px !important;
      }
      
    </style>

<body>
  <div class="section"></div>
  <main>
    <center>
      
      <div class="section"></div>
      <img src="{{asset('img/logo_streambe.svg')}}">
      <div class="container" style="margin-top: 2em;">
        @if (app('request')->input('error'))
        <div class="container">
          <div class="row">
              <div class="col m12 s12">
                <div class="card blue-grey darken-1">
                  <div class="card-content white-text">
                    <span class="card-title">Error en login</span>
                    <p>
                      Intente nuevamente
                    </p>
                  </div>
                </div>
            </div>
          </div>
        </div>
        @endif
        @if (app('request')->input('session'))
        <div class="container">
          <div class="row">
              <div class="col m12 s12">
                <div class="card blue-grey darken-1">
                  <div class="card-content white-text">
                    <span class="card-title">Debes iniciar sesion</span>
                  </div>
                </div>
            </div>
          </div>
        </div>
        @endif
        <div class="z-depth-1 grey lighten-4 row" style="display: inline-block; padding: 32px 48px 0px 48px; border: 1px solid #EEE;min-width: 400px;">
          <form class="col s12" method="post">
            @csrf
            <div class='row'>
              <div class='col s12'>
              </div>
            </div>
            <div class='row'>
              <div class='input-field col s12'>
                <input class='validate' type='text' name='user' id='email' />
                <label for='email'>Usuario</label>
              </div>
            </div>
            <div class='row'>
              <div class='input-field col s12'>
                <input class='validate' type='password' name='password' id='password' />
                <label for='password'>Password</label>
              </div>
            </div>
            <br />
            <center>
              <div class='row'>
                <button type='submit' name='btn_login' class='col s12 btn btn-large waves-effect red lighten-3'>
                  Login
                </button>
              </div>
            </center>
          </form>
        </div>
      </div>
    </center>

    <div class="section"></div>
    <div class="section"></div>
  </main>
</body>
@include('instanceManagerScripts')