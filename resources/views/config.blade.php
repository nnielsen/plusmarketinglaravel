 <div style="padding-top: 10px;" class="wrapper border-bottom white-bg page-heading">
    <h2>Configuraci&oacute;n de aplicaci&oacute;n</h2>
    <ol class="breadcrumb">
        <li>
            <a href="#!/main">Home</a>
        </li>
        <li class="active">
            <strong>Configuracion</strong>
        </li>
    </ol>
</div>
<div class="wrapper wrapper-content">
    <div class="row animated fadeInRight">
        <div class="col-sm-6 b-r">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Datos de contacto</h5>
                </div>
                <div>
                    <div class="ibox-content profile-content">
                        <p><i class="fa fa-map-marker"></i>&nbsp;
                            <label><strong>Direccion</strong> </label><br>
                            <label class="text-muted">Linea 1</label>
                            <editablespan 
                            model="config.direccionLinea1" 
                            on-ready="onReady(item)" 
                            span-class="info" 
                            input-class="info-input"
                            input-type="email">
                        </editablespan>
                        <label class="text-muted">Linea 2</label>
                              <editablespan 
                            model="config.direccionLinea2" 
                            on-ready="onReady(item)" 
                            span-class="info" 
                            input-class="info-input"
                            input-type="email">
                    </p>
                        <p><i class="fa fa-phone"></i>&nbsp;
                            <strong>Telefono</strong> 
                            <editablespan 
                            model="config.telefono" 
                            on-ready="onReady(item)" 
                            span-class="info" 
                            input-class="info-input">
                        </editablespan></p>
                           <p><i class="fa fa-instagram"></i>&nbsp;
                            <strong>Instagram</strong> 
                            <div class=row>
                                <div class="col-sm-5 text-right"><strong>www.instagram.com/</div>
                                <div class="col-sm-7">
                                     <editablespan 
                            model="config.ig" 
                            on-ready="onReady(item)" 
                            span-class="info" 
                            input-class="info-input">
                                </div>
                            </div>
                        </editablespan></p>
                        <p><i class="fa fa-facebook"></i>&nbsp;
                            <strong>Facebook</strong>
                            <div class=row>
                                    <div class="col-sm-5 text-right"><strong>www.facebook.com/</div>
                                    <div class="col-sm-7"> <editablespan 
                                        model="config.fb" 
                                        on-ready="onReady(item)" 
                                        span-class="info" 
                                        input-class="info-input">
                                    </editablespan></p></div>
                            </div>
                           
                        <p><i class="fa fa-twitter"></i>&nbsp;
                            <strong>Twitter</strong>
                            <div class=row>
                                    <div class="col-sm-5 text-right"><strong>www.twitter.com/</strong></div>
                                    <div class="col-sm-7">
                                            <editablespan 
                                            model="config.tw" 
                                            on-ready="onReady(item)" 
                                            span-class="info" 
                                            input-class="info-input">
                                        </editablespan></p>
                                    </div>
                            </div> 
                            
                    </div>
                </div>
            </div>
            <div class="i-box float-e-margins">
                <div class="ibox-title">
                     <h4>{{ __('ui.mail')}}  
                </div>
                 <div class="ibox-content profile-content">
                    <button class="btn-block btn btn-default" style="border-color:gray;" ng-init="advancedMailconfig=false" ng-click = "advancedMailconfig = !advancedMailconfig">{{ __('ui.advanced')}} @{{advancedMailconfig ? ' (esconder)' : '(mostrar)'}}</button></h4>
                        <p ng-show="advancedMailconfig">
                            <strong>{{ __('ui.host')}}</strong> 
                            <editablespan 
                            model="config.mailHost" 
                            on-ready="onReady(item)" 
                            span-class="info" 
                            input-class="info-input">
                        </editablespan></p>
                        <p>
                            <strong>{{ __('ui.user')}}</strong> 
                            <editablespan 
                            model="config.mailUsername" 
                            on-ready="onReady(item)" 
                            span-class="info" 
                            input-class="info-input">
                        </editablespan></p>
                        <p>
                            <strong>{{ __('ui.from')}}({{ __('ui.mail')}})</strong> 
                            <editablespan 
                            model="config.mailFromMail" 
                            on-ready="onReady(item)" 
                            span-class="info" 
                            input-class="info-input">
                        </editablespan></p>
                        <p>
                            <strong>{{ __('ui.recipient')}}({{ __('ui.name')}})</strong> 
                            <editablespan 
                            model="config.mailFromName" 
                            on-ready="onReady(item)" 
                            span-class="info" 
                            input-class="info-input">
                        </editablespan></p>
                        <p>
                            <strong>{{ __('ui.password')}}</strong> 
                            <editablespan 
                            model="config.mailPassword" 
                            on-ready="onReady(item)" 
                            span-class="info" 
                            input-class="info-input">
                        </editablespan></p>
                        <p ng-show="advancedMailconfig">
                            <strong>{{ __('ui.port')}}</strong> 
                            <editablespan 
                            model="config.mailPort" 
                            on-ready="onReady(item)" 
                            span-class="info" 
                            input-class="info-input">
                        </editablespan></p>
                        <div style="margin-bottom: 20px;">
                            <div ng-show="advancedMailconfig">
                                <strong>{{ __('ui.authentication')}}</strong>
                                <div class="row" style="margin-top: 5px;">
                                    <div class="text-center">
                                        <button class="btn btn-sm" type="button" ng-click="config.mailEncryption='tls';" ng-class="{'btn-info':config.mailEncryption=='tls','btn-default':config.mailEncryption != 'tls'}">TLS
                                        </button>
                                        <button class="btn btn-sm" type="button" ng-click="config.mailEncryption='ssl';"  ng-class="{'btn-info':config.mailEncryption=='ssl','btn-default':config.mailEncryption != 'ssl'}">SSL
                                        </button>
                                    </div>
                                </div>
                                <div>
                                    <strong>{{ __('ui.sslPeerVerification')}}</strong>
                                    <div class="row" style="margin-top: 5px;">
                                        <div class="text-center">
                                            <button class="btn btn-sm" type="button" ng-click="config.mailSMTPPeerVerif=1;" ng-class="{'btn-info':config.mailSMTPPeerVerif==1,'btn-default':config.mailSMTPPeerVerif == 0}">ON
                                            </button>
                                            <button class="btn btn-sm" type="button" ng-click="config.mailSMTPPeerVerif=0;"  ng-class="{'btn-info':config.mailSMTPPeerVerif==0,'btn-default':config.mailSMTPPeerVerif ==1}">OFF
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>                        
                        </div>
                    </div>
                </div>
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Logo</h5>
                </div>
                <div class="ibox-content profile-content">
                     <form enctype="multipart/form-data">
                        <input type="file" name="file" id="file" class="inputfile" onchange="angular.element(this).scope().uploadFile(this.files)"/>
                        <input type="file" name="file" id="file" class="inputfile" onchange="angular.element(this).scope().uploadFile(this.files)"/>
                        <label id="inpLabel" for="file" class="btn-block btn btn-default">Subir imagen</label><br>
                        <span id="upTexto"></span>
                        <div class="text-center">
                           <img id="logo" src="imgs/logo?u={{uniqid()}}" style="max-width: 400px;    margin-top: 2em;margin-bottom: 2em;">       
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-sm-6 b-r">
            <div class="ibox float-e-margins">
                <div class="ibox-content profile-content">
                    <div class="form-group">
                        <p><i class="fa fa-map-marker"></i>&nbsp;&nbsp;
                        <strong>Terminos y condiciones</strong><br>
                        <textarea id="tyc" onchange="onReady()" style="resize: vertical;" class="form-control" ng-model="config.terminosCondiciones"></textarea>
                        </p>
                    </div>
                </div>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-content profile-content">
                    <div class="form-group">
                        <p><i class="fa fa-map-marker"></i>&nbsp;&nbsp;
                        <strong>Categorías de artículos</strong><br>
                        <div categoria-manager></div>
                        </p>
                    </div>
                </div>
            </div>
             <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Reglas del negocio</h5>
                </div>
                <div class="ibox-content profile-content">
                    <p><i class="fa fa-money"></i>&nbsp;
                        <strong>Puntos por peso</strong> 
                        <editablespan 
                        model="config.puntosPeso" 
                        on-ready="onReady(item)" 
                        span-class="info" 
                        input-class="info-input">
                        </editablespan>
                    </p>
                </div>
            </div>
        </div>
</div>
<div class="row m-t">
    <div class="col-md-offset-3 col-md-6">
        <div class="ibox float-e-margins">
                <div class="ibox-content profile-content">
                 <div class="user-button">
                        <div>
                            <button confirmation-needed class="btn btn-primary btn-sm btn-block" type="button" ng-click="guardar()"><i class="fa fa-save" ></i>&nbsp;&nbsp;<span class="bold">Guardar</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>
