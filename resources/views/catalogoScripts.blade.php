<script type="text/javascript" src="{{ asset('js/jquery-2.1.1.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/materialize.min.js') }}"></script>
  <script type="text/javascript">
     document.addEventListener('DOMContentLoaded', function() {
      var elems = document.querySelectorAll('.sidenav');
      var instances = M.Sidenav.init(elems);
    });

    // Initialize collapsible (uncomment the lines below if you use the dropdown variation)
    // var collapsibleElem = document.querySelector('.collapsible');
    // var collapsibleInstance = M.Collapsible.init(collapsibleElem, options);

    // Or with jQuery

    $(document).ready(function(){
      $('.sidenav').sidenav();
      $('.collapsible').collapsible();
    });
    document.addEventListener('DOMContentLoaded', function() {
      var elems = document.querySelectorAll('.dropdown-trigger');
      var instances = M.Dropdown.init(elems, options);
    });

  // Or with jQuery

    $('.dropdown-trigger').dropdown();
  </script>
