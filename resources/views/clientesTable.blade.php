<div  style="padding-top:10px" class="row wrapper border-bottom white-bg page-heading">
    <div class="col-md-10">
        <h2>{{ __('ui.clients')}}</h2>
    </div>
    <div class="col-md-2">
        <h2>
        <a class="btn btn-primary btn-circle" type="button" ng-href="#!/cliente/alta" href="#!/cliente/alta"><i class="fa fa-plus"></i>
        </a>
        <h2>
    </div>
</div>
<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="ibox">
        <div class="ibox-content">
            @include('table-header')
            <div class="clients-list">
                <ul class="nav nav-tabs">
                    <span class="pull-right small text-muted">@{{(todo | filter:searchString).length}} {{ __('ui.client')}}@{{(todo | filter:searchString).length>1?"s":""}}</span>
                    <div ng-show="searchString" ng-click="clearSearch()">
                        <h4>
                            Resultados para: <span class="text-navy">@{{searchString}}<span class="bg-primary" style="border-radius: 100px;padding:4px;margin-left: 10px;"><i class="fa fa-times"></i></span></span>
                        </h4>
                    </div>
                </ul>
                <div class="tab-content">
                    <div id="tab-1" class="tab-pane active">
                        <div class="full-height-scroll">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover">
                                    <tbody>
                                        <tr ng-repeat="cliente in todo | filter:searchString" >
                                            <td width="40%"><a data-toggle="tab" ng-href="#!/cliente/edit/@{{cliente.id}}" class="client-link">@{{cliente.nombre}} @{{cliente.apellido}}</a></td>
                                            <td width="20%"> @{{cliente.telefono}}</td>
                                            <td class="contact-type"><i class="fa fa-envelope"> </i></td>
                                            <td width="30%"> @{{cliente.mail}}</td>
                                            <td width="10%" class="client-status">
                                                <a class="btn btn-success btn-circle" type="button" ng-href="#!/cliente/edit/@{{cliente.id}}"><i class="fa fa-pencil"></i>
                                                </a>
                                                <a class="btn btn-danger btn-circle" type="button" confirmation-needed ng-click="delete(cliente,$index)"><i class="fa fa-times-circle"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <button ng-show="lastPage > params.page" ng-click="showMore()" class="btn btn-block m-t">
                                    <i class="fa fa-arrow-down"></i> Ver más
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
