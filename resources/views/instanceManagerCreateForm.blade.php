@extends('instanceManagerMainLayout')
@section('content')
@if ($errors->any())
<div class="container">
	<div class="row">
	    <div class="col m12 s12">
	      <div class="card blue-grey darken-1">
	        <div class="card-content white-text">
	          <span class="card-title">Error en creacion de instancia</span>
	          <p>
	          	Los siguientes campos contienen errores:
	          </p>
	        </div>
	        <div class="card-action">
	           @foreach ($errors->all() as $error)
                <a href="">{{ $error }}</a>
            	@endforeach
	        </div>
	      </div>
	    </div>
	</div>
</div>
@endif
@if (app('request')->input('existingValues'))
<div class="container">
	<div class="row">
	    <div class="col m12 s12">
	      <div class="card blue-grey darken-1">
	        <div class="card-content white-text">
	          <span class="card-title">Error en creacion de instancia</span>
	          <p>
	          	El nombre de instancia o URL ya existen
	          </p>
	      	</div>
	    	</div>
		</div>
	</div>
</div>
@endif
<div class="container" style="margin-top: 1em">
	<h3 class="grey-text text-lighten-1">Crear</h3>
  <div class="row">
    <form class="col s12" action="{{route('instanceManagerInstanceCreatePost')}}" method="post">
	@csrf
      <div class="row">
        <div class="input-field col s6">
          <input name="businessName" id="businessName" type="text" class="validate" value="{{ old('businessName', '') }}">
          <label for="businessName">Nombre</label>
        </div>
        <div class="input-field col s6">
          <input name="url" value="{{ old('url', '') }}" id="url" type="text" class="validate">
          <label for="url">Url</label>
        </div>
      </div>
      <div class="row">
      	<p class="grey-text text-lighten-1">Administrador</p>
        <div class="input-field col s6">
          <input name="adminusr" value="{{ old('adminusr', '') }}" id="adminusr" type="text" class="validate">
          <label for="adminusr">Usuario</label>
        </div>
        <div class="input-field col s6">
          <input name="adminpass" value="{{ old('adminpass', '') }}" id="adminpass" type="password" class="validate">
          <label for="adminpass">Password</label>
        </div>
      </div>
      <div class="row">
        <button class="btn waves-effect waves-light red lighten-2" type="submit" name="action" id="submit">Crear
			<i class="material-icons right">flash_on</i>
		</button>
      </div>
    </form>
  </div>
</div> 
@stop
@section('scripts')
<script type="text/javascript">
	$( document ).ready(function() {
	    $('#submit').click(function()
	    {
	    	$.blockUI({ message: '<h1 class="white-text">Creando instancia</h1>' });
	    })
	});
</script>
@stop