<html>
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>{{ __('ui.card')}} | Cat&aacute;logo</title>

        <link href="{{ asset('css/materialize.min.css') }}" rel="stylesheet">
         <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">

    </head>
    <a href="#" style="position: absolute;color: white;height: 60px;width: 60px;top:10px" data-target="slide-out" class="sidenav-trigger">
      <i style="font-size: 40px;" class="material-icons">menu</i>
    </a>
        @include('catalogoSidebar')
<style type="text/css">
  .dropdown-trigger.subCategorias:after {
    content: '\e5c5';
    color: rgba(255,255,255,0.7);
    vertical-align: top;
    display: inline-block;
    font-family: 'Material Icons';
    font-weight: normal;
    font-style: normal;
    font-size: 25px;
    margin: 0 10px 0 8px;
    -webkit-font-smoothing: antialiased;
}
input:not([type]):focus:not([readonly]), input[type=text]:not(.browser-default):focus:not([readonly]), input[type=password]:not(.browser-default):focus:not([readonly]), input[type=email]:not(.browser-default):focus:not([readonly]), input[type=url]:not(.browser-default):focus:not([readonly]), input[type=time]:not(.browser-default):focus:not([readonly]), input[type=date]:not(.browser-default):focus:not([readonly]), input[type=datetime]:not(.browser-default):focus:not([readonly]), input[type=datetime-local]:not(.browser-default):focus:not([readonly]), input[type=tel]:not(.browser-default):focus:not([readonly]), input[type=number]:not(.browser-default):focus:not([readonly]), input[type=search]:not(.browser-default):focus:not([readonly]), textarea.materialize-textarea:focus:not([readonly]) {
    border-bottom: 1px solid white;
    -webkit-box-shadow: 0 1px 0 0 white;
    box-shadow: 0 1px 0 0 white;
}
input:not([type]), input[type=text]:not(.browser-default), input[type=password]:not(.browser-default), input[type=email]:not(.browser-default), input[type=url]:not(.browser-default), input[type=time]:not(.browser-default), input[type=date]:not(.browser-default), input[type=datetime]:not(.browser-default), input[type=datetime-local]:not(.browser-default), input[type=tel]:not(.browser-default), input[type=number]:not(.browser-default), input[type=search]:not(.browser-default), textarea.materialize-textarea {
    border-bottom: 1px solid white;
}
.bread-categorias .breadcrumb{
  font-size: 1em;
  font-weight: 100;
}
.dropdown-content li>a, .dropdown-content li>span {

    padding: 14px 16px 13px 10px;
</style>

<nav>
    <div class="nav-wrapper container">
         <a href="{{Helpers::instanceRoute('catalogo')}}" class="brand-logo">
          <img src="{{Helpers::instanceRoute('imgs/logo')}}" style="margin-top: 5px;    max-height: 53px;">
          </a>
      <ul class="right hide-on-med-and-down">
         <form action="{{Helpers::instanceRoute('catalogo')}}" method="get"> 
            <li>
                <button class="btn-floating waves-effect waves-light white" style="margin-right: 27px;" type="submit" href="sass.html">
                    <i style="line-height: 43px;" class="material-icons black-text">search</i></a></li>
                </button>
            <li>
            <input id="search" style="    min-width: 200px;" type="text" placeholder="Buscar" class="input form-control" name="query" value="{{isset($query)?$query:''}}">
        </li>
        </form>
      </ul>
    </div>
  </nav>
<nav>
  <div class="nav-wrapper">
    <div class="col s12">
      <div class="container">
        <div class="bread-categorias">
        <a href="{{Helpers::instanceRoute('catalogo')}}" class="breadcrumb">Catálogo</a>
        @if(isset($invertedCategoryTree))
            @foreach($invertedCategoryTree as $categoria)
            <a href="{{Helpers::instanceRoute('catalogo')}}?category={{$categoria->id}}" class="breadcrumb">
              {{$categoria->title}}
            </a>
            @endforeach
        @endif   
        @if(isset($subCategorias))
          <a class='dropdown-trigger subCategorias' style="margin-left: " href='#' data-target='dropdown1'>
          </a>
          <!-- Dropdown Structure -->
          <ul id='dropdown1' class='dropdown-content'>
            @foreach($subCategorias as $categoria)
            <li>
              <a href="{{Helpers::instanceRoute('catalogo')}}?category={{$categoria->id}}">{{$categoria->title}}</a>
            </li>
            @endforeach
          </ul>
        @endif
        </small>
      </div>
    </div>
  </div>
</nav>


  