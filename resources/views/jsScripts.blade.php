<!-- Mainly scripts -->
<script src="{{ asset('js/jquery-2.1.1.js') }}"></script>
<script src="{{ asset('js/angular.js') }}"></script>
<script src="{{ asset('js/angular-route.js') }}"></script>
<script src="{{ asset('js/angular-resource.js') }}"></script>
<script src="{{ asset('js/bootstrap.js') }}"></script>
<script src="{{ asset('js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
<script src="{{ asset('js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('js/jqblock.js') }}"></script>

<!-- Flot -->
<script src="{{ asset('/js/plugins/flot/jquery.flot.js') }}"></script>
<script src="{{ asset('/js/plugins/flot/jquery.flot.tooltip.min.js') }}"></script>
<script src="{{ asset('/js/plugins/flot/jquery.flot.spline.js') }}"></script>
<script src="{{ asset('/js/plugins/flot/jquery.flot.resize.js') }}"></script>
<script src="{{ asset('/js/plugins/flot/jquery.flot.pie.js') }}"></script>

<!-- Steps -->
<script src="{{ asset('js/plugins/staps/jquery.steps.js') }}"></script>

<!-- Jquery Validate -->
<script src="{{ asset('js/plugins/validate/jquery.validate.min.js') }}"></script>

<!-- Custom and plugin javascript -->
<script src="{{ asset('/js/inspinia.js') }}"></script>
<script src="{{ asset('/js/plugins/pace/pace.min.js') }}"></script>

<!-- jQuery UI -->
<script src="{{ asset('/js/plugins/jquery-ui/jquery-ui.min.js') }}"></script>

<!-- GITTER -->
<script src="{{ asset('/js/plugins/gritter/jquery.gritter.min.js') }}"></script>

<!-- Sparkline -->
<script src="{{ asset('/js/plugins/sparkline/jquery.sparkline.min.js') }}"></script>

<!-- ChartJS-->
<script src="{{ asset('/js/plugins/chartJs/Chart.min.js') }}"></script>

<!-- Chartist-->
<script src="{{ asset('/js/plugins/chartist/chartist.min.js') }}"></script>

<!-- Toastr -->
<script src="{{ asset('/js/plugins/toastr/toastr.min.js') }}"></script>


<!-- CardSwipe -->
<script src="{{ asset('js/cardSwipe.js') }}"></script>

<!-- EditableSpan -->
<script src="{{ asset('/js/editablespan.js') }}"></script>

<!-- APP -->
<script src="{{ asset('/js/app/globalFunctions.js') }}"></script>
<script src="{{ asset('/js/app/apiConector.js') }}"></script>
<script src="{{ asset('/js/app/mainApp.js') }}"></script>
<script src="{{ asset('/js/app/routes.js') }}"></script>
<script src="{{ asset('/js/app/swipe-service.js') }}"></script>
<script src="{{ asset('/js/app/session-service.js') }}"></script>
<script src="{{ asset('/js/app/mainController.js') }}"></script>
<script src="{{ asset('/js/app/articulosController.js') }}"></script>
<script src="{{ asset('/js/app/configController.js') }}"></script>
<script src="{{ asset('/js/app/clienteController.js') }}"></script>
<script src="{{ asset('/js/app/usuarioController.js') }}"></script>
<script src="{{ asset('/js/app/miController.js') }}"></script>
<script src="{{ asset('/js/app/rankingController.js') }}"></script>
<script src="{{ asset('/js/app/sucursalController.js') }}"></script>
<script src="{{ asset('/js/app/reporteController.js') }}"></script>
<script src="{{ asset('/js/app/operacionController.js') }}"></script>
<script src="{{ asset('/js/app/categoria-directive.js') }}"></script>
<script src="{{ asset('/js/app/transaccionesController.js') }}"></script>
<script src="{{ asset('/js/app/categoria-manager-directive.js') }}"></script>
<script src="{{ asset('/js/app/location-suggestion-directive.js') }}"></script>
<script src="{{ asset('/js/app/confirm-directive.js') }}"></script>
<script src="{{ asset('/js/app/card-input-directive.js') }}"></script>

<script src="{{ asset('/js/angular-confirm.min.js') }}"></script>
<script src="{{ asset('/js/angular-tree-control.js') }}"></script>
<script src="{{ asset('/js/angular-chartist.js') }}"></script>

<!--CDN-->
<script src="{{ asset('https://d3js.org/d3.v5.min.js') }}"></script>

<!--
<script src="{{ asset('js/app/app.js') }}"></script>
-->