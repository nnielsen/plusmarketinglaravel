 <div style="padding-top: 10px;" class="wrapper border-bottom white-bg page-heading">
    <h2>Metodos de ingreso</h2>
    <ol class="breadcrumb">
        <li>
            <a href="#!/mi">Metodos de ingreso</a>
        </li>
        <li class="active">
            <strong>Alta</strong>
        </li>
    </ol>


</div>
<div class="wrapper wrapper-content">
    <div class="row animated fadeInRight">
        <div class="col-md-offset-3 col-md-6 b-r">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>{{ __('ui.inputMethodParameters')}}</h5>
                </div>
                <div class="ibox-content profile-content">
                    <div class="form-group">
                        <label>{{ __('ui.name')}}</label> 
                        <input type="text" placeholder="Nombre" class="form-control" ng-model="mi.nombre">
                    </div>
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="form-group">
                                <label>{{ __('ui.start')}}</label> 
                                <input type="text" placeholder="%B" class="form-control" ng-model="mi.inicio">
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <span class="text-muted text-center">
                                <h1>##</h1>
                            </span>
                        </div>
                        <div class="col-sm-5">
                            <div class="form-group">
                                <label>{{ __('ui.end')}}</label> 
                                <input type="text" placeholder="\b" class="form-control" ng-model="mi.cierre">
                            </div>
                        </div>
                    </div>
                    <div class="user-button">
                        <div>
                            <button class="btn btn-primary btn-sm btn-block" type="button" ng-click="alta()"><i class="fa fa-save" ></i>&nbsp;&nbsp;<span class="bold">Guardar</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>