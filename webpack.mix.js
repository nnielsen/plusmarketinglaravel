const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

//Angular datetime-input
	mix.js('resources/js/angular-confirm-master/dist/angular-confirm.min.js', 
	'public/js/angular-confirm.min.js');
	mix.scripts([
				'public/js/app/globalFunctions.js',
				'public/js/app/apiConector.js',
				'public/js/app/mainApp.js',
				'public/js/swipe-service.js',
				'public/js/session-service.js',
				'public/js/app/routes.js',
				'public/js/app/mainController.js',
				'public/js/app/articulosController.js',
				'public/js/app/configController.js',
				'public/js/app/clienteController.js',
				'public/js/app/transaccionesController.js',
				'public/js/app/usuarioController.js',
				'public/js/app/sucursalController.js',
				'public/js/app/miController.js',
				'public/js/app/rankingController.js',
				'public/js/app/reporteController.js',
				'public/js/app/compraController.js',
				'public/js/app/card-input-directive.js',
				'public/js/app/categoria-directive.js',
				'public/js/app/location-suggestion-directive.js',
				'public/js/app/categoria-manager-directive.js',
				'public/js/angular-confirm.min.js'
			],'public/js/app/app.js');
	mix.styles(
		[
		'public/css/bootstrap.min.css',
		"public/font-awesome/css/font-awesome.css",
		"public/css/plugins/toastr/toastr.min.css",
		"public/js/plugins/gritter/jquery.gritter.css",
		"public/css/plugins/iCheck/custom.css",
		"public/css/plugins/steps/jquery.steps.css",
		'public/css/animate.min.css',
		'public/css/style.min.css',
		'resources/js/angular-confirm-master/dist/angular-confirm.min.css',
		], 
		'public/css/styles.css');

	//mix.copy('node_modules/materialize-css/dist/css/materialize.min.css', 'public/css/materialize.min.css');
	//mix.copy('node_modules/materialize-css/dist/js/materialize.min.js', 'public/js/materialize.min.js');
	mix.copy('node_modules/angular-chartist.js/dist/angular-chartist.js', 'public/js/angular-chartist.js');
	//mix.sass('resources/sass/catalogo-variables.scss', 'public/css');
	mix.sass('node-modules/materialize-css/sass/materialize.scss', 'public/css');