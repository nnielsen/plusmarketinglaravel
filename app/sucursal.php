<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Helpers;


class sucursal extends Model
{
    use SoftDeletes;
    protected $table = 'sucursal';
    protected $fillable = ['nombre','ciudad_id','calle','altura','extra'];
    protected $appends = ['ubicacion'];

    public function getUbicacionAttribute()
    {
        return Helpers::getUbicacionString($this->ciudad_id);
    }
    public function getFullUbicacion()
    {
        return implode(", ", [$this->ubicacion,$this->calle,$this->altura,$this->extra || '']);
    }
}
