<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use App\Http\Resources\ClienteExportResource;
use App\cliente;

class ClientesExport implements FromArray
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function array() : array
    {
    	$clientes = cliente::all();
    	$clientesArray=[];
    	foreach ($clientes as $cliente) {
			$clientesArray[] = [
			 'mail' => $cliente->mail,
            'apellido' => $cliente->apellido,
            'nombre' => $cliente->nombre,
            'calle' => $cliente->calle,
            'altura' => $cliente->altura,
            'piso'=>$cliente->piso,
            'depto'=>$cliente->depto,
            'ciudad'=>$cliente->ciudad,
            'codigoPostal'=>$cliente->codigoPostal,
            'pais'=>$cliente->pais,
            'telefono'=>$cliente->telefono,
            'nacimiento'=>$cliente->nacimiento,
            'genero'=>$cliente->genero,
            'dni'=>$cliente->dni,
            'created_at'=>$cliente->created_at
        	];
    	}
        return $clientesArray;
    }
}
