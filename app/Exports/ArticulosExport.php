<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use App\articulo;

class ArticulosExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return articulo::all();
    }
}
