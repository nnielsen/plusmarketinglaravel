<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Illuminate\Support\Facades\DB;

class rankingExport implements FromArray
{
    public function array():array
    {
        $results =  DB::select('SELECT * FROM puntos_cliente LIMIT 50');
        return $results;
    }
}
