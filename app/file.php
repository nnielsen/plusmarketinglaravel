<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class file extends Model
{
    protected $table = 'file';
    protected $fillable = ['nombre','typeId'];
    protected $appends = ['filename'];
    public function getPathAttribute()
    {
    	return storage_path() .'/app/' . $this->nombre;
    }
    public function getFilenameAttribute()
    {
    	$directory_separator = DIRECTORY_SEPARATOR;
    	$array = explode($directory_separator, $this->nombre);
    	return array_pop($array);
    }
}
