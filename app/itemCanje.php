<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class itemCanje extends Model
{
    use SoftDeletes;
    protected $table = 'canje_item';
    protected $fillable = ['articuloId', 'cantidad'];
    protected $appends = ['articuloNombre','puntos','articuloDeleted'];

    public function canje()
    {
        return $this->belongsTo('App\canje','canjeId');
    }
    public function articulo()
    {
    	return $this->hasOne('App\articulo','id','articuloId')->withTrashed();
    }
    public function getArticuloNombreAttribute()
    {
    	return $this->articulo->nombre;
    }
    public function getArticuloDeletedAttribute()
    {
        return $this->articulo->deleted_at != null;
    }
    public function getPuntosAttribute()
    {
    	return DB::select('SELECT puntos_canje.`puntosCanje`
                            FROM puntos_canje
                            WHERE puntos_canje.`canjeItemId` = '.$this->id)[0]->puntosCanje;
    }
}
