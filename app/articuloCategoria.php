<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\articulo;

class articuloCategoria extends Model
{
    protected $table = 'articulo_categoria';
    protected $fillable = ['title','parentId'];
    public $timestamps  = false;

    public static function getAll()
    {
        $query = "WITH RECURSIVE category_paths (id, title, parentId) AS
                    (
                      SELECT id, title, parentId
                        FROM  articulo_categoria
                        WHERE parentId IS NULL
                      UNION ALL
                      SELECT c.id, c.title, c.parentId
                        FROM category_paths AS cp JOIN  articulo_categoria AS c
                          ON cp.id = c.parentId
                    )
                    SELECT * FROM category_paths
                    ORDER BY parentId;";
        return DB::select($query);
    }
    public static function getAllWithPath()
    {
        $query = "WITH RECURSIVE category_paths (id, title, path) AS
                    (
                      SELECT id, title, title as path
                        FROM  articulo_categoria
                        WHERE parentId IS NULL
                      UNION ALL
                      SELECT c.id, c.title, CONCAT(cp.path, ' > ', c.title)
                        FROM category_paths AS cp JOIN  articulo_categoria AS c
                          ON cp.id = c.parentId
                    )
                    SELECT * FROM category_paths
                    ORDER BY path;";
        return DB::select($query);
    }
    public function getFullSons()
    {
    	$query = 'WITH RECURSIVE category_paths (id, title, path) AS
					(
					  SELECT id, title, parentId
					    FROM  articulo_categoria
					    WHERE parentId = '.$this->id.' 
					  UNION ALL
					  SELECT c.id, c.title, c.parentId
					    FROM category_paths AS cp JOIN  articulo_categoria AS c
					      ON cp.id = c.parentId
					)
					SELECT * FROM category_paths
					ORDER BY path;';
		return DB::select($query);
    }
    protected function getSonsIds()
    {
    	$return = [];
    	foreach ($this->getFullSons() as $key) {
    		$return[]=$key->id;
    	}
    	return $return;
    }
    public function deleteNode()
    {
		return $this->delete();
    }
    public function getFullAncestorsTree()
    {
    	$query = 'WITH RECURSIVE category_path (id, title, parentId) AS
			(
			  SELECT id, title, parentId
			    FROM  articulo_categoria
			    WHERE id = '.$this->id.'
			  UNION ALL
			  SELECT c.id, c.title, c.parentId
			    FROM category_path AS cp JOIN  articulo_categoria AS c
			      ON cp.parentId = c.id
			)
			SELECT * FROM category_path;';
		return DB::select($query);
    }
    public function _parent()
    {
        return $this->belongsTo(__CLASS__, 'parentId');
    }
    public function getParentAttribute()
   {
        if ( ! $this->getAttribute('parentId')) {
            return null;
        }
        return $this->_parent();
   }
}
