<?php

namespace App;
use Akaunting\Setting\Facade as Setting;
use Illuminate\Database\Eloquent\Model;
use App\puntosPeso;
use App\file as dbFile;

class configuracionInstancia extends Model
{
    protected $table = "configuracion_instancia";
    private static $instancia;
    protected $fillable = ['fb','tw','ig','direccionLinea1','direccionLinea2','telefono','terminosCondiciones','idMagneticoRegex','mailPort','mailHost','mailUsername','mailPassword','mailEncryption','mailFromMail','mailFromName','mailSMTPSecure','mailSMTPPeerVerif','puntosPeso'];
    protected $hidden = ['created_at','updated_at','id','logoId'];
    protected $appends = ['puntosPeso','nombreComercio', 'catalogoUrl','fullFb','fullIg','fullTw'];
    protected $url;

    public static function getInstancia()
    {
    	if(!isset(self::$instancia))
    	{
    		self::$instancia = self::find(1);
    	}
    	return self::$instancia;
    }
    public function registerCallbackUrl($callbackUrl) {
        $this->url = $callbackUrl;
    }
    public function instancia()
    {
        return $this->hasOne('App\Instance','id','instanceId');
    }
    public function getPublicUrlAttribute()
    {
        return implode('/', [
            env('APP_URL'),
            $this->url
            ]);
    }
    public function getCatalogoUrlAttribute() 
    {
        return implode('/', [
            $this->publicUrl,
            'catalogo'
            ]);
    }
    public function getFullFbAttribute() {
        return 'http://facebook.com/' . $this->fb;
    }
    public function getFullTwAttribute() {
        return 'http://twitter.com/' . $this->tw;
    }
    public function getFullIgAttribute() {
        return 'http://instagram.com/' . $this->ig;
    }
    public function getNombreComercioAttribute()
    {
        return $this->instancia->businessName;
    }
    public function getPuntosPesoAttribute($reload=false)
    {
        return puntosPeso::getInstancia($reload)->coeficiente;
    }
    public function getLogoAttribute()
    {
        return dbFile::find($this->logoId);
    }
    public function setPuntosPesoAttribute($value)
    {
        if($value != puntosPeso::getInstancia()->coeficiente)
        {
            $puntosPeso = new puntosPeso(['coeficiente'=>$value]);
            $puntosPeso->save();
            $this->getPuntosPesoAttribute(true);
        }
    }
}
