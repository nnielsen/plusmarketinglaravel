<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\tarjeta;

class tarjeta extends Model
{
    protected $table = 'tarjeta';
    protected $fillable = ['idMagnetico'];

    public static function idExists($idMagnetico)
    {
    	return tarjeta::where('idMagnetico',$idMagnetico)->get()->first() != null;
    }
    public function cliente()
    {
        return $this->belongsTo('App\cliente','clienteId','id');
    }
}
