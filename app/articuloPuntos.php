<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class articuloPuntos extends Model
{
    protected $table = 'articulo_puntos';
    protected $fillable = ['puntos'];
    public function articulo()
    {
        return $this->belongsTo('App\articulo','articuloId');
    }
}
