<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class userLoginResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $response = [
            'alias' => $this->alias,
            'apellido' => $this->apellido,
            'nombre' => $this->nombre,
            'id' => $this->id,
            'token' => $this->token,
            'inputMethodId'=>$this->inputMethodId,
            'metodoIngreso'=>$this->metodoIngreso,
            'tipoId'=>$this->tipoId,
            'acceptedTermsConditions'=>$this->acceptedTermsConditions
        ];
        if($this->metodoIngresoObject)
        {
            $response = array_merge($response,[
                'inputMethodInicio'=>$this->metodoIngresoObject->inicio,
                'inputMethodCierre'=>$this->metodoIngresoObject->cierre
            ]);
        }

        return $response;
    }
}
