<?php

namespace App\Http\Middleware;

use JWTAuth;
use App\User;
use Closure;

class userToInstance
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = JWTAuth::user();

        $hashedHash = JWTAuth::getPayload()->get('password');

        $storedUser = User::find($user->id);
        
        if($hashedHash != $storedUser->hashedHash)
        {
            return response('Error de permisos',401);
        }

        return $next($request);
    }
}
