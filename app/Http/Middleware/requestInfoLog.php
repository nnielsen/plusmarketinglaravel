<?php

namespace App\Http\Middleware;
use JWTAuth;
use Illuminate\Support\Facades\Log;

use Closure;

class requestInfoLog
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        $user = JWTAuth::user();
        $level = 'info';
        if($response->getStatusCode() == 401)
        {
            $level = 'notice';
        }

        Log::$level('RequestApi -> '. $request->getMethod() .' '. $request->fullUrl());
        Log::$level('Client '.$request->getClientIp() . ' - ' .$response->getStatusCode());
        if(!empty($user))
        {
            Log::$level('User '.$user->id);
        }
        
        return $response;
    }
}
