<?php

namespace App\Http\Middleware;
use JWTAuth;
use App\User;
use Closure;

class tipoUsuarioMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = JWTAuth::user();
        if($user->tipoId != 1)
        {
            return response('Error de permisos',401);
        }
        return $next($request);
    }
}
