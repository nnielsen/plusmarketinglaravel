<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;
use App\instanceManager;
use Illuminate\Support\Facades\Config;
use JWTAuth;
use App\User;

class databaseSelectorMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $instanceManager = instanceManager::getInstance(); 
        $databaseName = $instanceManager->databaseName;
        instanceManager::setDatabaseName($databaseName);

        $instanceUrl = trim($request->route()->parameter('instanceUrl'));

        $query = sprintf("SELECT database_name,state FROM instances WHERE url = \"%s\"", $instanceUrl);
        $database = DB::select($query);
        
        if(count($database) < 1)
        {
            abort(404);
        }else if($database[0]->state != Config::get('constants.instanceManager.states.play'))
        {
            abort(423);
        }
        $databaseName = $database[0]->database_name;
        
        instanceManager::setDatabaseName($databaseName);

        return $next($request);
    }
}
