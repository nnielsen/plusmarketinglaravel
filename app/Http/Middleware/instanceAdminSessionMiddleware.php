<?php

namespace App\Http\Middleware;

use Closure;

class instanceAdminSessionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->session()->get('instanceManagerSession')!= true)
        {
            return \Redirect::route('instanceManagerLoginForm',
                [
                    'session' => true
                ]);
        }
        return $next($request);
    }
}
