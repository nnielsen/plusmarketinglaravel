<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class articuloPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "nombre"=> "bail|required|min:2",
            "descripcion"=> "bail|required|min:2",
            "puntos"=> "bail|required|numeric|min:1",
            "stock"=> "bail|required|numeric"
        ];
    }
}
