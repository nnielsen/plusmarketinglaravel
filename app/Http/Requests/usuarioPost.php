<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class usuarioPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "alias"=> "bail|required|unique:users,deleted_at,NULL|min:2",
            "nombre"=> "bail|required|min:2",
            "apellido"=> "bail|required|min:2",
            "mail"=> "bail|required|unique:users,deleted_at,NULL|email",
            "telefono"=> "bail|required|phone",
            "tipoId"=>"bail|required|numeric|max:3",
            "password"=> "bail|required|min:2",
            "sucursal_id"=>"bail|exists:sucursal,id"
        ];
    }
}