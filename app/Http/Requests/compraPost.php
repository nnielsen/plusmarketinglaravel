<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class compraPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "clienteId"=> "required|numeric|min:1",
            'purchaseItems' => 'required|array|min:1',
        ];
    }
}
