<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;


class clientPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "nombre"=> "bail|required|min:2",
            "apellido"=> "bail|required|min:2",
            "mail"=> "bail|required|email",
            "telefono"=> "bail|required|phone",
            "calle"=> "bail|required",
            "altura"=> "bail|required|numeric",
            "codigoPostal"=> "bail",
            "ciudad_id"=>"bail|required",
            "genero"=> "bail|required",
            "estadoCivil"=> "bail|required",
            "nacimiento"=> "bail|required|date",
            "dni"=> "bail|required|min:6",
            //"idmagtarjeta"=> "required",
        ];
    }
}
