<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class instanceCreatePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'businessName'=>'required',
            'url'=>'required|alpha_dash',
            'adminusr'=>'required|alpha_num',
            'adminpass'=>'required|alpha_num'
        ];
    }
}
