<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\instanceManager;
use Illuminate\Support\Facades\Config;
use App\Http\Requests\instanceCreatePostRequest;


class instanceAdminController extends Controller
{
    public function index(Request $Request)
    {
    	$instanceManager = instanceManager::getInstance();

    	if(!$instanceManager->getManagerCreated())
    	{
    		abort(404);
    	}
    	else
    	{
    		$scope['instances'] = $instanceManager->getInstances();
    		return view('instanceManagerIndex',$scope);
    	}
    }
    public function instanceAction(Request $request)
    {
        if(!empty($state = $request->input('state')))
        {
            return $this->stateChange($request,$state);
        }
    }
    public function login(Request $request)
    {
        if($request->password != env('INSTANCE_ADMIN_PASS')
        OR $request->user != env('INSTANCE_ADMIN_USR'))
        {
            return \Redirect::route('instanceManagerLoginForm',
                ['error'=>true]);
        }else
        {
            session(['instanceManagerSession' => true]);
            return \Redirect::route('instanceManagerIndex');
        }
    }
    public function logout()
    {
        session(['instanceManagerSession' => false]);
        return \Redirect::route('instanceManagerLoginForm');
    }
    public function instanceCreatePost(instanceCreatePostRequest $request)
    {
        $request = $request->all();
        $instanceManager = instanceManager::getInstance();

        try{
            $instanceManager->createInstance($request);
            return \Redirect::route('instanceManagerIndex');
        }
        catch(\Exception $e)
        {
            return \Redirect::route('instanceManagerInstanceCreateForm',
                ['existingValues'=>true]);
        }
    }
    public function stateChange(Request $request,$state)
    {
        $instanceManager = instanceManager::getInstance();
        switch ($state) {
            case 'play':
            case 'pause':  
                $inpState = Config::get('constants.instanceManager.states.' . $state);
                break;
            default:
                # code...
                break;
        }
        $instanceManager->changeInstanceState($request->instanceId,$inpState);
        
        return back();
    }
}
