<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\sucursal;
use App\Http\Requests\sucursalPost;

class sucursalController extends Controller
{
    public function getAll(Request $request)
    {
    	if($request->has('queryString'))
        {
            return sucursal::where('nombre','like','%' . $request->queryString . '%')
            ->paginate(15);
        }
    	return response()->json(sucursal::paginate(15));
    }
    public function create(sucursalPost $request)
    {
    	$request = $request->all();
        $sucursal = new sucursal($request);
        $sucursal->save();
        return response()->json(['alta'=>$sucursal]);
    }
    public function put(sucursalPost $request)
    {
        $sucursal = sucursal::find($request->id);
        $keys = ['nombre','ciudad_id','calle','altura','extra'];

        foreach ($keys as $key) {
            $sucursal->$key = $request->$key;
        }
        $sucursal->save();
        return response()->json([
            'update' => $sucursal
        ]);
    }
    public function delete(Request $request)
    {
    	sucursal::destroy($request->id);
    	return response()->json(['delete'=>sucursal::withTrashed()->where('id',$request->id)->get()->first()->trashed()]);
    }
}
