<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\cliente;
use App\instanceManager;

class formFilter extends Controller
{
    public function searchLocation(Request $request)
    {
    	$instanceManager = instanceManager::getInstance();
    	$queryString = $request->queryString;
    	$queryFootprint = 'SELECT CONCAT(ciudad.nombre,", " , provincia.`nombre`) 
							AS searchString,
							ciudad.id 
							FROM '.$instanceManager->commonDatabase.'.ciudad AS ciudad
							JOIN '.$instanceManager->commonDatabase.'.provincia AS provincia
							ON ciudad.`provincia_id` = provincia.`id`
							WHERE ';
		$criteria = [
			'ciudad.nombre LIKE "%s%%"',
			'ciudad.nombre LIKE "%%%s%%"',
			'provincia.nombre LIKE "%s%%"',
			'provincia.nombre LIKE "%%%s%%"'
		];
		$statements = [];
		
		foreach ($criteria as $key) {
			$statements[] = sprintf($queryFootprint . $key,$queryString);
		}

		$query = implode(" UNION ", $statements);
		$query .= " LIMIT 10";
		return response()->json(DB::select($query));
    }
    public function searchCliente(Request $request)
    {
    	$key = $request->queryString;
    	$clientes = cliente::where('nombre',$key)
                ->orWhere('nombre', 'like', '%' . $key . '%')
                ->orWhere('apellido', 'like', '%' . $key . '%')
                ->limit(5)
                ->get();
        return response()->json($clientes);
    }
}
