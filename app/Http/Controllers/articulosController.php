<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\articulo;
use App\file as dbFile;
use App\articuloPuntos as Puntos;
use App\articuloStock as Stock;
use App\Http\Requests\articuloPost;
use App\Http\Requests\GetArticuloRequest;
use App\Exports\ArticulosExport;
use App\Helpers;
use App\articuloCategoria;


class articulosController extends Controller
{
    public function getAll(Request $request)
    {
        if($request->has('queryString'))
        {
            return articulo::where('nombre','like','%' . $request->queryString . '%')
            ->orWhere('descripcion','like','%' . $request->queryString . '%')
            ->paginate(15);
        }
    	return response()->json(articulo::paginate(15));
    }
    public function create(articuloPost $request)
    {
    	$request = $request->all();

        $stock = new Stock(['amount'=>$request['stock']]);
        $puntos = new Puntos(['puntos'=>$request['puntos']]);

        $articulo = new articulo([
            'descripcion' => $request['descripcion'],
            'nombre' => $request['nombre']
        ]);
        $articulo->save();

    	$articulo->getStock()->save($stock);
        $articulo->getPuntos()->save($puntos);

        return response()->json(['alta'=>$articulo]);
    }
    public function imgUpload(Request $request)
    {
        $path = $request->file('img')->store('imgs/articulos');
        $articulo = articulo::find($request->id);

        $archivo = new dbFile(['nombre'=>$path,'typeId'=>1]);
        $archivo->save();

        $articulo->imagenId = $archivo->id;

        return response()->json(['img'=>$articulo->save(),'url'=>asset($path)]);
    }
    public function get(Request $request)
    {
    	return response(articulo::find($request->id));
    }
    public function delete(Request $request)
    {
    	articulo::destroy($request->id);
    	return response(['delete'=> articulo::withTrashed()->where('id',$request->id)->get()->first()->trashed()]);
    }
    public function put(Request $request)
    {
    	$updatable = ['descripcion','nombre','puntos','stock','categoriaId'];

    	$articulo = articulo::where('id', $request['id'])->first();
    	
    	foreach ($updatable as $key) {
    		$articulo->$key = $request->$key;
    	}
    	$articulo->push();
        return response()->json(['update'=>$articulo]);
    }
    public function export(Request $request)
    {
        $articulosExport = new ArticulosExport;
        return Helpers::export($articulosExport,$request->format,'articulosExport');
    }
    public function getCategorias()
    {  
        return response()->json(articuloCategoria::all());
    }
    public function getSubtree(Request $request)
    {
        $categoria = articuloCategoria::find($request->categoryId);
        return response()->json($categoria->getFullAncestorsTree());
    }
    public function createCategoria(Request $request)
    {
        $categoria = new articuloCategoria(
            [
                'title'=>$request->title,
                'parentId'=>$request->parentId
            ]
        );
        $categoria->save();
        $response = ['categoriaAll'=>articuloCategoria::all()];
        return response()->json($response);
    }
    public function deleteCategoria(Request $request)
    {
        $articuloCategoria = articuloCategoria::find($request->id);
        return response()->json(['delete'=>$articuloCategoria->deleteNode()]);
    }
}
