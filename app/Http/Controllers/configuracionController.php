<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\configuracionInstancia;
use App\file as dbFile;
use App\Helpers;
use App\envioMail;
use App\Http\Requests\configPost;

class configuracionController extends Controller
{
    public function get()
    {
    	return response()->json(configuracionInstancia::getInstancia());
    }
    public function put(configPost $request)
    {
    	$instancia = configuracionInstancia::getInstancia();
        $request = $request->all();
        $instancia->fill($request);
        $instancia->save();
    	return response()->json(['cambio_config'=>$instancia]);
    }
    public function getLogo(Request $request)
    {
        if($request->route('mailId'))
        {
            $envio = envioMail::find($request->mailId);
            $envio->leido = date('Y-m-d H:i:s');
            $envio->save();
        }
        $instancia = configuracionInstancia::getInstancia();
        if($instancia->logo)
        {
            $path = $instancia->logo->path;
        }
        else
        {
            $path = 'img/NA.png';
        }
        
        return response()->file($path);
    }
    public function setLogo(Request $request)
    {
        $path = $request->file('img')->store('imgs/logo');
        $instancia = configuracionInstancia::getInstancia();

        $archivo = new dbFile(['nombre'=>$path,'typeId'=>1]);
        $archivo->save();

        $instancia->logoId = $archivo->id;

        return response()->json(['img'=>$instancia->save()]);
    }
}
