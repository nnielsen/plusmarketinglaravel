<?php

namespace App\Http\Controllers;
use App\User as Usuario;
use App\metodoIngreso as MetodoIngreso;
use Illuminate\Http\Request;
use JWTAuth;
use App\Helpers;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Exports\UsersExport;
use App\sucursal;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\usuarioPost;

class usuariosController extends Controller
{
	public function getAll(Request $request)
    {
    	if($request->has('queryString'))
        {
            return Usuario::where('nombre','like','%' . $request->queryString . '%')
                ->orWhere('apellido','like','%' . $request->queryString . '%')
                ->orWhere('mail','like','%' . $request->queryString . '%')
                ->orWhere('alias','like','%' . $request->queryString . '%')
                ->paginate(15);
        }
        return response()->json(Usuario::paginate(15));
    }
    public function get(Request $request)
    {
        return response()->json(Usuario::find($request->id));
    }
    public function create(usuarioPost $request)
    {
        $request = $request->all();
        $request['password'] =  Hash::make($request['password']);

        $usuario = new Usuario($request);
        $success = $usuario->save();

        $verification_code = str_random(30);
        DB::table('user_verifications')->insert(['user_id'=>$usuario->id,'token'=>$verification_code]);

        return response()->json(['alta'=>$success]);
    }
    public function put(Request $request)
    {
        $usuario = Usuario::find($request->id);
        if($request->has('password'))
        {
            $usuario->password = Hash::make($request->password);
            $usuario->save();
        }
        else
        {
            $updatable = ['alias','mail','telefono','tipoId','sucursal_id'];
            foreach ($updatable as $key) {
                if($usuario->$key != $request->$key)
                {
                    $usuario->$key = $request->$key;
                }
            }
            $usuario->push();    
        }
    	
    	return response()->json(['update'=>$usuario]);
    }
    public function delete(Request $request)
    {
        $user = JWTAuth::user();
        if($user->id == $request->id)
        {
            return response(['error'=>'Esta intentando eliminar el usuario asignado a la sesion actual'],403);
        }
        $delete = (bool)Usuario::destroy($request->id) == 1;
        return response(['delete'=>Usuario::withTrashed()->where('id',$request->id)->get()->first()->trashed()]);
    }
    public function termsconditions()
    {
        $user = JWTAuth::user();
        $user->acceptedTermsConditions = 1;
        return response()->json(['save'=>$user->save()]);
    }
    protected function getSelectData()
    {
        $inputMethods = \App\Helpers::createSelectArray(MetodoIngreso::all(),'nombre');
        $inputMethods[] = ['name'=>__('constants.manual'),'value'=>0];
        $userTypes = \Config::get('constants.userTypes');
        $branchOffices = sucursal::all()->toArray();

        $userTypesSelect=[];
        foreach ($userTypes as $key => $value) {
            $userTypesSelect[]=['name'=>$key,'value'=>$value];
        }
        return [
            'inputMethods'=>$inputMethods,
            'userTypes'=>$userTypesSelect,
            'branchOffices'=>$branchOffices
            ];
    }
    public function getCreateForm()
    {
    	return view('usuarioAlta',$this->getSelectData());
    }
    public function getEditForm()
    {
        return view('usuarioEdit',$this->getSelectData());
    }
    public function export(Request $request)
    {
        $usersExport = new UsersExport;
        return Helpers::export($usersExport,$request->format,'usersExport');
    }
}
