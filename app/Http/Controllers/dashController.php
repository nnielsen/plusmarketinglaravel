<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use JWTAuth;

class dashController extends Controller
{
    public function dashStats()
    {
    	$user = JWTAuth::user();

    	$results= [];
    	$queryCompra= 			'SELECT
    							COUNT(1) AS COUNT,
								AVG(
									(SELECT SUM(compra_item.`monto`) 
									FROM compra_item 
									WHERE compra_item.compraId = compra.id) 
									) AS AVG,
									SUM(
									(SELECT SUM(compra_item.`monto`) 
									FROM compra_item 
									WHERE compra_item.compraId = compra.id) 
									) AS SUM,
									MAX(
									(SELECT SUM(compra_item.`monto`) 
									FROM compra_item 
									WHERE compra_item.compraId = compra.id) 
									) AS MAX
								FROM compra
								WHERE compra.`deleted_at` IS NULL ';
		$results['compra']= [];
		$compra = DB::select($queryCompra);
		$results['compra']['global'] = [];
		$results['compra']['global']['siempre'] = $compra[0];
		$compra= DB::select($queryCompra . ' AND compra.userId = '.$user->id);
		$results['compra']['usr']['siempre'] = $compra[0];

    	$compra = DB::select($queryCompra . ' AND compra.`created_at` > (NOW() - INTERVAL 1 MONTH)');
    	
    	$results['compra']['global']['lastMonth'] = $compra[0];
		$compra = DB::select($queryCompra . ' AND compra.`created_at` > (NOW() - INTERVAL 1 MONTH) AND compra.userId = '.$user->id);
		$results['compra']['usr']['lastMonth'] = $compra[0];

    	$queryCanje =	'SELECT
    						COUNT(1) AS COUNT,
							AVG(
								(SELECT SUM(puntos_canje.`puntosCanje`) 
						FROM puntos_canje 
						JOIN canje_item
						ON canje_item.`id` = puntos_canje.`canjeItemId`
						WHERE canje_item.`canjeId` = canje.id) 
								) AS AVG,
								SUM(
								(SELECT SUM(puntos_canje.`puntosCanje`) 
						FROM puntos_canje 
						JOIN canje_item
						ON canje_item.`id` = puntos_canje.`canjeItemId`
						WHERE canje_item.`canjeId` = canje.id)
								) AS SUM,
								MAX(
								(SELECT SUM(puntos_canje.`puntosCanje`) 
						FROM puntos_canje 
						JOIN canje_item
						ON canje_item.`id` = puntos_canje.`canjeItemId`
						WHERE canje_item.`canjeId` = canje.id
								)
								) AS MAX
							FROM canje 
							WHERE canje.`deleted_at` IS NULL ';

		$results['canje']= [];
		$results['canje']['global']=[];
		$results['canje']['usr'] = [];
		$canje = DB::select($queryCanje);
		$results['canje']['global']['siempre'] = $canje[0];
		$canje = DB::select($queryCanje . ' AND canje.userId = '.$user->id);
		$results['canje']['usr']['siempre'] = $canje[0];
    	$canje = DB::select($queryCanje . ' AND canje.`created_at` > (NOW() - INTERVAL 1 MONTH)');
    	$results['canje']['global']['lastMonth'] = $canje[0];
    	$canje = DB::select($queryCanje . ' AND canje.`created_at` > (NOW() - INTERVAL 1 MONTH) AND canje.userId = '.$user->id);
		$results['canje']['usr']['lastMonth'] = $canje[0];

    	$query = ' SELECT SUM((SELECT SUM(puntos_compra.`puntosCompra`) 
					FROM puntos_compra 
					WHERE puntos_compra.`compraItemId` = compra_item.id)) as sumCanjes,
					day(compra_item.`created_at`) as day
					FROM  compra_item
					WHERE day(compra_item.`created_at`) = DAY(NOW() - INTERVAL %s DAY )
					GROUP BY day
					';

		$canjesHoy = DB::select(sprintf($query,0));


		!empty($canjesHoy) && ($results['compra']['global']['hoy'] = $canjesHoy[0]->sumCanjes);

		$results['clientes']['count'] = DB::select('SELECT count(1) as cantidadClientes FROM cliente WHERE cliente.`deleted_at` IS NULL')[0]->cantidadClientes;
		$results['articulos']['count'] = DB::select('SELECT count(1) as cantidadArticulos FROM articulo WHERE articulo.`deleted_at` IS NULL')[0]->cantidadArticulos;
		$query = 'SELECT COUNT(*) as comprasMesPasado FROM compra WHERE month(created_at) = MONTH(DATE_SUB(NOW(),INTERVAL 1 MONTH))';
		$results['compra']['comparativa']['haceUnMes']= DB::select($query)[0]->comprasMesPasado;
		$query = 'SELECT COUNT(*) as comprasDosMeses FROM compra WHERE month(created_at) = MONTH(DATE_SUB(NOW(),INTERVAL 2 MONTH))';
		$results['compra']['comparativa']['haceDosMeses']= DB::select($query)[0]->comprasDosMeses;
		$query = 'SELECT COUNT(*) as esteMes FROM compra WHERE month(created_at) = MONTH(NOW())';
		$results['compra']['comparativa']['esteMes']= DB::select($query)[0]->esteMes;

		$query = 'SELECT COUNT(*) as count, query 
			FROM catalogo_queries
			GROUP BY catalogo_queries.`query`
			ORDER BY count DESC
			LIMIT 5';
		$results['catalogoQueries']= DB::select($query);
    	
    	return response()->json($results);
    }
}
