<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\cliente;
use App\tarjeta;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\clientPost;
use App\Http\Requests\clientPut;
use App\Http\Requests\reporteFilter as ReporteFilterRequest;
use App\Http\Requests\transaccionesFilter as transaccionesFilterRequest;
use App\Helpers;
use App\Exports\ClientesExport;
use App\Exports\rankingExport;
use App\compra;
use App\canje;


class clientesController extends Controller
{
    public function getAll(Request $request)
    {
        if($request->has('queryString'))
        {
            return cliente::where('nombre','like','%' . $request->queryString . '%')
                ->orWhere('apellido','like','%' . $request->queryString . '%')
                ->orWhere('mail','like','%' . $request->queryString . '%')->paginate(15);
        }
    	return response()->json(cliente::paginate(15));
    }
    public function get(Request $request)
    {
        if($request->idMagnetico OR ($request->dni OR $request->nombre))
        {
            return $this->search($request);
        }
        $cliente = cliente::find($request->id);
        if(empty($cliente))
        {
            return response('Not found',404);
        }
        return response()->json($cliente);
    }
    public function search(Request $request)
    {
        if($request->idMagnetico)
        {
            $tarjeta = tarjeta::where('idMagnetico', $request->idMagnetico)->first();
            if($tarjeta && $tarjeta->cliente)
            {
                return response()->json($tarjeta->cliente);
            }
        }
        if($request->dni)
        {
            if(!empty($foundCliente = cliente::where('dni',$request->dni)->first()))
            {
                return response()->json($foundCliente);
            }
        } else if($request->nombre)
        {
            $queryes = [$request->nombre];
            $queryes = array_merge($queryes,preg_split('/[\s]+/', $request->nombre));
            foreach ($queryes as $key) 
            {
                if(!empty($foundCliente = cliente::where('nombre',$key)
                ->orWhere('nombre', 'like', '%' . $key . '%')
                ->orWhere('apellido', 'like', '%' . $key . '%')
                ->first()))
                {
                    return response()->json($foundCliente);
                }    
            }
        }
        return response()->json([],404);
    }
    public function create(clientPost $request)
    {
        $request = $request->all();
        $errors = [];
        
        
        if(isset($request['idmagtarjeta'])) {
            if(tarjeta::idExists($request['idmagtarjeta']))
            {
                $errors['idmagtarjeta'] =['El id de la tarjeta ya existe '];
            } else {
                $tarjeta = new tarjeta(['idMagnetico'=>$request['idmagtarjeta']]);
                unset($request['idmagtarjeta']);
            }
        }
        

        if((cliente::where('mail',$request['mail'])->get()->first())!=NULL)
        {
            $errors['mail'] = ['Mail ya esta registrado'];
        }
        
        if(!empty($errors))
        {
            return response()->json(['errors'=>$errors], 422);
        }
        
        $request['nacimiento'] = \App\Helpers::formatDate($request['nacimiento']);
        $cliente = new cliente($request);
        
        $response = [ 'alta'=> $cliente->save() ];
        
        if(isset($tarjeta)){
            $response['tarjeta'] = $cliente->tarjeta()->save($tarjeta);
        }

        return response()->json($response);
    }
    public function delete(Request $request)
    {
        cliente::destroy($request->id);
        return response(['delete'=>cliente::withTrashed()->where('id',$request->id)->get()->first()->trashed()]);
    }
    public function put(clientPut $request)
    {
        $updatable = ["apellido", "dni" ,"mail" ,"nombre","telefono","calle","altura","depto","piso","ciudad_id","estadoCivil","codigoPostal","genero",'idmagtarjeta'];

        $cliente = cliente::find($request['id']);
        
        if($cliente->idmagtarjeta != $request->idmagtarjeta)
        {
            if(tarjeta::idExists($request['idmagtarjeta']))
            {
                 $errors['idmagtarjeta'] =['El id de la tarjeta ya existe '];
            }
        }
        if($cliente->mail != $request->mail)
        {
            if((cliente::where('mail',$request['mail'])->get()->first())!=NULL)
            {
                $errors['mail'] = ['Mail ya esta registrado'];
            }
        }

        if(!empty($errors))
        {
            return response()->json(['errors'=>$errors],422);
        }

        foreach ($updatable as $key) 
        {
            if($cliente->key != $request->$key)
            {
                $cliente->$key = $request->$key;    
            }
        }
        $cliente->nacimiento =  \App\Helpers::formatDate($request->nacimiento);

        $cliente->push();
        return response()->json(['update'=>$cliente]);
    }
    public function export(Request $request)
    {
        $clientesExport = new ClientesExport;
        return Helpers::export($clientesExport,$request->format,'clientesExport');
    }
    public function ranking(Request $request)
    {
        return DB::table('puntos_cliente')->paginate(15);
    }
    public function exportRanking(Request $request)
    {
        $rankingExport = new rankingExport;
        return Helpers::export($rankingExport,$request->format,'rankingExport');
    }
    public function reporteCanjes(ReporteFilterRequest $request)
    {
        $PAGE_SIZE = env('PAGINATION_SIZE');
        $selectPattern = 'SELECT SUM(puntos_canje.`puntosCanje` * puntos_canje.cantidad) 
                            AS puntos_canjeados_totales, cliente.* 
                            FROM puntos_canje
                            JOIN canje_item
                            ON canje_item.id = puntos_canje.`canjeItemId`
                            JOIN canje
                            ON canje_item.`canjeId` = canje.id
                            JOIN cliente
                            ON cliente.id = canje.`clienteId`
                             %s 
                            GROUP BY cliente.`id`
                             %s ';
        $whereClause = '';
        $limitOffsetClause = '';
        if($request->has('page'))
        {
            $limit = $request->page * $PAGE_SIZE;
            $offset = $limit - $PAGE_SIZE;
        }else
        {
            $limit = $PAGE_SIZE;
        }
        $limitOffsetClause = sprintf(' LIMIT %d OFFSET %d ',$limit,$offset);
        
        $wheres = [];
        $wheres[] = ' canje.`deleted_at` IS NULL ';
        if($request->has('fromDate'))
        {
            $wheres[] = ' canje.`created_at` > "' . Helpers::formatDate($request->fromDate) . '" ';
        }
        if($request->has('toDate'))
        {
            $wheres[] = ' canje.`created_at` < "' . Helpers::formatDate($request->toDate). '" ';
        }

       
        $whereClause = ' WHERE ' . implode(' AND ', $wheres);
        

        $selectQuery = sprintf($selectPattern,$whereClause,$limitOffsetClause);

        return response()->json(['data'=>DB::select($selectQuery)]) ;
    }

    public function transactions(transaccionesFilterRequest $request)
    {
        $cliente = cliente::find($request->clienteId);
        $limit = env('PAGINATION_SIZE') * $request->input('page', 1);;
        $offset = $limit - env('PAGINATION_SIZE');
        $params = [
                    'limit'=>$limit,
                    'offset'=>$offset,
                    'fromDate'=>$request->fromDate,
                    'toDate'=>$request->toDate
                ];
 
        return response()->json($cliente->getTransacciones($params) );
    }




























}
