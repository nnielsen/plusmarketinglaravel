<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User as usuario;
use Illuminate\Support\Facades\Hash;
use App\Mail\passwordRecoveryMail as recoveryMail;
use Illuminate\Support\Facades\Mail;

class passwordRecoveryController extends Controller
{
    public function sendMail(Request $request)
    {
    	$usuario = usuario::where('mail','=',$request->mail)->get()->first();

    	$newPassword =  uniqid();
    	$usuario->password = Hash::make($newPassword);

    	if($usuario->save())
    	{
    		$usuario->unencryptedPassword = $newPassword;
    		Mail::to($usuario->mail)
		    ->queue(new recoveryMail($usuario,$newPassword));
    		return redirect('/login?recovery=true');
    	}
    }
}
