<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\articulo;
use App\articuloCategoria;
use App\configuracionInstancia;
use Illuminate\Support\Facades\DB;

class catalogoController extends Controller
{
    public function index(Request $request)
    {
    	$instancia = configuracionInstancia::getInstancia();
        $requestAll = $request->all();
    	if(isset($requestAll['query']))
        {
        	$query = $request->all()['query'];

            DB::table('catalogo_queries')->insert(['query' => strtoupper($query)]);
            
            $articulos = articulo::where('nombre','like','%' . $query . '%')
                		->orWhere('descripcion','like','%' . $query . '%')->paginate(15);
            $scope['query'] = $query;
        }
        else if(isset($requestAll['category']))
        {
            $categoria = articuloCategoria::find($requestAll['category']);
            $scope['invertedCategoryTree'] = array_reverse($categoria->getFullAncestorsTree());
            $categoriasHijo = [$categoria->id];
            foreach ($categoria->getFullSons() as $key) 
            {
                $categoriasHijo[]=$key->id;
            }
            $articulos = articulo::whereIn('categoriaId',$categoriasHijo)->paginate(15);
            
            if(($subCategorias= articuloCategoria::where('parentId','=',$requestAll['category'])->get())->count() != 0)
            {
                $scope['subCategorias'] = $subCategorias;
            }
        }
        else
        {
        	$articulos = articulo::paginate(15);	
        }

        $scope['categorias'] = articuloCategoria::whereNull('parentId')->get();

        $scope['articulos']=$articulos;
        $scope['instancia']=$instancia;
    	return view('catalogoIndex',$scope);
    }
    protected function checkCallbacks($request) {
        
    }
    public function item(Request $request)
    {
        $instancia = configuracionInstancia::getInstancia();
        $this->checkCallbacks($request);

    	$articulo = articulo::find($request->id);
    	$articulo->visitasCatalogo++;
    	$articulo->save();
        $scope = 
        [
            'articulo'=>$articulo,
            'instancia'=>$instancia
        ];
        if($articulo->categoriaTree)
        {
            $invertedCategoryTree = array_reverse($articulo->categoriaTree);   
            $scope['invertedCategoryTree'] = $invertedCategoryTree;
        }
        $scope['categorias'] = articuloCategoria::whereNull('parentId')->get();
    	return view('catalogoItemDetalle',$scope);
    }
}
