<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\metodoIngreso;
use JWTAuth;
use Illuminate\Support\Facades\Log;


class metodosIngresoController extends Controller
{
    public function getAll(Request $request)
    {
        /*
            $manualMethod = new \stdClass();
            $manualMethod->id = 0;
            $manualMethod->nombre = 'Manual';
            $results = metodoIngreso::all();
            $results->prepend($manualMethod);
        */
    	return response()->json(['todo'=>metodoIngreso::all()]);
    }
    public function getByAuth()
    {
        $user = JWTAuth::user();
        return response()->json(metodoIngreso::find($user->inputMethodId));
    }
    public function setByAuth(Request $request)
    {
        $user = JWTAuth::user();
        $user->inputMethodId = $request->id;
        return response()->json(['inputMethodSetted'=>$user->save()]);
    }
    public function get(Request $request)
    {
        return response()->json(metodoIngreso::find($request->id));
    }
    public function create(Request $request)
    {
        $request = $request->all();
        $mi = new metodoIngreso($request);
        return response()->json(['alta'=>$mi->save()]);
    }
    public function put(Request $request)
    {
    	$mi = metodoIngreso::find($request->id);
    	$updatable = ['inicio','cierre','nombre'];
    	foreach ($updatable as $key) {
    		if($mi->$key != $request->$key)
    		{
    			$mi->$key = $request->$key;
    		}
    	}
    	return response()->json(['update'=>$mi->save()]);
    }
    public function delete(Request $request)
    {
        metodoIngreso::destroy($request->id);
        return response(['delete'=> metodoIngreso::withTrashed()->where('id',$request->id)->get()->first()->trashed()]);
    }
}
