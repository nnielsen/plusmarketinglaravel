<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\compra as Compra;
use App\cliente;
use App\User;
use App\itemCompra;
use App\Events\compraRealizada as CompraRealizadaEvent;
use Illuminate\Support\Facades\DB;
use JWTAuth;
use App\Http\Requests\compraPost;
use App\configuracionInstancia;

class compraController extends Controller
{
    public function getAll(Request $request)
    {
    	return response()->json(Compra::all());
    }
    public function getItems(Request $request)
    {
        $compra = Compra::find($request->id);
        return response()->json($compra->items);
    }
    public function getHeader(Request $request)
    {
        $compra = Compra::find($request->id);
        return response()->json($compra);
    }

    public function deleteItem(Request $request)
    {
        $key = 'delete';
        $item = itemCompra::find($request->id);
        $compra = $item->compra;
        if($compra->items->count() == 1)
        {
            $success = $item->delete() && $compra->delete();
            $key = 'deleteParent';
        }
        else
        {
            $success = $item->delete();
        }

        return response()->json([$key=>$success]);
    }
    public function editItem(Request $request)
    {
        $item = itemCompra::find($request->id);
        $keys = ['descripcion','monto'];
        foreach ($keys as $key) {
            $item->$key = $request->$key;
        }
        $item->save();
        return response()->json(['item'=>$item]);
    }
    public function delete(Request $request)
    {
        $compra = Compra::find($request->id);
        $cliente = $compra->cliente;
        if(($cliente->puntos - $compra->puntos)<0)
        {
            return response()->json(['error'=>'No se puede realizar lo solicitado.'],403);
        }

        return response()->json(['delete'=>$compra->delete()]);
    }
    public function create(compraPost $request)
    {
        $configInstancia = configuracionInstancia::getInstancia();
        $urlInstancia = $request->route()->parameter('instanceUrl');

    	$request = $request->all();
    	$cliente = cliente::find($request['clienteId']);

        $itemsCompra = [];
        if(empty($request['purchaseItems']))
        {
            return response()->json(['error'=>'Debe ingresar al menos un item de compra'],403);
        }
        foreach ($request['purchaseItems'] as $itemCompra) {
            if(empty($itemCompra['monto']) OR empty($itemCompra['descripcion']))
            {
                return response()->json(['error'=>'Debe ingresar monto y descripcion en todos los items'],403);
            }
            $itemsCompra[] = new itemCompra($itemCompra);
        }

    	//TODO GET USER
        $user = JWTAuth::user();

    	$compra = new Compra();
    	$compra->user()->associate($user);
    	$compra->cliente()->associate($cliente);
    	$compra->save();

		$compra->items()->saveMany($itemsCompra);
        
        $response = [
            'compra'=>$compra->push(),
            'id'=>$compra->id,
            'cliente'=>$cliente,
            'errors'=>[]
        ]; 
        

        try{
            event(new CompraRealizadaEvent($compra, $configInstancia, $urlInstancia));
        }catch(\Swift_TransportException $e)
        {
            $response['errors']['transport_exception'] = true;
            if(env('APP_DEBUG')=='true')
            {
                $response['errors']['transport_exception_debug'] = $e->getTrace();
            }
        }
        catch(\Exception $e)
        {
            $response['errors']['unknown'] = 'Un error desconocido en el envio del mail al cliente.';
        }
        
        
        return response()->json($response);
    }
    public function comprobante(Request $request)
    {
        $compra = compra::find($request->id);
        $configInstancia = configuracionInstancia::getInstancia();
        $configInstancia->registerCallbackUrl($request->route()->parameter('instanceUrl'));

        return view('compraRealizadaMail', [
            'compra'=>$compra,
            'cliente'=>$compra->cliente,
            'itemsCompra'=>$compra->items,
            'configInstancia' => $configInstancia,
            'catalogoUrl' => $configInstancia->catalogoUrl
        ]);
    }
    public function getDescripcionSugerencia(Request $request)
    {   
        if($request->has('queryString'))
        {
            $results = DB::select(
                'SELECT compra_item.`descripcion`, compra_item.`monto` 
                FROM compra_item 
                WHERE compra_item.`descripcion` 
                LIKE "%'.$request->queryString.'%" 
                LIMIT 5'
            );

            return response()->json($results);
        }
        
    }
}
