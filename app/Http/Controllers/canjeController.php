<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\canje as Canje;
use App\itemCanje;
use App\cliente;
use App\User;
use App\Events\canjeRealizado as canjeRealizadoEvent;
use JWTAuth;
use App\configuracionInstancia;
use Illuminate\Support\Facades\DB;

class canjeController extends Controller
{
    public function delete(Request $request)
    {
        $canje = Canje::find($request->id);
        return response()->json(['delete'=>$canje->delete()]);
    }
    public function getItems(Request $request)
    {
        $canje = Canje::find($request->id);
        return response()->json($canje->items);
    }
    public function deleteItem(Request $request)
    {
        $key = 'delete';
        $item = itemCanje::find($request->id);
        $canje = $item->canje;
        if($canje->items->count() == 1)
        {
            $success = $item->delete() && $canje->delete();
            $key = 'deleteParent';
        }else
        {
            $success = $item->delete();
        }
        return response()->json([$key=>$success]);
    }
    public function comprobante(Request $request)
    {
        $canje = canje::find($request->id);
        $configInstancia = configuracionInstancia::getInstancia();
        $configInstancia->registerCallbackUrl($request->route()->parameter('instanceUrl'));

        return view('canjeRealizadoMail', [
            'canje'=>$canje,
            'cliente'=>$canje->cliente,
            'itemsCanje'=>$canje->items,
            'configInstancia' => $configInstancia,
            'catalogoUrl' => $configInstancia->catalogoUrl
        ]);
    }
    public function create(Request $request)
    {
        $configInstancia = configuracionInstancia::getInstancia();
        $urlInstancia = $request->route()->parameter('instanceUrl');

    	$request = $request->all();
    	$cliente = cliente::find($request['clienteId']);

    	//TODO GET USER
    	$user = JWTAuth::user();
        
        $itemsCanje = [];
        $puntosCanje = 0;
        foreach ($request['swapItems'] as $itemCanje) {
            if(!isset($itemCanje['cantidad']))
            return response()->json(['error'=>'Error en cantidad de ' .$itemCanje['nombre']],409);
            $itemsCanje[] = new itemCanje( 
                [
                    'articuloId' => $itemCanje['id'],
                    'cantidad' => $itemCanje['cantidad']
                ]
            );
        }
        if(!Canje::checkValido($itemsCanje,$cliente))
        {
            return response()->json(['error'=>'El canje es invalido por puntos insuficientes'],409);
        }
        DB::beginTransaction();
    	$canje = new Canje();
    	$canje->user()->associate($user);
        $canje->cliente()->associate($cliente);
        $canje->save();
		$canje->items()->saveMany($itemsCanje);

        foreach ($canje->items as $itemCanje) {
            if(!$itemCanje->articulo->placeCanje($itemCanje)){
                $descripcionArticulo = $itemCanje->articulo->nombre;
                DB::rollBack();
                return response()->json(
                    [
                        'error'=>'El canje es invalido por stock insuficiente de ' . 
                        $itemCanje->articulo->nombre
                    ],
                409);
            }
        }

        $response = [
            'canje'=>$canje->push(),
            'cliente'=>$cliente,
            'id'=>$canje->id
        ];

        try{
            event(new canjeRealizadoEvent($canje, $configInstancia, $urlInstancia));
        }catch(\Swift_TransportException $e)
        {
            $response['errors']['transport_exception'] = true;
        }
        catch(\Exception $e)
        {
            $response['errors']['unknown'] = 'Un error desconocido en el envio del mail al cliente.';
        }
        DB::commit();
        return response()->json($response);
    }
}
