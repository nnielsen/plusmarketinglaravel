<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Validator, DB, Hash, Mail;
use Illuminate\Support\Facades\Password;
use Illuminate\Mail\Message;
use App\Http\Resources\userLoginResource;
use App\configuracionInstancia;


class AuthController extends Controller
{
    public function login(Request $request)
    {
        $credentials = $request->only('alias', 'password');
        
        $rules = [
            'alias' => 'required',
            'password' => 'required',
        ];
        $validator = Validator::make($credentials, $rules);
        if($validator->fails()) {
            return response()->json(['success'=> false, 'error'=> $validator->messages()], 401);
        }
        
        $user = User::where('alias',$credentials['alias'])->get()->first();

        if(!isset($user))
        {
            return $this->errorLogin();
        }

        if(!Hash::check($credentials['password'], $user->password))
        {
			return $this->errorLogin();
        }

        //(Hash::check('plain-text', $hashedPassword))
        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::fromUser($user)) {
                return $this->errorLogin();
            }
        } catch (JWTException $e) {
            return $this->errorLogin();
        }
        $response = ['success' => true,  'token' => $token,'user'=> new userLoginResource($user)];

        if($user->acceptedTermsConditions!=1)
        {
            $configuracionInstancia = configuracionInstancia::getInstancia();
            $response['termsAndConditions'] = $configuracionInstancia->terminosCondiciones;
        }
        return response()->json($response, 200);
    }
    private function errorLogin()
    {
        return response()->json(['success'=> false, 'error'=> 'Error de login'], 401);
    }
    public function logout(Request $request)
    {
        /*
        $token = JWTAuth::getToken();
    	try {
            JWTAuth::invalidate($token);
            return response()->json(['success' => true]);
        } catch (JWTException $e) {
            return response()->json(['success' => false, 'error' => 'Error en cierre de sesion'], 500);
        }
        */
    }

}
