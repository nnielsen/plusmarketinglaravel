<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;
use App\articuloPuntos;

class articulo extends Model
{
    use SoftDeletes;
    
    protected $table = 'articulo';
    protected $fillable = ['descripcion','nombre','puntos','stock','categoriaId'];
    protected $appends = ['puntos','stock','imgUrl','categoriaTree','thumbUrl'];

    public function getPuntos()
    {
        return $this->hasOne('App\articuloPuntos','articuloId')
                    ->orderBy('created_at', 'desc')
                    ->limit(1);
    }
    public function getStock()
    {
        return $this->hasOne('App\articuloStock','articuloId')
                    ->orderBy('created_at', 'desc')
                    ->limit(1);
    }
    public function placeCanje($itemCanje)
    {
        if($this->getStock->amount < $itemCanje->cantidad) {
            return false;
        }
        $this->getStock->amount -= $itemCanje->cantidad;
        $this->getStock->save();
        return true;
    }
    public function getImg()
    {
        return $this->hasOne('App\file','id','imagenId');
    }
    public function getCategoria()
    {
        return $this->hasOne('App\articuloCategoria','id','categoriaId');
    }
    public function getCategoriaTreeAttribute()
    {
        if($this->getCategoria)
        {
            return $this->getCategoria->getFullAncestorsTree();    
        }
    }
    public function getImgUrlAttribute()
    {
        if(!empty($this->getImg))
        {
            return $this->getImg->nombre;
        }
    }
    public function getThumbUrlAttribute()
    {
        if(!empty($this->getImg))
        {
            return 'imgs' . DIRECTORY_SEPARATOR . 'articulos' . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $this->getImg->filename;
        }
    }
    public function getStockAttribute()
    {	
    	return $this->getStock->amount;
    }
    public function setStockAttribute($value)
    {
        $this->getStock->amount = $value;
    }
    public function getPuntosAttribute()
    {	
    	return $this->getPuntos->puntos;
    }
    public function setPuntosAttribute($value)
    {   
        $articuloPuntos = new articuloPuntos(['puntos'=>$value]);
        $articuloPuntos->articulo()->associate($this);
        $articuloPuntos->save();
    }
}
