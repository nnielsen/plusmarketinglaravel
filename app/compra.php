<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class compra extends Model
{
    use SoftDeletes;
    protected $table = 'compra';
    protected $fillable = ['items','user'];

    public function user()
    {
        return $this->belongsTo('App\User','userId','id');
    }
    public function getPuntosAttribute()
    {
        return DB::select('SELECT SUM(puntos_compra.`puntosCompra` * puntos_compra.cantidad)
                AS puntosCompra
                FROM puntos_compra 
                WHERE puntos_compra.`compraItemId` IN
                    (SELECT compra_item.id
                    FROM compra_item
                    RIGHT JOIN compra
                    ON compra.`id` = compra_item.`compraId`
                    WHERE compra.`deleted_at` IS NULL 
                    AND compra.`id` = ' . $this->id .')')[0]->puntosCompra;
    }
    public function cliente()
    {
        return $this->belongsTo('App\cliente','clienteId','id');
    }
    public function items()
    {
        return $this->hasMany('App\itemCompra','compraId');
    }
}
