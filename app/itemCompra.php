<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class itemCompra extends Model
{
    use SoftDeletes;
    protected $table = 'compra_item';
    protected $fillable = ['descripcion','monto','cantidad'];
    protected $appends = ['puntos'];

    public function compra()
    {
        return $this->belongsTo('App\compra','compraId','id');
    }
    public function getPuntosAttribute()
    {
        return DB::select('SELECT puntos_compra.`puntosCompra`
                            FROM puntos_compra
                            WHERE puntos_compra.`compraItemId` = '.$this->id)[0]->puntosCompra;
    }
}
