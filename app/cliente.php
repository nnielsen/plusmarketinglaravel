<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use App\compra as Compra;
use App\instanceManager;
use App\Helpers;

class cliente extends Model
{
    use SoftDeletes;
    protected $table = 'cliente';
    protected $appends = ['puntos','cantidadCompras','compras','puntosCanjeados','cantidadCanjes',"estadoCivilNombre","generoNombre",'idmagtarjeta',"ubicacion"];
    protected $fillable = ["apellido", "dni", "idmagtarjeta" ,"mail" ,"nacimiento","nombre","telefono","calle","altura","depto","piso","ciudad_id","estadoCivil","codigoPostal","genero"];


    public function tarjeta()
    {
        return $this->hasOne('App\tarjeta','clienteId');
    }
    public function getUbicacionAttribute()
    {
        return Helpers::getUbicacionString($this->ciudad_id);
    }
    public function getPuntosAttribute()
    {
    	$puntosCompra = DB::select('
            SELECT SUM(puntos_compra.`puntosCompra` * puntos_compra.cantidad)
                AS puntosCompra
                FROM puntos_compra 
                WHERE puntos_compra.`compraItemId` IN
                    (SELECT compra_item.id
                    FROM compra_item
                    RIGHT JOIN compra
                    ON compra.`id` = compra_item.`compraId`
                    WHERE compra.`deleted_at` IS NULL 
                    AND compra_item.`deleted_at` IS NULL 
                    AND compra.`clienteId` =  ' . $this->id . ')')[0]->puntosCompra;
        return $puntosCompra - $this->puntosCanjeados;
    }
    public function getCantidadComprasAttribute()
    {
        return 
            DB::table('compra')
             ->select(DB::raw('count(*) as cantidadCompras'))
             ->where('clienteId', '=', $this->id)
             ->whereNull('deleted_at')
             ->get()
             ->first()->cantidadCompras;
    }
    public function getIdmagtarjetaAttribute()
    {
        if($this->tarjeta)
        {
            return $this->tarjeta->idMagnetico;
        }
        return NULL;
    }
    public function setIdmagtarjetaAttribute($value)
    {
        if(!$this->tarjeta)
        {
            $tarjeta = new tarjeta(['idMagnetico'=>$value]);
            $this->tarjeta()->save($tarjeta);
        }
        else{
            $this->tarjeta->idMagnetico = $value;
        }
        
    }
    public function getEstadoCivilNombreAttribute($value)
    {
    	$civilStates = \Config::get('constants.civilStates');

    	foreach($civilStates as $civilState)
    	{
    		if($civilState['value'] == $value)
    		{
    			return __('constants.'.$civilState['name']);
    		}
    	}
    }
    public function getGeneroNombreAttribute($value)
    {
    	$genders = \Config::get('constants.genders');

    	foreach($genders as $gender)
    	{
    		if($gender['value'] == $value)
    		{
    			return __('constants.'.$gender['name']);
    		}
    	}
    }
    public function getComprasAttribute()
    {
    	return $this->hasMany('App\compra','clienteIds');
    }
    public function getPuntosCanjeadosAttribute()
    {
    	return 
        DB::select('SELECT SUM(puntos_canje.`puntosCanje` * puntos_canje.cantidad)
            AS puntosCanje
            FROM puntos_canje
            WHERE puntos_canje.`canjeItemId` IN
                (SELECT canje_item.id
                FROM canje_item
                RIGHT JOIN canje
                ON canje.`id` = canje_item.`canjeId`
                WHERE canje.`deleted_at` IS NULL 
                AND canje.`clienteId` = ' . $this->id . ')')[0]->puntosCanje;

    }
    public function getCanjesAttribute()
    {
        return 0;
    }
    public function getCantidadCanjesAttribute()
    {
    	return 
            DB::table('canje')
             ->select(DB::raw('count(*) as cantidadCanjes'))
             ->where('clienteId', '=', $this->id)
             ->whereNull('deleted_at')
             ->get()
             ->first()->cantidadCanjes;
    }
    public function getTransacciones($params)
    {
        extract($params);
        $wheres = [];
        $wheres[] = "clienteId = " . $this->id;
        if($limit == null)
        {
            $limit = 15;
        }
        if(!empty($fromDate))
        {
            $wheres[] = ' created_at > "' . Helpers::formatDate($fromDate) . '" ';
        }
        if(!empty($toDate))
        {
            $wheres[] = ' created_at < "' . Helpers::formatDate($toDate) . '" ';
        }

        $wheres = implode($wheres, " AND ");

        $totalQuery = 'SELECT 
                        ((SELECT count(*) 
                        FROM compra WHERE %s )
                            +
                        (SELECT count(*) FROM canje WHERE %s ))
                        as total';
        $totalQuery = sprintf($totalQuery,$wheres,$wheres);

        $totalPages = ceil(DB::select($totalQuery)[0]->total
                        /15);

        $query = '  
            SELECT compra.id as idCompra, compra.`created_at`, 
                    compra.`clienteId`, 
                    NULL as idCanje,compra.`deleted_at`,
                    COALESCE((SELECT SUM(puntos_compra.`puntosCompra` * puntos_compra.cantidad) 
                        FROM puntos_compra 
                        WHERE puntos_compra.`compraItemId` 
                        IN (SELECT compra_item.id 
                            FROM compra_item
                            WHERE compra_item.`compraId` = compra.`id`
                            AND compra_item.`deleted_at` IS NULL)
                        ),0)
                     as puntos
                    FROM compra
                    WHERE %s
            UNION 
            SELECT  NULL as idCompra, 
                        canje.`created_at`, canje.`clienteId`, 
                        canje.id as idCanje, canje.`deleted_at`,
                        COALESCE(
                        (SELECT SUM(puntos_canje.`puntosCanje` * puntos_canje.cantidad)
                            FROM puntos_canje
                            WHERE puntos_canje.`canjeItemId`
                            IN (SELECT canje_item.id
                                FROM canje_item
                                WHERE canje_item.`canjeId` = canje.`id`
                                AND canje_item.`deleted_at` IS NULL)
                        ),0) as puntos
                    FROM canje
            WHERE %s
            ORDER BY created_at DESC
            LIMIT %s
        ';
        if(!empty($offset))
        {
            $query .= ' OFFSET '. $offset;
        }
        $todo = DB::select(sprintf($query,$wheres,$wheres,$limit,$offset));

        return ['todo'=>$todo,'totalPages'=>$totalPages];
    }
}
