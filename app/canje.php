<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class canje extends Model
{
	use SoftDeletes;
    protected $table = 'canje';
    
    public static function getPuntosTotales($items) {
        $puntosTotales = 0;
        foreach($items as $item) {
            $puntosTotales += $item->articulo->puntos * $item->cantidad;
        }
        return $puntosTotales;
    }
    public static function checkValido($items,$cliente)
    {
        return $cliente->puntos >= self::getPuntosTotales($items);
    }
    public function user()
    {
        return $this->belongsTo('App\User','userId','id');
    }
    public function cliente()
    {
        return $this->belongsTo('App\cliente','clienteId');
    }
    public function getPuntosAttribute() {
        return self::getPuntosTotales($this->items);
    }
    public function items()
    {
        return $this->hasMany('App\itemCanje','canjeId');
    }
}
