<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Illuminate\Support\Facades\Schema;
use App\instanceManager;

class installInstanceManager extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'install:instanceManager';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Creando router de DBs para multiples instancias');
        $instanceManager = instanceManager::getInstance();
        if($exception = $instanceManager->createInstanceManager())
        {
            $this->info(sprintf('Bases creadas satisfactoriamente'));

            if($this->confirm('Crear instancia DEMO?'))
            {
                 $instanceManager->createInstance(
                    [
                        'url'=>'demo',
                        'adminusr'=>'admin',
                        'adminpass'=>'admin',
                        'businessName'=>'DEMO'
                    ]);
            }
        }else
        {
            $this->error(sprintf('Fallo la creacion del esquema de datos->%s', $exception->getMessage()));
        }
    }
}
