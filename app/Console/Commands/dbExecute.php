<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\instanceManager;

class dbExecute extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:execute {sql} {--database=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Execute DB command in instances';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sql = "DROP VIEW puntos_canje;
        CREATE VIEW puntos_canje AS
        SELECT  canje_item.id as canjeItemId,
        (SELECT articulo_puntos.`puntos` as articuloPuntos 
        FROM articulo_puntos
        WHERE articulo_puntos.`articuloId` = canje_item.`articuloId`
        AND canje_item.`created_at` > articulo_puntos.`created_at`
        ORDER BY articulo_puntos.`created_at` DESC
        LIMIT 1) as puntosCanje,
        canje_item.cantidad
        FROM canje_item;
        DROP VIEW puntos_compra;
        CREATE VIEW puntos_compra AS
        SELECT  compra_item.id as compraItemId,
        compra_item.`monto` *
        (SELECT puntos_peso.`coeficiente`
        FROM puntos_peso
        WHERE compra_item.`created_at` >= puntos_peso.`created_at`
        ORDER BY puntos_peso.`created_at` DESC
        LIMIT 1) as puntosCompra,
        compra_item.cantidad
        FROM compra_item;
        DROP VIEW IF EXISTS puntos_cliente;
        CREATE VIEW puntos_cliente AS
        SELECT cliente.*, COALESCE((
        SELECT SUM(puntos_compra.`puntosCompra` * puntos_compra.cantidad)
        AS puntosCompra
        FROM puntos_compra 
        WHERE puntos_compra.`compraItemId` IN
        (SELECT compra_item.id
        FROM compra_item
        RIGHT JOIN compra
        ON compra.`id` = compra_item.`compraId`
        WHERE compra.`clienteId` = cliente.id
        AND compra.`deleted_at` IS NULL
        AND compra_item.`deleted_at` IS NULL )
        ),0) - COALESCE(
        (
        SELECT SUM(puntos_canje.`puntosCanje` * puntos_canje.cantidad)
        AS puntosCanje
        FROM puntos_canje
        WHERE puntos_canje.`canjeItemId` IN
        (SELECT canje_item.id
        FROM canje_item
        RIGHT JOIN canje
        ON canje.`id` = canje_item.`canjeId`
        WHERE canje.`clienteId` = cliente.id
        AND canje.`deleted_at` IS NULL)
        ),0) as puntos_totales 
        FROM cliente
        HAVING puntos_totales != 0
        ORDER BY puntos_totales DESC;
        ALTER TABLE canje_item ADD cantidad int;
        ALTER TABLE compra_item ADD cantidad int;
        ";
        $instanceManager = new InstanceManager;
        //$database = $this->argument('database');
        $instanceManager->executeSQL($sql);

    }
}
