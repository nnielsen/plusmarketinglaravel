<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\Mail;
use Illuminate\Console\Command;
use App\instanceManager;

class checkMailConnection extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mail:test {db}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        instanceManager::setDatabaseName(env('APP_NAME') . '_' . $this->argument('db'));
       
        Mail::send([], [], function ($message) {
          $message->to(env('MAIL_USERNAME'))
            ->subject('TEST')
            ->setBody('MAIL TEST FIDELIZACION ' . date('H:i:s Y-M-D')); 
        });
    }
}
