<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\instanceManager;

class installAppInstance extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'install:appInstance {--adminusr=} {--adminpass=} {--businessName=} {--url=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crear instancia de la App';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Creando instancia de App');
        $instanceManager = instanceManager::getInstance();
        $parametros = $this->options();

        if($instanceManager->createInstance($parametros))
        {
            $this->info("Creada satisfactoriamente");
        }else
        {
            $this->error("Error en la creacion");
        };
        
    }

}
