<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Instance extends Model
{
     protected $table = 'instance';
     
     protected $fillable = [
        'name', 'email', 'password',
    ];
}
