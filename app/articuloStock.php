<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class articuloStock extends Model
{
    protected $table = 'articulo_stock';
    protected $fillable = ['amount'];
    public function articulo()
    {
        return $this->belongsTo('App\articulo','articuloId');
    }
}
