<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class compraRealizada
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $compra, $configInstancia, $urlInstancia;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($compra, $configInstancia, $urlInstancia)
    {
        $this->compra = $compra;
        $this->configInstancia = $configInstancia;
        $this->urlInstancia = $urlInstancia;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
