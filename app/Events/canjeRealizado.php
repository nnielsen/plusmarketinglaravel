<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\canje as Canje;

class canjeRealizado
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $canje, $configInstancia, $urlInstancia;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Canje $canje, $configInstancia, $urlInstancia)
    {
        $this->canje = $canje;
        $this->configInstancia = $configInstancia;
        $this->urlInstancia = $urlInstancia;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
