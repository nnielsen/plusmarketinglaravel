<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\canje as Canje;
use App\configuracionInstancia;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\envioMail;

class canjeRealizado extends Mailable
{
    use Queueable, SerializesModels;

    public $canje, $cliente, $itemsCanje,$envio, $configInstancia, $catalogoUrl;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($event)
    {
        $this->canje = $event->canje;
        $this->itemsCanje = $this->canje->items()->get();
        $this->cliente = $this->canje->cliente;
        $this->configInstancia = $event->configInstancia;
        $this->configInstancia->registerCallbackUrl($event->urlInstancia);
        $this->catalogoUrl = $this->configInstancia->catalogoUrl;
    }
    public function setEnvio(envioMail $envio)
    {
        $this->envio = $envio;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('canjeRealizadoMail',
            [
                'canje'=>compact($this->canje),
                'cliente'=>compact($this->cliente),
                'envio'=>compact($this->envio),
                'configInstancia' => compact($this->configInstancia),
                'catalogoUrl' => $this->catalogoUrl
            ]);
    }
}
