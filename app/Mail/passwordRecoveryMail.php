<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User as Usuario;

class passwordRecoveryMail extends Mailable
{
    use Queueable, SerializesModels;

    public $usuario,$unencryptedPassword;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Usuario $usuario, $unencryptedPassword)
    {
        $this->usuario = $usuario;
        $this->unencryptedPassword = $unencryptedPassword;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Recuperación de contraseña')
        ->view('passwordRecoveryMail');
    }
}
