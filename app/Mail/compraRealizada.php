<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\compra;
use App\configuracionInstancia;
use App\envioMail;

class compraRealizada extends Mailable
{
    use Queueable, SerializesModels;

    public $compra,$itemsCompra,$cliente,$envio,$configInstancia, $catalogoUrl;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($event)
    {
        $this->compra = $event->compra;
        $this->itemsCompra = $this->compra->items()->get();
        $this->cliente = $this->compra->cliente;
        $this->configInstancia = $event->configInstancia;
        $this->configInstancia->registerCallbackUrl($event->urlInstancia);
        $this->catalogoUrl = $this->configInstancia->catalogoUrl;
    }
     public function setEnvio(envioMail $envio)
    {
        $this->envio = $envio;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('compraRealizadaMail',
        [
            'compra' => compact($this->compra),
            'cliente' => compact($this->cliente),
            'envio' => compact($this->envio),
            'configInstancia' => compact($this->configInstancia),
            'catalogoUrl' => $this->catalogoUrl
        ]);
    }
}
