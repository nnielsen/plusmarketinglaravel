<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\compraRealizada as CompraRealizadaEvent;
use Illuminate\Support\Facades\Mail;
use App\Mail\compraRealizada as CompraRealizadaMail;
use App\envioMail;

class compraMail implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(CompraRealizadaEvent $event)
    {
        $mail = new CompraRealizadaMail($event);
        $envioMail = envioMail::generateRecord($mail);
        $mail->setEnvio($envioMail);

        Mail::to($event->compra->cliente->mail)
        ->queue($mail);
    }
}
