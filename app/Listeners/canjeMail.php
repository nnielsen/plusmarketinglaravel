<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\canje as Canje;
use Illuminate\Support\Facades\Mail;
use App\Events\canjeRealizado as CanjeRealizadoEvent;
use App\Mail\canjeRealizado as CanjeRealizadoMail;
use App\envioMail;

class canjeMail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(CanjeRealizadoEvent $event)
    {
        $mail = new CanjeRealizadoMail($event);
        $envioMail = envioMail::generateRecord($mail);
        $mail->setEnvio($envioMail);
        
        Mail::to($event->canje->cliente->mail)
        ->queue($mail);
    }
}
