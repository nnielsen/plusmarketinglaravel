<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\metodoIngreso as MetodoIngreso;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable, SoftDeletes;

    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'alias','nombre','apellido','password','telefono','mail','inputMethodId','instanciaId','tipoId',
       'sucursal_id'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','instanciaId'
    ];
    protected $appends = ['metodoIngreso'];

    public function metodoIngresoObject()
    {
        return $this->hasOne('App\metodoIngreso','id','inputMethodId');
    }
    public function sucursal()
    {
        return $this->hasOne('App\sucursal','id','sucursal_id');
    }
    public function getTokenAttribute()
    {
        return $this->getJWTIdentifier();
    }
    public function getMetodoIngresoAttribute()
    {
        if($this->inputMethodId == '0')
        {
            return __('constants.manual');
        }else
        {
            return $this->metodoIngresoObject->nombre;
        }
    }
     /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getHashedHashAttribute()
    {
        return md5($this->password);
    }
    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [
            'password' => $this->getHashedHashAttribute()
        ];
    }

}
