<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class puntosPeso extends Model
{
    protected $table = 'puntos_peso';
    private static $instancia;
    protected $fillable = ['coeficiente'];

    public static function getInstancia($reload = false)
    {
    	if(!isset(self::$instancia) || $reload)
    	{
    		self::$instancia = self::orderBy('created_at', 'desc')->first();
    	}
    	return self::$instancia;
    }
}
