<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Mail\Mailable;

class envioMail extends Model
{
    protected $table = 'envio_mail';

    public static function generateRecord(Mailable $mail)
    {
    	$instancia = new self();
    	switch (get_class($mail)) {
    		case 'App\Mail\canjeRealizado':
    			$instancia->ext_id = $mail->canje->id;
    			$instancia->tipo = 'CANJE';
    			$instancia->enviado = date('Y-m-d H:i:s');
    			break;
            case 'App\Mail\compraRealizada':
                $instancia->ext_id = $mail->compra->id;
                $instancia->tipo = 'COMPRA';
                $instancia->enviado = date('Y-m-d H:i:s');
                break;
    		default:
    			# code...
    			break;
    	}
    	$instancia->save();
        return $instancia;
    }
    public function save(array $options = array())
    {
    	$this->timestamps = false;
    	return parent::save($options);
    }
}
