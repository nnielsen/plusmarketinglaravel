<?php

namespace App;
use PDO;
use Illuminate\Support\Facades\DB;
use PDOException;
use Illuminate\Support\Facades\Hash;
use Artisan;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class instanceManager
{
	private static $instance;
    protected $parametrosKeys = ['businessName','adminusr','adminpass','url'];
	public function __construct()
	{
		$this->databaseName = env('APP_NAME') . '_DB_RESOLVER';
        $this->commonDatabase = env('APP_NAME') . '_COMMON';
		$this->rootConnection = $this->_SetPDOConnection();
	}
    public function getManagerCreated()
    {
        try{
            $query = "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME =  ?";
            $results = DB::select($query, [$this->databaseName]);
            return true;
        }catch(QueryException $e)
        {
            return false;
        }
    }
    public function getInstances()
    {
        self::setDatabaseName($this->databaseName);
        $query = "SELECT id,business_name,url,admin_user,admin_pass,state FROM instances
        ";
        $results = DB::select($query);
        return $results;
    }
    public function getAllInstances() {
        $query = "SHOW DATABASES LIKE \"". env('APP_NAME') . "_%\"";
        $results = DB::select($query);
        return $results;
    }
    public function executeSQL($SQL, $database = false) {
        if($database)
        {
            self::setDatabaseName($database);
            DB::statement($SQL);
            return;
        }
        $instances = $this->getAllInstances();
        foreach ($instances as $instance) {
            $current = current( (Array)$instance );
            if(strrpos($current, 'COMMON') || strrpos($current, 'RESOLVER'))
                continue; 
            self::setDatabaseName($instance);
            DB::statement($SQL);
        }
    }
    public function changeInstanceState($instanceId,$state)
    {
        self::setDatabaseName($this->databaseName);
        DB::table('instances')
            ->where('id', $instanceId)
            ->update(['state' => $state]);

    }
    public function createInstanceManager()
    {
        $pdo = $this->rootConnection;

        try {
            $pdo->exec(sprintf(
                'CREATE SCHEMA IF NOT EXISTS %s;',
                $this->commonDatabase,
                env('DB_CHARSET'),
                env('DB_COLLATION')
            ));
            self::setDatabaseName($this->commonDatabase);
            
            Schema::create('provincia', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            });

            DB::unprepared(file_get_contents(base_path() . DIRECTORY_SEPARATOR  . 'database/insert_provincias.sql'));

            Schema::create('ciudad', function (Blueprint $table) {
                $table->increments('id');
                $table->string('nombre');
                $table->integer('cp');
                $table->integer('provincia_id')->unsigned();
                $table->foreign('provincia_id')->references('id')->on('provincia');
            });
            $sql = file_get_contents(base_path() . DIRECTORY_SEPARATOR  . 'database/insert_ciudades.sql');
                $statements = array_filter(array_map('trim', explode(';', $sql)));
                
                foreach ($statements as $stmt) {
                    DB::statement($stmt);
                }

            //Artisan::call('db:seed',['--class'=>'commonSeeder']);

            $pdo->exec(sprintf(
                'CREATE SCHEMA IF NOT EXISTS %s;',
                $this->databaseName,
                env('DB_CHARSET'),
                env('DB_COLLATION')
            ));

            $pdo->exec("USE ". $this->databaseName);

            $pdo->exec(
                "
                    CREATE TABLE instances (
                        id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
                        database_name VARCHAR(255),
                        business_name VARCHAR(255),
                        url VARCHAR(255),
                        admin_user VARCHAR(255),
                        admin_pass VARCHAR(255),
                        state TINYINT(1) DEFAULT 1,
                        created_at TIMESTAMP NULL DEFAULT NULL,
                        updated_at TIMESTAMP NULL DEFAULT NULL
                    );
                "
            );
            
            return true;
        } catch (PDOException $exception) {
            return $exception;
        }  
    }

	public static function getInstance() 
	{
        if (self::$instance == null) 
        {
            self::$instance = new self();
        }
        return self::$instance;
    }
    public static function setDatabaseName($name)
    {
    	config()->set('database.connections.' . config('database.default') .'.database', $name);
    	DB::purge('mysql');
    }
    public function setCommonDB()
    {
        self::setDatabaseName($this->commonDatabase);
    }
	private function _SetPDOConnection()
    {
        return new PDO(sprintf('mysql:host=%s;port=%d;', env('DB_HOST'), env('DB_PORT')), env('DB_USERNAME'), env('DB_PASSWORD'));
    }   
    public function createInstance($parametros)
    {
        foreach ($this->parametrosKeys as $key) 
        {
            if(empty($parametros[$key]))
            {
                throw new \Exception("Falta el parametro " . strtoupper($key), 1);
                
            }
        }

        $newDatabaseName = env('APP_NAME') . '_' . strtolower(implode("_", explode(" ",$parametros['businessName'])));

        $query = "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME =  ?";
        $results = DB::select($query, [$newDatabaseName]);
        if(!empty($results))
        {
            throw new \Exception("Instancia existente");
            
        }
        $pdo = $this->rootConnection;

        try {
            $pdo->exec("USE ". $this->databaseName);

            $pdo->exec(sprintf(
                "
                    INSERT INTO instances(business_name,url,admin_user,admin_pass,database_name,created_at,updated_at) VALUES (\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",NOW(),NOW());
                ",
                        $parametros['businessName'],$parametros['url'],$parametros['adminusr'],$parametros['adminpass'],$newDatabaseName
                )
            );
            $pdo->exec(sprintf(
                'CREATE SCHEMA IF NOT EXISTS %s;',
                $newDatabaseName,
                env('DB_CHARSET'),
                env('DB_COLLATION')
            ));
            self::setDatabaseName($newDatabaseName);
            Artisan::call('migrate');


            $this->seedData($parametros);

            self::setDatabaseName($this->databaseName);
            
            return true;
        } catch (PDOException $exception) {
            $pdo->exec("USE ". $this->databaseName);

            $pdo->exec(sprintf(
                "
                    DELETE FROM instances WHERE businessName = \"%s\";
                ",
                        $parametros['businessName']
                )
            );
            $pdo->exec(sprintf(
                "
                    DROP SCHEMA IF EXISTS %s;
                ",
                        $newDatabaseName
                )
            );
            return false;
        }
    }
    private function seedData($parametros)
    {
        DB::table('file_types')->insert(
            array(
                'extension' => 'jpg'
            )
        );
        DB::table('puntos_peso')->insert(
            array(
                'coeficiente' => 1,
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d')
            )
        );
        DB::table('instance')->insert(
            array(
                'name' => $parametros['businessName'],
                'businessName'=> $parametros['businessName'],
                'url'=> $parametros['url'], 
                'adminPass'=> $parametros['adminpass'],
                'adminUser'=> $parametros['adminusr'],
            )
        );
        DB::table('configuracion_instancia')->insert(
            array(
                'instanceId' => 1,
                'mailPort'=> env('MAIL_PORT'),
                'mailHost'=> env('MAIL_HOST'),
                'mailUsername'=> env('MAIL_USERNAME'),
                'mailPassword'=> env('MAIL_PASSWORD'),
                'mailEncryption'=> env('MAIL_ENCRYPTION'),
                'mailFromName'=> env('MAIL_FROM_NAME'),
                'mailFromMail'=> env('MAIL_FROM_ADDRESS')
            )
        );
        DB::table('users')->insert(
            [
                'alias' => $parametros['adminusr'],
                'nombre' => 'Administrador',
                'apellido'=> '',
                'mail' => '',
                'inputMethodId' => 0,
                'tipoId' => 1,
                'password' => Hash::make($parametros['adminpass']) 
            ]
        );
    }
}
