<?php

namespace App;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;
use App\instanceManager;

class Helpers
{
	public static function formatDate($date)
    {
        $date = date_create($date);
        $date = date_format($date,'Y-m-d');        
        return $date;
    }
    public static function resolveItemName($item)
    {
    	return (__('constants.'.$item['name'])=='constants.'.$item['name'])?$item['name']:
	    		__('constants.'.$item['name']);
    }
    public static function getUbicacionString($idUbicacion)
    {
    	$instanceManager = instanceManager::getInstance();
        $result = DB::select('
            SELECT CONCAT(ciudad.`nombre`,", ",provincia.`nombre`) as ubicacion
                FROM '.$instanceManager->commonDatabase.'.ciudad AS ciudad
                JOIN '.$instanceManager->commonDatabase.'.provincia AS provincia
                ON provincia.id = ciudad.`provincia_id`
                WHERE ciudad.id='. $idUbicacion);
        if(isset($result[0]))
        {
            return $result[0]->ubicacion;
        }
        return null;
    }
    public static function createSelect($array,$model,$selectedCondition=null,$onChange=false)
	{
		$onChange = $onChange?' ng-change="'.$onChange.'" ': '';
		$return = '<select class="form-control" ng-model="'.$model.'"'.$onChange.'>';
	    
	    foreach ($array as $item)
	    {
	    	
	     	$return .= '<option value="'
	                 	.$item['value']
	                 	.'" ng-selected="'.$selectedCondition.'=='.$item['value'].'">'
	                 	. static::resolveItemName($item)
	                 	.'</option>';
	    }
		
		return $return .'</select> ';    
	}
	public static function createSelectFromArray($array,$model,$selectedCondition=null,$value,$name,$onChange=false)
	{
		$onChange = $onChange?' ng-change="'.$onChange.'" ': '';
		$return = '<select class="form-control" ng-model="'.$model.'"'.$onChange.'>';
	    
	    foreach ($array as $item)
	    {
	    	
	     	$return .= '<option value="'
	                 	.$item[$value]
	                 	.'" ng-selected="'.$selectedCondition.'=='.$item[$value].'">'
	                 	. $item[$name]
	                 	.'</option>';
	    }
		
		return $return .'</select> ';    
	}
	public static function createSelectArray($array,$nameKey=null,$valueKey='id')
	{
		$return = [];
		foreach ($array as $item) {
    		$return[] = [
						'name'=>$item->$nameKey,
						'value'=>$item->$valueKey
						];
    	}
    	return $return;
	}
	public static function instanceRoute($append)
	{
		return \App::make('url')->to(request()->route()->parameter('instanceUrl') .'/'. $append);
	}
	public static function export($export,$format,$prefix)
	{
		$nameFile = $prefix . date('d_m_Y_H_i_s');
		switch ($format) 
        {
            case 'csv':
                return Excel::download($export, $nameFile.'.csv');
            case 'html':
                return Excel::download($export, $nameFile.'.html');
            case 'xlsx':
                return Excel::download($export, $nameFile.'.xlsx');
            case 'xls':
                return Excel::download($export, $nameFile.'.xls');
            case 'json':
            default:
                return response()->json($export->collection());
                break;
        }

	}
}