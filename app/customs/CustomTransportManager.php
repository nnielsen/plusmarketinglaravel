<?php
namespace App\Customs;

use Illuminate\Mail\TransportManager;
use App\configuracionInstancia;

class CustomTransportManager extends TransportManager {

    /**
     * Create a new manager instance.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return void
     */
    public function __construct($app)
    {
        $this->app = $app;
        $settings = configuracionInstancia::getInstancia();
        $config = [
            'driver'        => 'smtp',
            'host'          => $settings->mailHost,
            'port'          => $settings->mailPort,
            'from'          => [
                'address'   => $settings->mailFromMail,
                'name'      => $settings->mailFromName
            ],
            'encryption'    => $settings->mailEncryption,
            'username'      => $settings->mailUsername,
            'password'      => $settings->mailPassword
        ];
        if($settings->mailSMTPPeerVerif == 0) {
            $config['stream'] = [
                'ssl' => [
                   'allow_self_signed' => true,
                   'verify_peer' => false,
                   'verify_peer_name' => false,
                ]
            ];
        }
        $this->app['config']['mail'] = $config;
    }
  
}