<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class metodoIngreso extends Model
{
	use SoftDeletes;
    protected $table = 'metodo_ingreso';
    protected $fillable = ['inicio','cierre','nombre'];
}
