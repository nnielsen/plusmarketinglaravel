<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware'=>'databaseSelector','prefix'=>"{instanceUrl}/api"], function()
{
	Route::group(['middleware' => 'logging'], function () {

		/*
			AUTH
		*/
			Route::post('/log', 'AuthController@login')->name('login');
			Route::get('user/verify/{verification_code}', 'AuthController@verifyUser');
			Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.request');
			Route::post('password/reset', 'Auth\ResetPasswordController@postReset')->name('password.reset');
			Route::post('/logout','AuthController@logout')->name('logout');


	Route::group(['middleware' => ['jwt.auth','user.instance']], function () {

		/*
			CLIENTE
		*/
		Route::get('/clientes', 'clientesController@getAll')->name('clientesGetAll');
		Route::post('/cliente', 'clientesController@create')->name('clienteCreate');
		Route::get('/cliente', 'clientesController@get')->name('clienteGet');
		Route::delete('/cliente/{id}', 'clientesController@delete')->name('clienteDelete');
		Route::get('/cliente/{id}', 'clientesController@get')->name('cllienteGet');
		Route::put('/cliente/{id}', 'clientesController@put')->name('cllientePut');
		Route::get('/clientes/export', 'clientesController@export')->name('clientesExport');
		Route::get('/transactions','clientesController@transactions')->name('clienteTransactions');

		/*
			ARTICULO
		*/
		Route::get('/articulos', 'articulosController@getAll')->name('articulosGetAll');
		Route::get('/articulos', 'articulosController@getAll')->name('articulosGetAll');
		Route::get('/articulo/{id}', 'articulosController@get')->name('articuloGet');
		Route::put('/articulo/{id}', 'articulosController@put')->name('articuloPut');
		Route::delete('/articulo/{id}', 'articulosController@delete')->name('articuloDelete');
		Route::post('/articulo', 'articulosController@create')->name('articuloCreate');
		Route::post('/articulo/imagen/{id}','articulosController@imgUpload')->name('articuloImg');
		Route::get('/articulo/categorias/all','articulosController@getCategorias')->name('articuloCategoriasGet');
		Route::post('/articulo/categoria/new','articulosController@createCategoria')->name('articuloCategoriasPost');
		Route::get('/articulos/export', 'articulosController@export')->name('articulosExport');
		Route::get('articulo/categoria/subtree','articulosController@getSubtree')->name('articuloCategoriaSubtree');
		Route::delete('articulo/categoria/delete','articulosController@deleteCategoria')->name('articuloCategoriaDelete');

		//METODOS INGRESO

		Route::get('/mis', 'metodosIngresoController@getAll')->name('metodosIngresoGetAll');
		Route::get('/mis/{id}', 'metodosIngresoController@get')->name('metodoIngresoGet');

		//USUARIO
		Route::get('/usuario/{id}', 'usuariosController@get')->name('usuarioGet');
		Route::get('/usuario/mi', 'metodosIngresoController@getByAuth')->name('metodoIngresoGet');
		Route::post('/usuario/mi', 'metodosIngresoController@setByAuth')->name('metodoIngresoSet');


		//COMPRA
		Route::post('/compra','compraController@create')->name('compraCreate');
		Route::get('/compras/descripciones', 'compraController@getDescripcionSugerencia')->name('compraGetDescripcionSugerencia');
		Route::get('/compra/items','compraController@getItems')->name('getCompraItems');



		//CANJE
		Route::post('/canje','canjeController@create')->name('canjeCreate');
		Route::get('/canje/items','canjeController@getItems')->name('getCanjeItems');

		//CONFIG
		Route::get('/config','configuracionController@get')->name('configGet');

		//USER
		Route::post('/termsconditions', 'usuariosController@termsconditions')->name('acceptTermsAndConditions');

		//RANKING
		Route::get('/ranking/clientes', 'clientesController@ranking')->name('rankingClientesGet');
		Route::get('/ranking/clientes/export', 'clientesController@exportRanking')->name('rankingClientesExport');

		//REPORTE CANJES
		Route::get('reporte/canjes','clientesController@reporteCanjes')->name('reporteCanjes');

		//DASH
		Route::get('dash','dashController@dashStats')->name('dashStats');

		//FormFILTERS
		Route::get('formFilter/location','formFilter@searchLocation')->name('searchLocation');
		Route::get('formFilter/cliente','formFilter@searchCliente')->name('searchCliente');	

			//ADMIN
		Route::group(['middleware' => 'user.admin'], function () {

			//SUCURSALES
			Route::get('/sucursales','sucursalController@getAll')->name('sucursalGetAll');
			Route::post('/sucursal','sucursalController@create')->name('sucursalPost');
			Route::delete('/sucursal','sucursalController@delete')->name('sucursalDelete');
			Route::put('/sucursal','sucursalController@put')->name('sucursalPut');
			
			//COMPRA
			Route::delete('/compra','compraController@delete')->name('compraDelete');
			Route::delete('/compra/item','compraController@deleteItem')->name('compraItemDelete');
			Route::put('/compra/item','compraController@editItem')->name('compraItemEdit');
			Route::put('/compra/header','compraController@getHeader')->name('compraHeaderGet');

			//CANJE
			Route::delete('/canje','canjeController@delete')->name('canjeDelete');
			Route::delete('/canje/item','canjeController@deleteItem')->name('canjeItemDelete');


			//CONFIG
			Route::get('/config','configuracionController@get')->name('configGet');
			Route::put('/config','configuracionController@put')->name('configPut');
			Route::post('imgs/logo','configuracionController@setLogo')->name('logoSet');

			//USUARIOS
			Route::get('/usuarios', 'usuariosController@getAll')->name('usuariosGetAll');
			Route::put('/usuario/{id}', 'usuariosController@put')->name('usuarioPut');
			
			Route::post('/usuario', 'usuariosController@create')->name('usuarioCreate');
			Route::delete('/usuario/{id}', 'usuariosController@delete')->name('usuarioDelete');
			Route::get('/usuarios/export', 'usuariosController@export')->name('usuariosExport');

			//METODOS INGRESO
			Route::put('/mis/{id}', 'metodosIngresoController@put')->name('metodoIngresoPut');
			Route::post('/mis', 'metodosIngresoController@create')->name('metodoIngresoCreate');
			Route::delete('/mis/{id}', 'metodosIngresoController@delete')->name('metodoIngresoDelete');
			});
		});
	});
});
	