<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::view('/admin/login','instanceManagerLogin')->name('instanceManagerLoginForm');
Route::post('/admin/login','instanceAdminController@login')->name('instanceManagerLoginPost');
Route::group(['middleware'=>'instanceAdminSession'],function(){
    Route::get('/admin', 'instanceAdminController@index')->name('instanceManagerIndex');
    Route::get('/admin/logout','instanceAdminController@logout')->name('instanceManagerLogout');
    Route::view('/admin/instance/create','instanceManagerCreateForm')->name('instanceManagerInstanceCreateForm');

    Route::post('/admin/instance/create','instanceAdminController@instanceCreatePost')->name('instanceManagerInstanceCreatePost');
    Route::get('/admin/instance/{instanceId}','instanceAdminController@instanceAction')->name('instanceManagerInstanceAction');
});


Route::group(['middleware'=>'databaseSelector','prefix'=>"{instanceUrl?}"], function()
{
    Route::get('/', function (Request $request) {
        return redirect(Request()->route()->parameter('instanceUrl') . '/login');
    });
    Route::get('/login', function () {
        return view('login');
    });
    Route::get('/passwordRecovery', function () {
        return view('passwordRecovery');
    });
    Route::post('/passwordRecovery/send','passwordRecoveryController@sendMail');


    Route::get('/main',function()
    {
      return view('mainLayout');
    });
    Route::get('imgs/logo','configuracionController@getLogo')->name('logoGet');
    Route::get('imgs/logo/{mailId}','configuracionController@getLogo')->name('logoGet');
    Route::get('imgs/articulos/{filename}', function ($base,$filename)
    {
        $path = storage_path() . '/app/imgs/articulos/' . $filename;
        if(!File::exists($path)) abort(404);

        $file = File::get($path);
        $type = File::mimeType($path);

        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);

        return $response;
    });
    Route::get('imgs/articulos/thumb/{filename?}', function ($base,$filename)
    {
        $base_path = storage_path() . DIRECTORY_SEPARATOR .'app'. DIRECTORY_SEPARATOR .'imgs' . DIRECTORY_SEPARATOR.'articulos' . DIRECTORY_SEPARATOR;
        $path =  $base_path .'thumb' . DIRECTORY_SEPARATOR . $filename;

        if(!File::exists($path)) 
        {
            $originalPath = $base_path . $filename;
            
            if(!File::exists($originalPath))
                abort(404);
            else
            {
                $image = Image::make($originalPath)->fit(600, 480, function ($constraint) {
                    $constraint->upsize();
                });
                $image->save($path);
            }
        }

        $file = File::get($path);
        $type = File::mimeType($path);

        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);

        return $response;
    })->where(['filename' => '.*']);
 

    Route::get('img/{filename?}', function ($base,$filename)
    {
        if(!File::exists($path = public_path() . DIRECTORY_SEPARATOR ."img".DIRECTORY_SEPARATOR. $filename))
            {

                abort(404);
            }         

        $file = File::get($path);
        $type = File::mimeType($path);

        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);

        return $response;
    })->where(['filename' => '.*']);

    Route::get('imgs/{filename?}', function ($base,$filename)
    {
        $path = storage_path() . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . $filename;
        if(!File::exists($path)) {

          abort(404);
        }

        $file = File::get($path);
        $type = File::mimeType($path);

        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);

        return $response;
    })->where(['filename' => '.*']);

    

    Route::get('catalogo','catalogoController@index')->name('catalogo');
    Route::get('catalogo/item/{id?}','catalogoController@item')->name('catalogoItem');


    Route::prefix('view')->group(function () 
    {
      $genders = Config::get('constants.genders');
      $civilStates = Config::get('constants.civilStates');

        //CLIENTE
        Route::view('cliente/alta', 'clienteAlta', ['genders' => $genders,'civilStates'=>$civilStates]);
        Route::view('cliente/edit', 'clienteEdit', ['genders' => $genders,'civilStates'=>$civilStates]);
        Route::view('clientes', 'clientesTable');
        Route::view('cliente/transacciones', 'transaccionesEdit');

        //ARTICULO
        Route::view('articulo/alta', 'articuloAlta');
        Route::view('articulo/edit', 'articuloEdit');
        Route::view('articulos', 'articulosTable');

        //METODOS INGRESO
        Route::view('mis', 'metodosIngreso');
        Route::view('mi/edit', 'metodoIngresoEdit');
        Route::view('mi/alta', 'metodoIngresoAlta');

        //SUCURSALES
        Route::view('sucursales','sucursalesTable');
        Route::view('sucursal/alta','sucursalAlta');


        //dash
        Route::view('dash','dash');

        //config
        Route::view('config', 'config');    

        //RANKING
        Route::view('ranking','ranking');

        //Reporte Canjes
        Route::view('reporte','reporteCanjes');

        //OPERACION
        Route::view('operacion', 'operacion');   
        Route::group(['middleware' => 'jwt.auth'], function () {
             Route::get('compra/comprobante/{id}', 'compraController@comprobante');  
             Route::get('canje/comprobante/{id}', 'canjeController@comprobante');   
        }); 

        //CATEGORIAS
        Route::view('categoria-nodo','categoria-nodo');

        //USUARIO
        Route::get('usuario/alta', 'usuariosController@getCreateForm');
        Route::get('usuario/edit', 'usuariosController@getEditForm');
        Route::view('usuarios', 'usuariosTable');
        Route::view('cambiarPassword','cambioPassword');

        //ContactoSoporte
        Route::view('contactoSoporte','contactoSoporte');
      });  
});
