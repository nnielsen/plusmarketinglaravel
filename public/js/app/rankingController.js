 mainApp.controller("ranking",function($scope,$http){
        $scope.todo = [];
        $scope.getAll = (params=$scope.params)=>
        {
            $http({
                    url: getUrl('rankingClientes'), 
                    method: "GET",
                    params
                 }).then(function(response){
                $scope.lastPage = response.data.last_page;
                response.data.data.forEach((item)=>$scope.todo.push(item));
            }); 
        }
        $scope.export = (format)=>
        {
            let params= {format};
            $http(
                {
                    url: getUrl('rankingExport'), 
                    method: "GET",
                    params
                 }).then(function(response)
                {
                    downloadFromHttpResponse(response);
                });
        }
        $scope.initParams = ()=>
        {
            $scope.params = {
                page:1
            };    
        }
        $scope.goToClient = (cliente) =>
        {
            location.assign("#!/cliente/edit/"+cliente.id);
        }
        $scope.showMore = ()=>
        {
            $scope.params.page++;
            $scope.getAll($scope.params);
        }
        $scope.initParams();
        $scope.getAll();
    });

   