mainApp.service('swipeService',swipeService);
swipeService.$inject =
[
	'$rootScope','session'
];
function swipeService($rootScope,session)
{
	this.listener = false;

	this.inputMethod = {};

	this.readerSuccess = (data)=>
	{
		$rootScope.$broadcast('card-read-success',data);
		console.info('Lectura de tarjeta --> ' + data);
	}
	this.getInputMethodId = ()=>
	{
		return this.inputMethod.id;
	}
	this.parser = function (rawData) 
	{
	    let inicio = localStorage.getItem('inputMethodInicio'),
	    cierre = localStorage.getItem('inputMethodCierre'),
	    pattern = new RegExp("^"+inicio+".*"+cierre),
	    match = pattern.exec(rawData);
	    
	    if (!match) return null;
	    let input = match.input,
	    patterndos = new RegExp(inicio+ "([0-9]*)" +cierre),
	    a = input.match(patterndos);

	    return a[1];
	}
	this.setInputMethod = function(inputMethod)
	{
		session.set('inputMethodId',inputMethod.id);
		session.set('inputMethodInicio',inputMethod.inicio);
		session.set('inputMethodCierre',inputMethod.cierre);
		this.inputMethod = inputMethod;
	}
	this.enable = ()=>
	{
		if(!this.listener)
		{

			this.listener = 
			$.cardswipe({
		            firstLineOnly: true,
		            success: this.readerSuccess,
		            parsers: [this.parser],
		            debug: false
		        });
		}else {
			this.listener.enable();
		}
		this.enabled = true;
	}
	this.isEnabled = ()=>{
		return this.enabled;
	}
	this.disable = ()=>
	{
		this.listener && this.listener.disable();
		this.enabled = false;
	}
}
