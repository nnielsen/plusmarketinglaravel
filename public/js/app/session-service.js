mainApp.service('session',session);

function session()
{
	var session = {};
	this._profile = [];
	session.setToken = function(token) 
	{
		localStorage.setItem("Authorization",token);
		this.token = token;
	}
	session.getToken = function() 
	{
		if(this.token == null | this.token == undefined)
		{
			this.token = localStorage.getItem("Authorization");
		}
		return this.token;
	}
	session.set = function(key,value)
	{
		return localStorage.setItem(key,value);
	}
	session.resolveLogin = function(authData)
	{
		this.set('authData',JSON.stringify(authData));
		switch (this.getUserType()) 
		{
			case 'intern':
			case 'internAdmin':
				if(authData.requiredNewPass == 1)
				{
					$goTo = 'intern/profile';
				}
				else
				{
					$goTo = 'intern/desktop';	
				}
				this.set('userName',authData.firstName + ' ' + authData.lastName);
				break;
			case 'technicalManager':
				$goTo = 'tm/desktop';
				this.set('userName',authData.firstName + ' ' + authData.lastName);
				break;
			case 'company':
				$goTo = 'company/desktop';
				break;
			default:
				break;
		}
		if(typeof $goTo != 'undefined')
		{
			return location.assign('#!/' + $goTo);
		}
	}
	session.getUserType = function()
	{
		var profilesArray = this.getProfile();
		if(typeof profilesArray == 'undefined')
          return undefined;

		if(profilesArray.includes(3))
        {
          $userType = 'internAdmin';
        }
        else if(profilesArray.includes(6) || 
        	profilesArray.includes(9) || 
        	profilesArray.includes(8))
        {
          $userType = 'technicalManager';
        }
        else if(profilesArray.includes(1))
        {
          $userType = 'company';
        }
        else
        {
          $userType = 'intern'
        }
        return $userType;
	}
	session.get = function(key)
	{
		if(localStorage.getItem(key) == null)
		{
			return false;
		}
		return localStorage.getItem(key);
	}
	session.setProfile = function(profile)
	{
		if(Array.isArray(profile))
		{
			this._profile=profile;
			localStorage.setItem('listProfile',this._profile.join(";"));
		}
	}
	session.getProfile = function()
	{
		if(typeof this._profile == 'undefined' && localStorage.getItem('listProfile'))
		{
			this._profile = localStorage.getItem('listProfile')
			.split(";").map(function(i){return parseInt(i);});
		}
		return this._profile;
	}
	session.getAuthData = function()
	{
		if(session.get('authData'))
		{
			return JSON.parse(session.get('authData'));
		}
	}
	session.getIsAgent = function()
	{
		return session.getAuthData.isAgent?true:false;
	}
	session.delete = function(key)
	{
		return localStorage.removeItem(key);
	}
	session.clear = function()
	{
		this._profile = undefined;
		this.token = undefined;
		localStorage.clear();
	}
	session.logOut = function()
	{
		this.clear();
	}
	session.isAuthenticated = function()
	{
		return localStorage.getItem("Authorization") !== null;
	}
	return session;
};