  mainApp.controller("sucursalTableController",function($scope,$http,$routeParams, Clientes,$rootScope,$ngConfirm){
        
        $scope.initParams = ()=>
        {
            return $scope.params = {
                page:1
            }
        }
        $scope.search = (queryString)=>
        {
            $scope.searchString = queryString;
            $scope.params = Object.assign({queryString},$scope.initParams());
            $scope.getAll($scope.params);
        }
        $scope.getAll = (params=$scope.params,append=false)=>
        {
            typeof $scope.todo == 'undefined' && ($scope.todo=[]);
               return $http({
                    url:getUrl('sucursalesGetAll'),
                    params,
                    method:"GET"
                }).then(function(response)
                {
                    $scope.lastPage = response.data.last_page;
                    if(append)
                    {
                        response.data.data.forEach((item)=>{
                        $scope.todo.push(item);
                    });
                    }else {
                        $scope.todo = response.data.data;
                    }
                    
                });
        }
        $scope.edit = (sucursal)=>
        {
            sucursal.edit = true;
        }
        $scope.editing = (sucursal)=>
        {
            return sucursal.edit === true;
        }
        $scope.save = (sucursal,index)=>
        {
            $http({
                url:getUrl('sucursal'),
                method:"PUT",
                params:sucursal
            })
            .then((response)=>
            {
                sucursal.edit = false;
                $scope.todo[index] = response.data.update;
                toastr.success('Sucursal modificada con éxito');
            })
        }
        $scope.delete = (sucursal,index)=>
        {
            $http({
                url:getUrl('sucursal'),
                method:"DELETE",
                params:{
                    id:sucursal.id
                }
            })
            .then((response)=>
            {
                toastr.success('Sucursal eliminada con éxito');
                $scope.todo.splice(index,1)
            })
        }
        $scope.suggestLocation = (inpString)=>
        {
            return $http({
                url:getUrl('suggestLocation'),
                params:{
                    queryString:inpString
                },
                method:"GET"
            })
            .then((response)=>
            {
                $scope.locationSuggestions = response.data;
            });
        }
        $scope.initParams();
        $scope.getAll();
        $scope.clearSearch = ()=>
        {
            $scope.inpSearch = $scope.searchString = '';
            delete $scope.params.queryString;
            $scope.getAll();
        }

    })
  .controller("sucursalAltaController",function($scope,$http,$routeParams, Clientes,$rootScope,$ngConfirm){
        
       $scope.sucursal = {}, $scope.saving = false;
       $scope.suggestLocation = (inpString)=>
        {
            return $http({
                url:getUrl('suggestLocation'),
                params:{
                    queryString:inpString
                },
                method:"GET"
            })
            .then((response)=>
            {
                $scope.locationSuggestions = response.data;
            });
        }
        $scope.alta = ()=>
        {
            $scope.saving = true;
            $http.post(getUrl('sucursal'),$scope.sucursal)
            .then((response)=>
            {
                $scope.saving = false;
                response = response.data;
                if(response.alta)
                {
                    toastr.success('Sucursal creada con éxito');
                    location.assign('#!/sucursales');
                }
            },function(){
                $scope.saving = false;
            });
        }
    });