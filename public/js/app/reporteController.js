 mainApp.controller("reporte",function($scope,$http, Usuarios){
        
        $scope.inpSearch = {};
        $scope.getAll = (params = $scope.params)=>
        {
            $http({
                url:getUrl('reporteCanjes'),
                method: "GET",
                params
            })
            .then(function(response){
                $scope.todo = response.data.data;
            });
        }

        $scope.actualizar = function(item) {

            let fromDate = $scope.inpSearch.fromDate, toDate=$scope.inpSearch.toDate;
            $scope.getAll(Object.assign(($scope.params={page:1}),{fromDate,toDate}));
        };
         $scope.export = (format)=>
        {
            let params= {format};
            $http(
                {
                    url: getUrl('reporteExport'), 
                    method: "GET",
                    params
                 }).then(function(response)
                {
                    downloadFromHttpResponse(response);
                });
        }
        $scope.initParams = ()=>
        {
            $scope.params = {
                page:1
            };    
        }

        $scope.showMore = ()=>
        {
            $scope.params.page++;
            $scope.getAll($scope.params);
        }
        $scope.initParams();
        $scope.getAll();
    });