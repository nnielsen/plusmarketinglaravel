var mainApp = angular.module('mainApp',
  [
    'ngRoute',
    'ngResource',
    'mk.editablespan',
    'cp.ngConfirm',
    'angular-chartist'
  ]);
    mainApp.factory('Clientes', ['$resource', function($resource) {
        return $resource(getUrl('clienteId'), {id:'@id'}, {
            'update': { method: 'PUT' }
        });
    }]);
    mainApp.factory('Compras', ['$resource', function($resource) {
        return $resource(getUrl('compraEditId'), {id:'@id'}, {
            'update': { method: 'PUT' }
        });
    }]);
    mainApp.factory('Usuarios', ['$resource', function($resource) {
        return $resource(getUrl('usuarioId'),{id:'@id'}, {
            'update': { method: 'PUT' }
        });
    }]);
    mainApp.factory('Articulos', ['$resource', function($resource) {
        return $resource(getUrl('articuloId'), {id:'@id'},
        {
            'update': { method:'PUT'}
        });
    }]);
    mainApp.factory('MetodosIngreso', ['$resource', function($resource) {
        return $resource(getUrl('mi') + '/:id', {id:'@id'},
        {
            'update': { method:'PUT'}
        });
    }]);
    mainApp.factory('httpRequestInterceptor', function ($q,session) {
      return {
        request: function (config) {
            config.headers['Authorization'] = 'Bearer ' +  localStorage.getItem('token');
            return config;
            },
        response: function (response) {
                if(response.msj)
                {
                    toastr.error(response.msj);
                }
                return response;
            },
        'responseError': function(rejection) {
            var data = rejection.data;

            if(data.msj)
            {
                toastr.error(data.msj);
            }else if (data.errors)
            {
                toastr.error(Object.values(data.errors).map((arr)=>arr[0]).join("<br>"));
            }
            else if (data.error)
            {
                toastr.error(data.error);
            }
            if(rejection.status==401)
            {
                errorMsg('Sesion >> Redireccionando a pagina de login','Error de autenticación');
                session.clear();
                window.setTimeout( function(){
                    window.location = "login?reject=1";
                }, 4000 );
            }
            if(rejection.status == 423)
            {
                errorMsg('Instancia en pausa','Manager de instancias');
                session.clear();
                window.setTimeout( function(){
                    window.location = "login?reject=2";
                }, 4000 );
            }
            return $q.reject(rejection);
        }
    };
    });
    mainApp.config(function ($httpProvider) {
      $httpProvider.interceptors.push('httpRequestInterceptor');
    });
    mainApp.filter('sinDatos', function() {
    return function(input, defaultValue) {
        if (angular.isUndefined(input) || input === null || input === '') {
            return defaultValue;
        }
        return input;
        }
    }).filter("asDate", function () 
    {
        return function (input) 
        {
            if(typeof input != 'undefined')
            {
                return new Date(input.replace(/\s/, 'T'));
            }
            return "";
        }
    })
    .filter("asDate2", function () 
    {
        return function (input) 
        {
            if(typeof input != 'undefined')
            {
                return new Date(input);
            }
            return "";
        }
    });
    mainApp.directive('ngEnter', function() {
        return function(scope, element, attrs) {
            element.bind("keydown keypress", function(event) {
                if(event.which === 13) {
                        scope.$apply(function(){
                                scope.$eval(attrs.ngEnter);
                        });
                        
                        event.preventDefault();
                }
            });
        };
}).config(['$provide', function($provide) {
        $provide.decorator('$locale', ['$delegate', function($delegate) {
            $delegate.NUMBER_FORMATS.DECIMAL_SEP = ',';
            $delegate.NUMBER_FORMATS.GROUP_SEP = '.';
            return $delegate;
        }]);
    }]);
angular.module("contextMenu", []);