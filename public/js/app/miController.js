 mainApp.controller("mi",function($scope,$http, Usuarios,MetodosIngreso){
        $scope.getAll = ()=>{
                $http.get(getUrl("mis")).then(function(response){
                response = response.data;
                $scope.todo = response.todo;
            });
        }
        $scope.getAll();
        $scope.delete = (metodo)=>{
            MetodosIngreso.remove({id:metodo.id}).$promise.then(function(response){
                if(response.delete) {
                    toastr.info("Eliminado");
                    $scope.getAll();
                }
            },function(error)
            {
                toastr.error('Accion no permitida');
            });
        }
    });
    mainApp.controller("miAlta",function($scope,$http,$routeParams, MetodosIngreso){
       
        $scope.alta = function(item) {
             MetodosIngreso.save({},$scope.mi).$promise.then(function(response){
                if(response.alta) {
                    window.location.href = "#!/mi";
                }
                if(response.msj){
                    toastr.error(response.msj);
                }
            });
        };
    });
    mainApp.controller("miEdit",function($scope,$http,$routeParams, MetodosIngreso){
        $scope.mi = MetodosIngreso.get({id:$routeParams.id});

        $scope.onReady = function(item) {
            $scope.cambio = true;
        };
        $scope.delete = (metodo=$scope.mi)=>{
            MetodosIngreso.remove({id:metodo.id}).$promise.then(function(response){
                if(response.delete) {
                    toastr.info("Eliminado");
                    location.assign('#!/mi')
                }
            },function(error)
            {
                toastr.error('Accion no permitida');
            });
        }
        $scope.guardar = function(item) {
            $.blockUI({ message: '<img src="img/login/loading.gif">' });
            MetodosIngreso.update({id:$routeParams.id}, $scope.mi).$promise.then(function(response){
                    $scope.cambio = false;
                    $.unblockUI();
                    toastr.success('Cambios guardados');
            });
        };
    });
