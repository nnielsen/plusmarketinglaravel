mainApp.directive('locationSuggestion',function () 
  {
  return {
    link: function (scope, element, attr) 
    {
      scope.inpString = '';
      scope.modelChange = scope.modelChange();
      scope._modelChange = (inpString)=>
      {
        scope.searching = inpString;
        inpString && inpString!= '' && scope.modelChange(inpString)
        .then(()=>{
          if(scope.suggestions)
          {
            if((scope.choosen = scope.suggestions.find((e)=> 
              e.searchString == scope.inpString)) !='undefined'
              )
            {
              scope.locationModel = scope.choosen.id;
            }
          }
        });
      }
    },
     restrict: 'EA',
      transclude:true,
      template:   `<div class="form-group">
                      <label>Ubicacion</label>
                      <input list="locationSuggestions" ng-model="inpString" 
                      type="text" class="form-control" ng-change="_modelChange(inpString)"  
                      ng-model-options="{ debounce: 300 }">
                      <datalist id="locationSuggestions">
                          <option ng-repeat="location in suggestions" 
                          value="{{location.searchString}}">
                      </datalist>
                  </div>
                  `,
      scope: {
          locationModel: '=',
          suggestions:'=',
          modelChange:'&'
      }
  };
});