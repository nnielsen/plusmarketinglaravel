    function status()
    {
        if(window.localStorage.getItem("token") == null)
        {
            //location.href = "login.html";
        }    
    }
    status();
     function cambiarLogo()
    {
        d = new Date();
        $("#appLogo").fadeOut(1000,function()
        {
            $("#appLogo").attr("src", "downImg/logo?"+d.getTime());
            $("#appLogo").fadeIn(1000);
        });
    }
    function logout()
    {
        localStorage.clear();
        window.location.href = './';
    }
    Date.prototype.yyyymmdd = function() {
      var mm = this.getMonth() + 1; // getMonth() is zero-based
      var dd = this.getDate();

      return [this.getFullYear(),
              (mm>9 ? '' : '0') + mm,
              (dd>9 ? '' : '0') + dd
             ].join('-');
    };
    function downloadFromHttpResponse(response)
    {
      if(!response.headers('Content-Disposition'))
      {
        var str = JSON.stringify(response.data);
        var data = encode( str );
        var blob = new Blob( [ data ], {
          type: 'application/octet-stream'
        });
      }else
      {
        var blob = new Blob([response.data], {type: response.headers('Content-Type')});  
      }
      
      var downloadUrl = URL.createObjectURL(blob);
      var a = document.createElement("a");
      a.href = downloadUrl;
      a.download = response.headers('Content-Disposition')?
      response.headers('Content-Disposition').substr(21) : "export.json";
      document.body.appendChild(a);
      a.click();
    }
    function changeImg(input,destiny) {
        var reader = new FileReader();

        reader.onload = function(e) {
          destiny.attr('src', e.target.result);
        }
        reader.readAsDataURL(input);
    }
    var encode = function( s ) {
    var out = [];
    for ( var i = 0; i < s.length; i++ ) {
      out[i] = s.charCodeAt(i);
    }
    return new Uint8Array( out );
  }
  function errorMsg(msg,title)
  {
    toastr.error(msg,title,
                {
                    "preventDuplicates":true,
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": true,
                    "positionClass": "toast-bottom-full-width",
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "300",
                    "timeOut": "3000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                });
  }
