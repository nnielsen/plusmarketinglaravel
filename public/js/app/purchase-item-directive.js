mainApp.directive('purchaseItem',
 function () 
 {
    return {
        restrict: 'EA',
        transclude:true,
        template:   `<li class="list-group-item fist-item">
                        <span class="float-right">
                            09:00 pm
                        </span>
                        <span class="label label-success">1</span> Please contact me
                    </li>
                    `,
        scope: {
            purchase: '='
        }
    };
});