    function status()
    {
        if(window.localStorage.getItem("token") == null)
        {
            //location.href = "login.html";
        }    
    }
    status();
     function cambiarLogo()
    {
        d = new Date();
        $("#appLogo").fadeOut(1000,function()
        {
            $("#appLogo").attr("src", "downImg/logo?"+d.getTime());
            $("#appLogo").fadeIn(1000);
        });
    }
    function logout()
    {
        localStorage.clear();
        window.location.href = './';
    }
    Date.prototype.yyyymmdd = function() {
      var mm = this.getMonth() + 1; // getMonth() is zero-based
      var dd = this.getDate();

      return [this.getFullYear(),
              (mm>9 ? '' : '0') + mm,
              (dd>9 ? '' : '0') + dd
             ].join('-');
    };
    function downloadFromHttpResponse(response)
    {
      if(!response.headers('Content-Disposition'))
      {
        var str = JSON.stringify(response.data);
        var data = encode( str );
        var blob = new Blob( [ data ], {
          type: 'application/octet-stream'
        });
      }else
      {
        var blob = new Blob([response.data], {type: response.headers('Content-Type')});  
      }
      
      var downloadUrl = URL.createObjectURL(blob);
      var a = document.createElement("a");
      a.href = downloadUrl;
      a.download = response.headers('Content-Disposition')?
      response.headers('Content-Disposition').substr(21) : "export.json";
      document.body.appendChild(a);
      a.click();
    }
    function changeImg(input,destiny) {
        var reader = new FileReader();

        reader.onload = function(e) {
          destiny.attr('src', e.target.result);
        }
        reader.readAsDataURL(input);
    }
    var encode = function( s ) {
    var out = [];
    for ( var i = 0; i < s.length; i++ ) {
      out[i] = s.charCodeAt(i);
    }
    return new Uint8Array( out );
  }
  function errorMsg(msg,title)
  {
    toastr.error(msg,title,
                {
                    "preventDuplicates":true,
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": true,
                    "positionClass": "toast-bottom-full-width",
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "300",
                    "timeOut": "3000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                });
  }

 var _resource = 
 {
    clientesAll:'clientes',
    clientesExport:'clientes/export',
    clienteTransacciones:'transactions',
    articulosAll:'articulos',
    articulosExport:'articulos/export',
    mis:'mis',
    mi:'mis',
    mailConfig:'mail/config',
    miByAuth:'usuario/mi',
    config:'config',
    clienteId:'cliente/:id',
    clienteSearch:'cliente',
    usuarioId:'usuario/:id',
    usuariosExport:'usuarios/export',
    articulo:'articulo/:id',
    articulosSearch:'articulos',
    articuloId:'articulo/:id',
    compraEditId:'compraedit/:id',
    compraItem:'compra/item',
    canjeItem:'canje/item',
    compraDescripcionSugerencia:'compras/descripciones',
    dash:'dash',
    usuariosGetAll:'usuarios',
    compraCreate:'compra',
    compra:'compra',
    canje:'canje',
    canjeCreate:'canje',
    logout:'logout',
    uploadArticuloImagen:'articulo/imagen/',
    config:'config',
    acceptTermsConditions:'termsconditions',
    rankingClientes:'ranking/clientes',
    rankingExport:'ranking/clientes/export',
    reporteCanjes:'reporte/canjes',
    articuloCategorias:'articulo/categorias/all',
    articuloCategoriasCreate:'articulo/categoria/new',
    updateCategoryTree:'articulo/categoria/subtree',
    articuloCategoriaDelete:'articulo/categoria/delete',
    cambiarPassword:'usuario',
    suggestLocation: 'formFilter/location',
    suggestCliente: 'formFilter/cliente',
    setLogo:'imgs/logo',
    sucursalesGetAll:'sucursales',
    sucursal:'sucursal'
};
function getUrl(route)
{
    return 'api/' + _resource[route];
}

var mainApp = angular.module('mainApp',
  [
    'ngRoute',
    'ngResource',
    'mk.editablespan',
    'cp.ngConfirm',
    'angular-chartist'
  ]);
    mainApp.factory('Clientes', ['$resource', function($resource) {
        return $resource(getUrl('clienteId'), {id:'@id'}, {
            'update': { method: 'PUT' }
        });
    }]);
    mainApp.factory('Compras', ['$resource', function($resource) {
        return $resource(getUrl('compraEditId'), {id:'@id'}, {
            'update': { method: 'PUT' }
        });
    }]);
    mainApp.factory('Usuarios', ['$resource', function($resource) {
        return $resource(getUrl('usuarioId'),{id:'@id'}, {
            'update': { method: 'PUT' }
        });
    }]);
    mainApp.factory('Articulos', ['$resource', function($resource) {
        return $resource(getUrl('articuloId'), {id:'@id'},
        {
            'update': { method:'PUT'}
        });
    }]);
    mainApp.factory('MetodosIngreso', ['$resource', function($resource) {
        return $resource(getUrl('mi') + '/:id', {id:'@id'},
        {
            'update': { method:'PUT'}
        });
    }]);
    mainApp.factory('httpRequestInterceptor', function ($q,session) {
      return {
        request: function (config) {
            config.headers['Authorization'] = 'Bearer ' +  localStorage.getItem('token');
            return config;
            },
        response: function (response) {
                if(response.msj)
                {
                    toastr.error(response.msj);
                }
                return response;
            },
        'responseError': function(rejection) {
            var data = rejection.data;

            if(data.msj)
            {
                toastr.error(data.msj);
            }else if (data.errors)
            {
                toastr.error(Object.values(data.errors).map((arr)=>arr[0]).join("<br>"));
            }
            else if (data.error)
            {
                toastr.error(data.error);
            }
            if(rejection.status==401)
            {
                errorMsg('Sesion >> Redireccionando a pagina de login','Error de autenticación');
                session.clear();
                window.setTimeout( function(){
                    window.location = "login?reject=1";
                }, 4000 );
            }
            if(rejection.status == 423)
            {
                errorMsg('Instancia en pausa','Manager de instancias');
                session.clear();
                window.setTimeout( function(){
                    window.location = "login?reject=2";
                }, 4000 );
            }
            return $q.reject(rejection);
        }
    };
    });
    mainApp.config(function ($httpProvider) {
      $httpProvider.interceptors.push('httpRequestInterceptor');
    });
    mainApp.filter('sinDatos', function() {
    return function(input, defaultValue) {
        if (angular.isUndefined(input) || input === null || input === '') {
            return defaultValue;
        }
        return input;
        }
    }).filter("asDate", function () 
    {
        return function (input) 
        {
            if(typeof input != 'undefined')
            {
                return new Date(input.replace(/\s/, 'T'));
            }
            return "";
        }
    })
    .filter("asDate2", function () 
    {
        return function (input) 
        {
            if(typeof input != 'undefined')
            {
                return new Date(input);
            }
            return "";
        }
    });
    mainApp.directive('ngEnter', function() {
        return function(scope, element, attrs) {
            element.bind("keydown keypress", function(event) {
                if(event.which === 13) {
                        scope.$apply(function(){
                                scope.$eval(attrs.ngEnter);
                        });
                        
                        event.preventDefault();
                }
            });
        };
}).config(['$provide', function($provide) {
        $provide.decorator('$locale', ['$delegate', function($delegate) {
            $delegate.NUMBER_FORMATS.DECIMAL_SEP = ',';
            $delegate.NUMBER_FORMATS.GROUP_SEP = '.';
            return $delegate;
        }]);
    }]);
angular.module("contextMenu", []);
mainApp.config(
function($routeProvider){
    $routeProvider
    .when('/main',
    {
        controller: 'sesion',
        templateUrl: 'view/dash'
    })
    .when('/clientes',
    {
        controller: 'clientes',
        templateUrl: 'view/clientes'
    })
    .when('/config',
    {
        controller: 'config',
        templateUrl: 'view/config'
    })
    .when('/articulos',
    {
        controller: 'articulos',
        templateUrl: 'view/articulos'
    })
    .when('/articulo/alta',
    {
        controller: 'articuloAlta',
        templateUrl: 'view/articulo/alta'
    })
    .when('/articulo/edit/:id',
    {
        controller: 'articuloEdit',
        templateUrl: 'view/articulo/edit'
    })
    .when('/mi',
    {
        controller: 'mi',
        templateUrl: 'view/mis'
    })
    .when('/mi/alta',
    {
        controller: 'miAlta',
        templateUrl: 'view/mi/alta'
    })
    .when('/mi/edit/:id',
    {
        controller: 'miEdit',
        templateUrl: 'view/mi/edit'
    })
    .when('/cliente/edit/:id',
    {
        controller: 'clienteEdit',
        templateUrl: 'view/cliente/edit'
    })
    .when('/cliente/edit/:id/transacciones',
    {
        controller: 'clienteTransacciones',
        templateUrl: 'view/cliente/transacciones'
    })
    .when('/cliente/alta',
    {
        controller: 'clienteAlta',
        templateUrl: 'view/cliente/alta'
    })
    .when('/usuarios',
    {
        controller: 'usuarios',
        templateUrl: 'view/usuarios'
    })
    .when('/usuario/edit/:id',
    {
        controller: 'usuarioEdit',
        templateUrl: 'view/usuario/edit'
    })
    .when('/usuario/alta',
    {
        controller: 'usuarioAlta',
        templateUrl: 'view/usuario/alta'
    })
    .when('/cambiarpass/:id',
    {
        controller: 'cambiarpass',
        templateUrl: 'view/cambiarPassword'
    })
    .when('/operacion',
    {
        controller: 'operacionController',
        templateUrl: 'view/operacion'
    })
    .when('/operacion/:clienteId',
    {
        controller: 'operacionController',
        templateUrl: 'view/operacion'
    })
    .when('/reporte',
    {
        controller: 'reporte',
        templateUrl: 'view/reporte'
    })
    .when('/ranking',
    {
        controller: 'ranking',
        templateUrl: 'view/ranking'
    })
    .when('/contacto',
    {
        controller: 'sesion',
        templateUrl: 'view/contactoSoporte'
    })
    .when('/sucursales',
    {
        controller: 'sucursalTableController',
        templateUrl: 'view/sucursales'
    })
    .when('/sucursal/alta',
    {
        controller: 'sucursalAltaController',
        templateUrl: 'view/sucursal/alta'
    })
    .otherwise({ redirectTo:'/main'});
});

var controladores = {};
    controladores.sesion = function(
        $scope,
        $http,
        MetodosIngreso,
        Usuarios,
        $rootScope,
        swipeService,
        session,
        $ngConfirm
        )
    {  
        $scope.setInputMode = (method)=>
        {
            isManual = method=='0';
            if(!isManual)
            {
                $scope.getMis();
            }
            $rootScope.manualCardInput = isManual;
        }
        $scope.$watch(function() {
            return session.get('inputMethodId');
        }, function(methodId) {
            $scope.setInputMode(methodId);
            $scope.inputMethodId = methodId;

            if(!$scope.manualCardInput)
            {
                $scope.inputMethod = swipeService.getInputMethodId();
                swipeService.enable();
            }
            else
            {
                swipeService.disable();
            }
        });
      
        $scope.setInputMethod = (method={id:0,nombre:'manual'})=>
        {
            $http
            .post(getUrl('miByAuth'),{id:method.id})
            .then(function(response){
                    response = response.data;
                    if(response.inputMethodSetted)
                    {
                        swipeService.setInputMethod(method);
                    }
                }
            );
        }

        $scope.getMis = function(item) 
        {
            $http.get(getUrl("mi")).then(function(response){
                response = response.data;
                $scope.mitodos = response.todo;
            });
        };

        $scope.logout = function()
        {
            $http.post(getUrl('logout'),{})
            .then(function(response){
                session.logOut();
                location.assign('login');
            },
            function(response)
            {
                toastr.error("Er")
            });
        }
        $scope.showModalTermsConditions = ()=>
        {

            $scope.modalTermsAndConditions = 
            $ngConfirm({
                    title: '',
                    escapeKey: true,
                    content: `
                    <div style="height:200px;">
                    <h3>Términos y condiciones</h3>
                    <div style="overflow-y: scroll; height: 80px;">
                        <p>
                            `+session.get('termsAndConditions')+`
                        </p>
                    </div>
                    <div class="row" style="margin-top:15px;">
                        <div class="text-center">
                            <button ng-click="closeModalTermsConditions()" class="btn 
                            btn-default m-r-3 buttonMinimum">
                                Cerrar
                            </button>
                            <button ng-click="acceptTermsConditions()" 
                            class="btn btn-info buttonMinimum">
                                Aceptar
                            </button>
                        </div>
                    </div>
                    </div>
                    `,
                    scope: $scope,
                    columnClass: 'col-md-6 col-md-offset-3',
      
                });
        }
        $scope.closeModalTermsConditions = ()=>
        {
            $scope.modalTermsAndConditions.close();
        }
        $scope.acceptTermsConditions = ()=>
        {
            $scope.closeModalTermsConditions();
            $http.post(getUrl('acceptTermsConditions'))
            .then((response)=>
            {
                if(response.data.save)
                {
                    toastr.success('Gracias');
                    session.delete('acceptedTermsConditions');
                }
            })
        }

        $scope.usuarioFullName = session.get('nombre') + ' ' +  session.get('apellido');
        $scope.comercioName = session.get('usrComercio') || 'Configuración';

        $scope.tyc = session.get('tyc');

        if(session.get("acceptedTermsConditions")==="0")
        {
            toastr.remove();
            toastr.warning('Leer terminos y condiciones del servicio', 'Importante',{
                    timeOut: 0,
                    onclick: 
                    function() {
                        $scope.showModalTermsConditions();
                    }
                }
            );
        }
        if(session.get("tipoId") == '1')
        {
            $scope.admin = true;
        }else
        {
            $('.admin').hide();
        }
    }
    controladores.dash = function($scope,$http)
    {
        $scope.chartsData = {};
        $scope.chartsData.buysAvgs = {};
        $scope.chartsData.swapsAvgs = {};

        $scope.chartsData.swapsAvgs.events=
        $scope.chartsData.buysAvgs.events= {
            draw:(data)=>
            {
                if(data.type === 'bar') 
                {
                    data.group.append(new Chartist.Svg('circle', {
                      cx: data.x2,
                      cy: data.y2,
                      r: 8
                    }, 'ct-slice-pie'));
                }
            }
        }

        $scope.setChartsData=(dash)=>
        {
            $buysAvgsData = [
                [
                dash.compra.usr.siempre.AVG,
                dash.compra.usr.lastMonth.AVG,
                ],
                [
                dash.compra.global.siempre.AVG,
                dash.compra.global.lastMonth.AVG,
                ]
            ];

            $swapsAvgsData = [
                [
                    dash.canje.usr.siempre.AVG,
                    dash.canje.usr.lastMonth.AVG
                ],
                [
                    dash.canje.global.siempre.AVG,
                    dash.canje.global.lastMonth.AVG,
                ]    
            ];

            $scope.chartsData.buysAvgs.data = {
              labels: ['USR/GLOBAL Siempre', 'USR/GLOBAL Mes'],
              series: $buysAvgsData,
              
            }, {
              high: 10,
              low: -10,
              axisX: {
                labelInterpolationFnc: function(value, index) {
                  return index % 2 === 0 ? value : null;
                }
              }
            }
            $scope.chartsData.swapsAvgs.data = {
              labels: ['USR/GLOBAL Siempre', 'USR/GLOBAL Mes'],
              series: $swapsAvgsData
            };
            $scope.chartsData.userGlobal = {};

            $usrCanjesPorciento = (dash.canje.usr.siempre.COUNT/dash.canje.global.siempre.COUNT)*100;
            $scope.chartsData.userGlobal.data={
                labels:['Global','Usuario'],
                series:[100-$usrCanjesPorciento,$usrCanjesPorciento]
            }
            $scope.chartsData.monthCompare = {};

            $scope.chartsData.monthCompare.data = {
                series:[
                            {
                                name:'comparativa',
                                data:[
                                dash.compra.comparativa.haceDosMeses,
                                dash.compra.comparativa.haceUnMes,
                                dash.compra.comparativa.esteMes,
                                ]
                            }
                        ],
                labels:['Dos meses atrás','Un mes atrás','Este mes']
            }
            $scope.chartsData.monthCompare.option = {
                series: {
                        'comparativa': {
                          lineSmooth: Chartist.Interpolation.simple(),
                          showArea:true
                        }
                      }
            }
        }

        $.blockUI({ message: '<img src="img/login/loading.gif">' });
        $http.get(getUrl('dash')).then(function(response){
            $scope.dash = response.data;
            $scope.setChartsData($scope.dash);
            $.unblockUI();
        });
        
    }
    mainApp.controller(controladores);
mainApp.controller("articulos",function($scope,$http, Clientes,Articulos){
        $scope.inpSearch = '',$scope.saving = false;
        $scope.initParams = (object={})=>
        {
            $scope.params = Object.assign({
                page:1
            },object);    
        }
        $scope.showMore = ()=>
        {
            $scope.params.page++;
            $scope.getAll($scope.params);
        }
        $scope.search = (queryString)=>
        {
            $scope.searchString = queryString;
            $scope.getAll($scope.initParams({queryString}));
        }

        $scope.getAll = (params=$scope.params,append=true) =>
        {
            typeof $scope.todo == 'undefined' && ($scope.todo=[]);
            $http(
                {
                    url: getUrl('articulosAll'), 
                    method: "GET",
                    params
                 })
            .then(function(response)
            {
                $scope.lastPage = response.data.last_page;
                if(append)
                {
                    response.data.data.forEach((articulo)=>{
                        !$scope.todo.find((e)=>e.id == articulo.id) &&
                        $scope.todo.push(articulo);
                    });    
                }else {
                    $scope.todo = response.data.data;
                }
                $scope.modif = response.modif;
            });
        }
        $scope.initParams();
        $scope.getAll();
        $scope.clearSearch = ()=>
        {
            $scope.inpSearch = $scope.searchString = '';
            delete $scope.params.queryString;
            $scope.getAll($scope.initParams(),false);
        }
        $scope.edit = (articulo)=>{
            location.assign("#!/articulo/edit/"+articulo.id);
        }
        $scope.delete = function(item,index) {
                Articulos.remove({id:item.id}).$promise.then(function(response){
                    if(response.delete) {
                        toastr.info("Eliminado");
                        $scope.todo.splice(index,1);
                    }
                });
            };
        $scope.export = (format)=>
        {
            let params= {format};
            $http(
                {
                    url: getUrl('articulosExport'), 
                    method: "GET",
                    params
                 }).then(function(response)
                {
                    downloadFromHttpResponse(response);
                });
        }    
    });
mainApp.controller("articuloAlta",function($scope,$http,$routeParams, Articulos)
{

        $scope.uploadFile = function(files) {
            var fd = new FormData();
            fd.append("img", files[0]);
            token = localStorage.getItem('token');
            toastr.info('Subiendo imagen');
            $http.post(getUrl('uploadArticuloImagen')+$scope.altaArticuloId, fd, {
                withCredentials: true,
                headers: {'Content-Type': undefined ,auth:token },
                transformRequest: angular.identity
            }).then(function(reponse){$.unblockUI();},function(response)
            {   
                $.unblockUI();
                toastr.info('Podés volver a intentar agregar imagen al artículo desde la pantalla de edición')
            })
        };
        $scope.alta = function(item) {
            $scope.saving = true;
             Articulos.save({},$scope.articulo).$promise.then(function(response){
                $scope.saving = false;
                if(response.alta) {
                    $scope.altaArticuloId=response.alta.id;
                    toastr.success('Alta de articulo exitosa.');
                    $.blockUI({ message: '<img src="img/login/loading.gif">' });
                    if($('#file').prop('files').length > 0)
                    {
                        $scope.uploadFile($('#file').prop('files'));
                    }
                    setTimeout(function(){ window.location.href = "#!/articulos";$.unblockUI(); }, 1000);
                }
                if(response.msj){
                    toastr.error(response.msj);
                }
            },function(){
                $scope.saving = false;
            });
        };

    });
mainApp.controller("articuloEdit",function($scope,$http,$routeParams, Articulos)
{
    $scope.$watch(function() {
        return $scope.articulo.categoriaId;
    }, function(categoryId) {
        $scope.onReady();
        if(categoryId)
        {
            $http({
                params:{
                    categoryId
                },
                url:getUrl('updateCategoryTree'),
                method:"GET"
            })
            .then(function(response)
            {
                $scope.articulo.categoriaTree = response.data;
            });
        }
    });
        $scope.articulo = Articulos.get({id:$routeParams.id});
        if($scope.articulo.$resolved && $scope.articulo.$resolved == false)
        {
            window.location.href = "#!/articulos";
        }

        $scope.onReady = function() {
            $scope.cambio = true;
        };
        $scope.delete = function(item = $scope.articulo) {
            Articulos.remove({id:item.id}).$promise.then(function(response){
                if(response.delete) {
                    toastr.info("Eliminado");
                    location.assign("#!/articulos");
                }
            });
        };    
        $scope.uploading=false;
         $scope.uploadFile = function(files) {
            var fd = new FormData();
            fd.append("img", files[0]);
            token = localStorage.getItem('token');
            toastr.info('Subiendo imagen');
            $scope.uploading = true;
            $.blockUI({ message: '<img src="img/login/loading.gif">' });
            $http.post(getUrl('uploadArticuloImagen')+$routeParams.id, fd, {
                withCredentials: true,
                headers: {'Content-Type': undefined ,auth:token },
                transformRequest: angular.identity
            }).then(function(response){
                $.unblockUI();
                toastr.success('Imagen modificada');
                $scope.articulo.imgUrl = response.data.url;
                changeImg(files[0],$('#imgArticulo'));
                $scope.uploading = false;
            },function(response)
            {   
                $.unblockUI();
                toastr.info('Podés volver a intentar agregar imagen al artículo desde la pantalla de edición')
            })
        };
        $scope.setImagen = function(id)
        {
            $scope.articulo.imagen = id;
        }

        $scope.guardar = function(item) {
            $.blockUI({ message: '<img src="img/login/loading.gif">' });
            Articulos.update({id:$routeParams.id}, $scope.articulo).$promise.then(
                function(response){
                    if(response.msj)
                    {
                        $.unblockUI();
                        toastr.error(response.msj);
                    }
                    if(response.update)
                    {
                        $scope.cambio = false;
                        $.unblockUI();
                        toastr.success('Cambios guardados');    
                    }
                    
            },function(rejection){
                $.unblockUI();
            });
        };
    });
mainApp.controller("config",function($scope,$http, Clientes){
        $http.get(getUrl('config')).then(function(response){
            response = response.data;
            $scope.config = response;
        });
        $scope.onReady = function(item) {
            $scope.cambio = true;
        };
        $scope.uploadFile = function(files) {
            var fd = new FormData();
            fd.append("img", files[0]);
            $http.post(getUrl('setLogo'), fd, {
                withCredentials: true,
                headers: {'Content-Type': undefined },
                transformRequest: angular.identity
            }).then(function(response){
                response = response.data;
                if(response.img)
                {
                    toastr.success('Logo cambiado');
                    changeImg(files[0],$('#logo'));
                    changeImg(files[0],$('#navbar-logo'));
                }
                },
                function(response)
                {
                        toastr.error('Error en cambio de logo');
                })
        };
        $scope.guardar = function()
        {
            $http.put(getUrl('config'),$scope.config)
            .then(function(response){
                response = response.data;
                if(response.cambio_config)
                {
                    toastr.success('Cambios guardados');
                }
            });
        }
    });

 mainApp.controller("clienteCompras",function($scope,$http,$routeParams,Clientes, Compras)
 {
        $scope.cliente = Clientes.get({id:$routeParams.id});

        $scope.editItem = function(index)
        {
            $scope.editingItem = $scope.compras[index];
        }
        $http.get('compras/all/'+$routeParams.id)
        .then(function(response)
            {
                $scope.compras = response.data.todo;
            });
        $scope.update = function(compra)
        {
            Compras.update(compra).$promise.then(function(response)
                {
                    toastr.success("Compra actualizada");
                });
        }
        $scope.elim = function(compra) {
            $scope.eliminandoCompra =true;
            Compras.remove({id:compra.id}).$promise.then(function(response){
                if(response.delete) {
                    toastr.info("Eliminada");
                    window.location.href = "#!/cliente/edit/"+$routeParams.id;
                    $scope.eliminandoCompra = false;
                }
            },function(rejection){$scope.eliminandoCompra=false;});
        };
    });
    mainApp.controller("clientes",function($scope,$http, Clientes){
        $scope.inpSearch = '';
        $scope.initParams = (object={})=>
        {
            return $scope.params = Object.assign({
                page:1
            },object);    
        }
        $scope.showMore = ()=>
        {
            $scope.params.page++;
            $scope.getAll();
        }
        $scope.search = (queryString)=>
        {
            $scope.searchString = queryString;
            $scope.params = Object.assign({queryString},{page:1});
            $scope.getAll($scope.params);
        }

        $scope.getAll = (params=$scope.params,append=true)=>
        {
            typeof $scope.todo == 'undefined' && ($scope.todo=[]);
            $http(
                {
                    url: getUrl('clientesAll'), 
                    method: "GET",
                    params
                 }).then(function(response)
                {
                    $scope.lastPage = response.data.last_page;
                    if(append)
                    {
                        response.data.data.forEach((cliente)=>{
                            !$scope.todo.find((e)=>e.id == cliente.id) &&
                            $scope.todo.push(cliente);
                        });    
                    }else {
                        $scope.todo = response.data.data;
                    }
                    
                    $scope.modif = response.modif;
                });
        }
        $scope.initParams();
        $scope.getAll();
        $scope.clearSearch = ()=>
        {
            $scope.inpSearch = $scope.searchString = '';
            delete $scope.params.queryString;
            $scope.getAll($scope.initParams(),false);
        }
         $scope.delete = function(cliente,index) {
            Clientes.remove({id:cliente.id}).$promise.then(function(response){
                if(response.delete) {
                    toastr.info("Eliminado");
                    $scope.todo.splice(index,1);
                }
            });
        };
        $scope.export = (format)=>
        {
            let params= {format};
            $http(
                {
                    url: getUrl('clientesExport'), 
                    method: "GET",
                    params
                 }).then(function(response)
                {
                    downloadFromHttpResponse(response);
                });
        }
    });
    mainApp.controller("clienteEdit",function($scope,$http,$routeParams, Clientes,$rootScope){
        $scope.cliente = Clientes.get({id:$routeParams.id});
        $scope.changeUbication = false;
        $scope.onReady = function(item) {
            $scope.cambio = true;
        };

        if(localStorage.getItem("tipo") != '0')
        {
            $('.admin').hide();
        }
        $scope.setIdMagnetico = function(data)
        {
            $scope.cliente.idmagtarjeta = data.idMagnetico;
            $scope.$apply();
        }
        $scope.newOperation = ()=>
        {
            location.assign('#!/operacion/'+$routeParams.id);
        }
        $scope.rootscopeAltaReadSuccess = $rootScope.$on('card-read-success',(event,data)=>
        {
            $scope.setIdMagnetico({idMagnetico:data});
        });
        $scope.guardar = function(item) {
            $.blockUI({ message: '<img src="img/login/loading.gif">' });
            Clientes.update({id:$routeParams.id}, $scope.cliente).$promise.then(function(response){
                    $scope.cambio = false;
                    $.unblockUI();
                    if(response.update)
                    {
                        $scope.changeUbication = false;
                        $scope.cliente = response.update;
                        toastr.success('Cambios guardados');    
                    }
                    if(response.msj)
                    {
                        toastr.error(response.msj);
                    }
                
            },function(rejection)
            {
                 $.unblockUI();
            });
        };
        $scope.suggestLocation = (inpString)=>
        {
            return $http({
                url:getUrl('suggestLocation'),
                params:{
                    queryString:inpString
                },
                method:"GET"
            })
            .then((response)=>
            {
                $scope.locationSuggestions = response.data;
            });
        }
        $scope.delete = function(cliente=$scope.cliente) {
            Clientes.remove({id:cliente.id}).$promise.then(function(response){
                if(response.delete) {
                    toastr.info("Eliminado");
                    location.assign("#!/clientes");
                }
            });
        };
        $scope.goToTransactions = ()=>
        {
            location.assign('#!/cliente/edit/'+$routeParams.id+'/transacciones');
        }
        $http({
                url:getUrl('clienteTransacciones'),
                params:{
                    clienteId:$routeParams.id
                },
                method:"GET"
        })
        .then(function(response){
            response = response.data.todo;
            $scope.actividades = response;
        });

    });
    mainApp.controller("clienteAlta", function($scope,$http,$routeParams, Clientes,$rootScope,$ngConfirm){
       $scope.cliente = {}, $scope.saving = false;
        $scope.alta = function(item) {
            $scope.saving = true;
            Clientes.save({},
                Object.assign($scope.cliente,{
                    locacion:2
                }))
            .$promise.then(function(response){
                $scope.saving = false;
                if(response.alta) {
                    window.location.href = "#!/clientes";
                }
                if(response.msj){
                    toastr.error(response.msj);
                }
            },function()
            {
                $scope.saving = false;
            });
        };
        $scope.suggestLocation = (inpString)=>
        {
            return $http({
                url:getUrl('suggestLocation'),
                params:{
                    queryString:inpString
                },
                method:"GET"
            })
            .then((response)=>
            {
                $scope.locationSuggestions = response.data;
            });
        }
        $scope.setIdMagnetico = function(data)
        {
            $scope.cliente.idmagtarjeta = data.idMagnetico;
            $scope.$apply();
        }
        $scope.rootscopeAltaReadSuccess =
        $rootScope.$on('card-read-success',(event,data)=>
        {
            $scope.setIdMagnetico({idMagnetico:data});
        });
        $scope.$on("$destroy",function()
        {
            $scope.rootscopeAltaReadSuccess();
        });

    });
   mainApp.controller("clienteTransacciones",function($scope,$http,$routeParams, Clientes,$rootScope,$ngConfirm){
        
        $scope.clearInputSearch = ()=>
        {
            return $scope.inpSearch = {
                'fromDate':'',
                'toDate':''
            };
        }
        $scope.clearInputSearch();
        $scope.getCliente = ()=>
        {
            $scope.cliente = Clientes.get({id:$routeParams.id});    
        }
        $scope.getCliente();
        $scope.initParams = (object={})=>
        {
            return $scope.params = Object.assign(object,{
                page:1,
                clienteId:$routeParams.id
            });    
        }

        $scope.showMore = ()=>
        {
            $scope.params.page++;
            $scope.getAll($scope.params,true);
        }

        $scope.delete = (item,index)=>
        {
            $scope._delete(item,index);
        }
        $scope.edit = (item)=>
        {
            $scope._methodResolver(item,'Edit');
        }
        $scope._methodResolver = (item,action)=>
        {
            $itemType = item.idCompra?"compra":"canje";
            $scope[$itemType+action](item);
        }
        $scope.compraEdit = (compra)=>
        {
            if(!compra.edit)
            {
                $scope.getCompraDetails(compra);
            }
            compra.edit = !compra.edit;
        }
        $scope.getCompraDetails = (compra)=>
        {
            $http.get(getUrl('compra')+'/items',{params:{id:compra.idCompra}})
                .then(function(response)
                {
                    compra.items = response.data;
                });
        }
        $scope.compraItemEdit = (compraItem)=>
        {
            $http.put(getUrl('compra')+'/item',compraItem)
                .then(function(response)
                {
                    if(response.data.item)
                    { 
                        $scope.getAll($scope.params);
                    }
                });
        }
        $scope.canjeEdit = (canje) =>
        {
            if(!canje.edit)
            {
                $http.get(getUrl('canje')+'/items',{params:{id:canje.idCanje}})
                .then(function(response)
                {
                    canje.items = response.data;
                });
            }           
            canje.edit =! canje.edit;
        }
        $scope.changes = (item)=>
        {
            item.changes = true;
        }
        $scope._delete = (item,index)=>
        {
            $http({
                    params:{id:item.idCompra?item.idCompra:item.idCanje},
                    url:getUrl(item.idCompra?"compra":"canje"),
                    method:"DELETE"
                }).then(function(response)
                {
                    if(response.data.delete)
                    {
                        item.deleted_at = true;
                        //$scope.getCliente();
                    }
                },function(rejection)
                {
                    rejection.data.error && toastr.error(rejection.data.error);
                });
        }
        $scope.deleteItem = (parentItem,item,index)=>
        {
            let params = {
                id:item.id
            },url = getUrl(parentItem.idCompra?'compraItem':'canjeItem');
            $http({
                    params:params,
                    url:url,
                    method:"DELETE"
                }).then(function(response)
                {
                    parentItem.puntos -= item.puntos;
                    if(response.data.delete)
                    {
                        parentItem.items.splice(index,1);
                    }else if(response.data.deleteParent)
                    {
                        $scope.getAll($scope.params);
                        toastr.info('Se ha anulado la transaccion');
                    }
                },function(rejection)
                {
                    rejection.data.error && toastr.error(rejection.data.error);
                });
        }
        
        $scope.getAll = (params=$scope.params,append=false)=>
        {
            typeof $scope.todo == 'undefined' && ($scope.todo=[]);
               return $http({
                    url:getUrl('clienteTransacciones'),
                    params,
                    method:"GET"
                }).then(function(response)
                {
                    $scope.lastPage = response.data.totalPages;
                    if(append)
                    {
                        response.data.todo.forEach((item)=>{
                        $scope.todo.push(item);
                    });
                    }else {
                        $scope.todo = response.data.todo;
                    }
                    
                });
        }
        $scope.filtrar = (inpSearch)=>
        {
            $scope.getAll($scope.initParams(inpSearch))
            .then(()=>
            {
                $scope.actualFilter = inpSearch;
            });
        }
        $scope.clearFilter = () =>
        {
            $scope.initParams();
            $scope.getAll()
            .then(()=>
            {
                delete $scope.actualFilter;
                $scope.clearInputSearch();
            });
        }
        $scope.initParams();
        $scope.getAll();

    });
 mainApp.controller("usuarios",function($scope,$http, Usuarios){
        $scope.inpSearch = '';
        $scope.initParams = ()=>
        {
            $scope.params = {
                page:1
            };    
        }
        $scope.showMore = ()=>
        {
            $scope.params.page++;
            $scope.getAll($scope.params);
        }
        $scope.search = (queryString)=>
        {
            $scope.searchString = queryString;
            $scope.params = Object.assign({queryString},$scope.initParams);
            $scope.getAll($scope.params);
        }
        $scope.getAll = (params=$scope.initParams()) =>
        {    
            typeof $scope.todo == 'undefined' && ($scope.todo=[]);
            
            $http(
                {
                    url: getUrl('usuariosGetAll'), 
                    method: "GET",
                    params
                 })
            .then(function(response){
                $scope.lastPage = response.data.last_page;
                response.data.data.forEach((usuario)=>{
                        !$scope.todo.find((e)=>e.id == usuario.id) &&
                        $scope.todo.push(usuario);
                    });
                    $scope.modif = response.modif;
            });
        }
        $scope.getAll();
         $scope.clearSearch = ()=>
        {
            $scope.inpSearch = $scope.searchString = '';
            $scope.getAll();
        }
        $scope.delete = function(item) {
            Usuarios.remove({id:item.id}).$promise.then(function(response){
                if(response.delete) {
                    toastr.info("Eliminado");
                    $scope.getAll();
                }
            },function(rejection){toastr.error(rejection.data.msg);});
        };
        $scope.export = (format)=>
        {
            let params= {format};
            $http(
                {
                    url: getUrl('usuariosExport'), 
                    method: "GET",
                    params
                }).then(function(response)
                {
                    downloadFromHttpResponse(response);
                });
        }
    });
    mainApp.controller("usuarioEdit",function($scope,$http,$routeParams, Usuarios){
        $scope.usuario = Usuarios.get({id:$routeParams.id});

        $scope.onReady = function(item) {
            $scope.cambio = true;
        };
        $scope.delete = function(item=$scope.usuario) {
            Usuarios.remove({id:$routeParams.id}).$promise.then(function(response){
                if(response.delete) {
                    toastr.info("Eliminado");
                    window.location.href = "#!/usuarios";
                }
            },function(rejection){toastr.error(rejection.data.msg);});
        };
        $scope.guardar = function(item) {
            $.blockUI({ message: '<img src="img/login/loading.gif">' });
            Usuarios.update({id:$routeParams.id}, $scope.usuario).$promise.then(
                function(response){
                    $scope.cambio = false;
                    $.unblockUI();
                    toastr.success('Cambios guardados');
                },function()
                {
                    $.unblockUI();
                }
                );
        };

    });
    mainApp.controller("usuarioAlta",function($scope,$http,$routeParams, Usuarios){
       $scope.usuario = {},$scope.saving = false;
        $scope.alta = function(item) {
            $scope.saving = true;
             Usuarios.save({},$scope.usuario).$promise.then(function(response){
                if(response.alta) {
                    window.location.href = "#!/usuarios";
                }
                if(response.msj){
                    toastr.error(response.msj);
                }
            }, function()
            {
                $scope.saving = false;
            });
        };

    });
    mainApp.controller("cambiarpass",function($scope,$http,$routeParams, Usuarios){
        $scope.usuario = Usuarios.get({id:$routeParams.id});
        $scope.url = 'cambiarpass/usuario/'+ $scope.usuario.id;
        $scope.inpPassword = {};
       

        $scope.cambiar = function(pass) {
            
            $http.put(getUrl('cambiarPassword')+'/'+ $routeParams.id,$scope.inpPassword)
            .then(function(response){
                response = response.data;
                if(response.update)
                {
                    toastr.success('Password cambiada con exito');
                    window.location.href = "#!/usuarios";
                }
                else if (response.msj)
                {
                    toastr.error(response.msj);
                }
            });
        };

    });

  mainApp.controller("sucursalTableController",function($scope,$http,$routeParams, Clientes,$rootScope,$ngConfirm){
        
        $scope.initParams = ()=>
        {
            return $scope.params = {
                page:1
            }
        }
        $scope.search = (queryString)=>
        {
            $scope.searchString = queryString;
            $scope.params = Object.assign({queryString},$scope.initParams());
            $scope.getAll($scope.params);
        }
        $scope.getAll = (params=$scope.params,append=false)=>
        {
            typeof $scope.todo == 'undefined' && ($scope.todo=[]);
               return $http({
                    url:getUrl('sucursalesGetAll'),
                    params,
                    method:"GET"
                }).then(function(response)
                {
                    $scope.lastPage = response.data.last_page;
                    if(append)
                    {
                        response.data.data.forEach((item)=>{
                        $scope.todo.push(item);
                    });
                    }else {
                        $scope.todo = response.data.data;
                    }
                    
                });
        }
        $scope.edit = (sucursal)=>
        {
            sucursal.edit = true;
        }
        $scope.editing = (sucursal)=>
        {
            return sucursal.edit === true;
        }
        $scope.save = (sucursal,index)=>
        {
            $http({
                url:getUrl('sucursal'),
                method:"PUT",
                params:sucursal
            })
            .then((response)=>
            {
                sucursal.edit = false;
                $scope.todo[index] = response.data.update;
                toastr.success('Sucursal modificada con éxito');
            })
        }
        $scope.delete = (sucursal,index)=>
        {
            $http({
                url:getUrl('sucursal'),
                method:"DELETE",
                params:{
                    id:sucursal.id
                }
            })
            .then((response)=>
            {
                toastr.success('Sucursal eliminada con éxito');
                $scope.todo.splice(index,1)
            })
        }
        $scope.suggestLocation = (inpString)=>
        {
            return $http({
                url:getUrl('suggestLocation'),
                params:{
                    queryString:inpString
                },
                method:"GET"
            })
            .then((response)=>
            {
                $scope.locationSuggestions = response.data;
            });
        }
        $scope.initParams();
        $scope.getAll();
        $scope.clearSearch = ()=>
        {
            $scope.inpSearch = $scope.searchString = '';
            delete $scope.params.queryString;
            $scope.getAll();
        }

    })
  .controller("sucursalAltaController",function($scope,$http,$routeParams, Clientes,$rootScope,$ngConfirm){
        
       $scope.sucursal = {}, $scope.saving = false;
       $scope.suggestLocation = (inpString)=>
        {
            return $http({
                url:getUrl('suggestLocation'),
                params:{
                    queryString:inpString
                },
                method:"GET"
            })
            .then((response)=>
            {
                $scope.locationSuggestions = response.data;
            });
        }
        $scope.alta = ()=>
        {
            $scope.saving = true;
            $http.post(getUrl('sucursal'),$scope.sucursal)
            .then((response)=>
            {
                $scope.saving = false;
                response = response.data;
                if(response.alta)
                {
                    toastr.success('Sucursal creada con éxito');
                    location.assign('#!/sucursales');
                }
            },function(){
                $scope.saving = false;
            });
        }
    });
 mainApp.controller("mi",function($scope,$http, Usuarios,MetodosIngreso){
        $scope.getAll = ()=>{
                $http.get(getUrl("mis")).then(function(response){
                response = response.data;
                $scope.todo = response.todo;
            });
        }
        $scope.getAll();
        $scope.delete = (metodo)=>{
            MetodosIngreso.remove({id:metodo.id}).$promise.then(function(response){
                if(response.delete) {
                    toastr.info("Eliminado");
                    $scope.getAll();
                }
            },function(error)
            {
                toastr.error('Accion no permitida');
            });
        }
    });
    mainApp.controller("miAlta",function($scope,$http,$routeParams, MetodosIngreso){
       
        $scope.alta = function(item) {
             MetodosIngreso.save({},$scope.mi).$promise.then(function(response){
                if(response.alta) {
                    window.location.href = "#!/mi";
                }
                if(response.msj){
                    toastr.error(response.msj);
                }
            });
        };
    });
    mainApp.controller("miEdit",function($scope,$http,$routeParams, MetodosIngreso){
        $scope.mi = MetodosIngreso.get({id:$routeParams.id});

        $scope.onReady = function(item) {
            $scope.cambio = true;
        };
        $scope.delete = (metodo=$scope.mi)=>{
            MetodosIngreso.remove({id:metodo.id}).$promise.then(function(response){
                if(response.delete) {
                    toastr.info("Eliminado");
                    location.assign('#!/mi')
                }
            },function(error)
            {
                toastr.error('Accion no permitida');
            });
        }
        $scope.guardar = function(item) {
            $.blockUI({ message: '<img src="img/login/loading.gif">' });
            MetodosIngreso.update({id:$routeParams.id}, $scope.mi).$promise.then(function(response){
                    $scope.cambio = false;
                    $.unblockUI();
                    toastr.success('Cambios guardados');
            });
        };
    });

 mainApp.controller("ranking",function($scope,$http){
        $scope.todo = [];
        $scope.getAll = (params=$scope.params)=>
        {
            $http({
                    url: getUrl('rankingClientes'), 
                    method: "GET",
                    params
                 }).then(function(response){
                $scope.lastPage = response.data.last_page;
                response.data.data.forEach((item)=>$scope.todo.push(item));
            }); 
        }
        $scope.export = (format)=>
        {
            let params= {format};
            $http(
                {
                    url: getUrl('rankingExport'), 
                    method: "GET",
                    params
                 }).then(function(response)
                {
                    downloadFromHttpResponse(response);
                });
        }
        $scope.initParams = ()=>
        {
            $scope.params = {
                page:1
            };    
        }
        $scope.goToClient = (cliente) =>
        {
            location.assign("#!/cliente/edit/"+cliente.id);
        }
        $scope.showMore = ()=>
        {
            $scope.params.page++;
            $scope.getAll($scope.params);
        }
        $scope.initParams();
        $scope.getAll();
    });

   
 mainApp.controller("reporte",function($scope,$http, Usuarios){
        
        $scope.inpSearch = {};
        $scope.getAll = (params = $scope.params)=>
        {
            $http({
                url:getUrl('reporteCanjes'),
                method: "GET",
                params
            })
            .then(function(response){
                $scope.todo = response.data.data;
            });
        }

        $scope.actualizar = function(item) {

            let fromDate = $scope.inpSearch.fromDate, toDate=$scope.inpSearch.toDate;
            $scope.getAll(Object.assign(($scope.params={page:1}),{fromDate,toDate}));
        };
         $scope.export = (format)=>
        {
            let params= {format};
            $http(
                {
                    url: getUrl('reporteExport'), 
                    method: "GET",
                    params
                 }).then(function(response)
                {
                    downloadFromHttpResponse(response);
                });
        }
        $scope.initParams = ()=>
        {
            $scope.params = {
                page:1
            };    
        }

        $scope.showMore = ()=>
        {
            $scope.params.page++;
            $scope.getAll($scope.params);
        }
        $scope.initParams();
        $scope.getAll();
    });
mainApp.directive('cardInput',
 function () 
 {
    return {
        restrict: 'EA',
        transclude:true,
        template:   `<div class="text-center m-t-md animated fadeInRight" ng-show="manualCardInput">
                        <label>Ingrese el ID de tarjeta </label>
                        <div class="form-group">
                            <div>
                               <input ng-model="cardInput" name="tarjeta" class="form-control" ng-enter="searchCallback()"/>
                            </div>
                        </div>
                    </div>
                    <div ng-show="!manualCardInput" class="text-center m-t-md animated fadeInRight">      
                     <div class="text-center">
                        <i class="fa fa-credit-card big-icon"></i>
                        <div class="id-magnetico text-info">
                            <h1>{{valueRead}}</h1>
                        </div>
                    </div>
                            
                        <label class="text-muted">Lector esperando tarjeta</label>
                    </div>`,
        scope: {
            cardInput: '=',
            manualCardInput:'=',
            valueRead:'=',
            searchCallback:'&?'
        }
    };
});
mainApp.directive('categoriaItem',
 function ($ngConfirm,$http) 
 {
    return {
        link:function(scope,element,attr)
        {
            scope.inpCategory = '';
            scope.addParentCategory = false;
            scope.edit = function()
            {
                $http.get(getUrl('articuloCategorias'))
                .then(function(response)
                {
                    scope.categorias = response.data;
                    scope.selected = scope.categorias.find((e)=>e.id == scope.selectedId);
                });
                scope.setSelected = function(category=null)
                {
                    scope.selected = category;
                    scope.selectedId = category.id;
                }
                scope.createChild = function(parent,title)
                {
                    $http.post(getUrl('articuloCategoriasCreate'),{
                        parentId:(parent && parent.id)||null,
                        title
                    })
                    .then(function(response)
                    {   
                        scope.addParentCategory = false;
                        scope.categorias = response.data.categoriaAll;
                        parent && (parent.createChild = false);
                        scope.inpCategory = '';
                    });
                }
                scope.editModal =  $ngConfirm({
                    title: '',
                    offsetTop: 20,
                    content: `
                     <div style="overflow:scroll">
                        <h3>Seleccionar categoría</h3>
                        <button ng-show="addParentCategory==false" 
                         class="btn-block btn btn-default"
                        ng-click="addParentCategory=true">
                            + Agregar categoría
                        </button>
                        <div class="input-group m-b" ng-show="addParentCategory==true"  style="display:inline-flex">
                            <span class="input-group-prepend">
                                <button type="button" ng-click="createChild(null,inpCategory)" 
                                ng-disabled="!inpCategory || inpCategory==''" 
                                class="btn btn-primary" disabled="disabled">+</button> 
                            </span> 
                            <input type="text" placeholder="Nueva categoría" 
                            ng-model="inpCategory" class="form-control 
                            ng-enter="createChild(null,inpCategory)" 
                            ng-pristine ng-valid ng-empty ng-touched">
                        </div>
                        <div>
                        <ul class="directory-list">
                            <li ng-class="{'selected':selected==parent}" class="folder"  
                            ng-repeat="parent in categorias | filter:{parentId:null}">
                                <span ng-click="setSelected(parent)" ng-class="{'invert':selected==parent}" 
                                class="category-title" >{{parent.title}} </span>
                                <a class="btn btn-xs" ng-class="{'invert':selected==parent}" 
                                ng-click="parent.createChild = true">
                                            + <small class="text-muted">Agregar subcategoría</small>
                                            </a>
                                        <div ng-if="parent.createChild == true">
                                            <div class="input-group m-b" style="display:inline-flex;width:100%;">
                                            <span class="input-group-prepend">
                                                <button type="button" ng-click="createChild(parent,inpCategory)" 
                                                ng-disabled="!inpCategory || inpCategory==''" 
                                                class="btn btn-primary" style="border-radius:0;">+</button> </span> 
                                                <input type="text" placeholder="Nueva categoría" 
                                                ng-enter="createChild(parent,inpCategory)" 
                                                ng-model="inpCategory" class="form-control">
                                            </div>
                                        </div>
                                <div ng-if="(categorias | filter:{parentId:parentId}:true).length > 0">
                                    <div ng-include="'view/categoria-nodo'" ng-init="parentId = parent.id"></div>
                                </div>
                            </li>
                        </ul>
                        </div>
                    </div>
                    `,
                    scope,
                    columnClass: 'col-md-6 col-md-offset-3',
                });
            }
        },
        restrict: 'EA',
        transclude:true,
        template:   `<div class="form-group">
                        <label><strong>Categor&iacute;a</strong></label>
                        <a class="pull-right" ng-click="edit()">
                            <i class="fa fa-pencil"></i><small> Editar</small>
                        </a>
                        <p ng-if="!categoriaTree" class="text-muted">Sin categoría</p>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item" ng-repeat="categoria in categoriaTree | orderBy:'-'">
                                <span ng-class="{'text-muted':selectedId!=categoria.id}" href="index.html">{{categoria.title}}</span>
                            </li>
                        </ol>
                    </div>
                    `,
        scope: {
            categoriaTree: '=categoriaItem',
            selectedId:'='
        }
    };
});
mainApp.directive('locationSuggestion',function () 
  {
  return {
    link: function (scope, element, attr) 
    {
      scope.inpString = '';
      scope.modelChange = scope.modelChange();
      scope._modelChange = (inpString)=>
      {
        scope.searching = inpString;
        inpString && inpString!= '' && scope.modelChange(inpString)
        .then(()=>{
          if(scope.suggestions)
          {
            if((scope.choosen = scope.suggestions.find((e)=> 
              e.searchString == scope.inpString)) !='undefined'
              )
            {
              scope.locationModel = scope.choosen.id;
            }
          }
        });
      }
    },
     restrict: 'EA',
      transclude:true,
      template:   `<div class="form-group">
                      <label>Ubicacion</label>
                      <input list="locationSuggestions" ng-model="inpString" 
                      type="text" class="form-control" ng-change="_modelChange(inpString)"  
                      ng-model-options="{ debounce: 300 }">
                      <datalist id="locationSuggestions">
                          <option ng-repeat="location in suggestions" 
                          value="{{location.searchString}}">
                      </datalist>
                  </div>
                  `,
      scope: {
          locationModel: '=',
          suggestions:'=',
          modelChange:'&'
      }
  };
});
mainApp.directive('categoriaManager',
 function ($ngConfirm,$http) 
 {
    return {
        link:function(scope,element,attr)
        {
            scope.inpCategory = '';
            scope.addParentCategory = false;
            scope.manager = true;
            scope.getAll = ()=>
            {
                $http.get(getUrl('articuloCategorias'))
                .then(function(response)
                {
                    scope.categorias = response.data;
                    scope.selected = scope.categorias.find((e)=>e.id == scope.selectedId);
                });
            }
            scope.getAll();
            scope.setSelected = function(category=null)
            {
                scope.selected = category;
                scope.selectedId = category.id;
            }
            scope.createChild = function(parent,title)
            {
                $http.post(getUrl('articuloCategoriasCreate'),{
                    parentId:(parent && parent.id)||null,
                    title
                })
                .then(function(response)
                {   
                    scope.inpCategory = '';
                    scope.addParentCategory = false;
                    scope.categorias = response.data.categoriaAll;
                    parent && (parent.createChild = false);
                    scope.inpCategory = '';
                });
            }
            scope.deleteNode = function(node)
            {
                 $http({
                        params:{
                            id:node.id
                        },
                        url:getUrl('articuloCategoriaDelete'),
                        method:"DELETE"
                    })
                    .then(function(response)
                    {
                        if(response.data.delete)
                        {
                            scope.getAll();
                        }
                    });
            }
        },
        restrict: 'EA',
        transclude:true,
        template:   `<div style="overflow:scroll">
                        <button ng-show="addParentCategory==false" 
                         class="btn-block btn btn-default"
                        ng-click="addParentCategory=true">
                            + Agregar categoría
                        </button>
                        <div class="input-group m-b" ng-show="addParentCategory==true"  style="display:inline-flex;width:100%;">
                            <span class="input-group-prepend">
                                <button type="button" ng-click="createChild(null,inpCategory)" 
                                ng-disabled="!inpCategory || inpCategory==''" 
                                class="btn btn-primary" disabled="disabled" style="border-radius:0;">+</button> 
                            </span> 
                            <input type="text" placeholder="Nueva categoría" 
                            ng-model="inpCategory" class="form-control 
                            ng-enter="createChild(null,inpCategory)"
                            ng-pristine ng-valid ng-empty ng-touched">
                        </div>
                        <div>
                        <ul class="directory-list">
                            <li ng-class="{'selected':selected==parent}" class="folder"  
                            ng-repeat="parent in categorias | filter:{parentId:null}">
                                <span ng-click="setSelected(parent)" ng-class="{'invert':selected==parent}" 
                                class="category-title" >{{parent.title}} </span>
                                <a class="btn btn-xs" class="text-muted" ng-class="{'invert':selected==parent}" 
                                ng-click="parent.createChild = true">
                                         <small class="text-muted">+ Agregar subcategoría</small>
                                        </a>
                                <a class="btn btn-xs" ng-class="{'invert':selected==parent}" 
                                confirmation-needed
                                ng-click="deleteNode(parent)">
                                         <span class="text-warning"><i class="fa fa-times"></i></span>
                                        </a>
                                    <div ng-if="parent.createChild == true">
                                        <div class="input-group m-b" style="display:inline-flex;width:100%;">
                                        <span class="input-group-prepend">
                                            <button type="button" ng-click="createChild(parent,inpCategory)" 
                                            ng-disabled="!inpCategory || inpCategory==''" 
                                            class="btn btn-primary" style="border-radius:0;">+</button> </span> 
                                            <input type="text" placeholder="Nueva categoría" 
                                            ng-model="inpCategory" class="form-control" ng-enter="createChild(null,inpCategory)">
                                        </div>
                                    </div>
                                <div ng-if="(categorias | filter:{parentId:parentId}:true).length > 0">
                                    <div ng-include="'view/categoria-nodo'" ng-init="parentId = parent.id"></div>
                                </div>
                            </li>
                        </ul>
                        </div>
                    </div>
                    `
    };
});
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(1);
(function webpackMissingModule() { throw new Error("Cannot find module \"/Users/nico/ws/fidelizacion/node-modules/materialize-css/sass/materialize.scss\""); }());


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/*!
 * angular-confirm v1.1.0 (http://craftpip.github.io/angular-confirm/)
 * Author: Boniface Pereira
 * Website: www.craftpip.com
 * Contact: hey@craftpip.com
 *
 * Copyright 2016-2017 angular-confirm
 * Licensed under MIT (https://github.com/craftpip/angular-confirm/blob/master/LICENSE)
 */

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

if ("undefined" == typeof jQuery) throw new Error("angular-confirm requires jQuery");if ("undefined" == typeof angular) throw new Error("angular-confirm requires Angular");angular.module("cp.ngConfirm", []).service("$ngConfirmTemplate", function () {
  this.default = '<div class="ng-confirm"><div class="ng-confirm-bg ng-confirm-bg-h"></div><div class="ng-confirm-scrollpane"><div class="ng-bs3-container"><div class="ng-bs3-row"><div class="ng-confirm-box-p"><div class="ng-confirm-box" role="dialog" aria-labelledby="labelled" tabindex="-1"><div class="ng-confirm-closeIcon"></div><div class="ng-confirm-title-c"><span class="ng-confirm-icon-c"><i></i></span><span class="ng-confirm-title"></span></div><div class="ng-confirm-content-pane"><div class="ng-confirm-content"></div></div><div class="ng-confirm-buttons"></div><div class="ng-confirm-clear"></div></div></div></div></div></div></div>';
}).service("$ngConfirmDefaults", function () {
  return { title: "Hello", titleClass: "", type: "default", typeAnimated: !0, content: "Are you sure to continue?", contentUrl: !1, defaultButtons: { ok: function ok() {} }, icon: "", theme: "white", bgOpacity: null, animation: "zoom", closeAnimation: "scale", animationSpeed: 400, animationBounce: 1.2, scope: !1, escapeKey: !1, rtl: !1, buttons: {}, container: "body", containerFluid: !1, backgroundDismiss: !1, backgroundDismissAnimation: "shake", alignMiddle: !0, offsetTop: 50, offsetBottom: 50, autoClose: !1, closeIcon: null, closeIconClass: !1, columnClass: "small", boxWidth: "50%", useBootstrap: !0, bootstrapClasses: { container: "container", containerFluid: "container-fluid", row: "row" }, onScopeReady: function onScopeReady() {}, onReady: function onReady() {}, onOpenBefore: function onOpenBefore() {}, onOpen: function onOpen() {}, onClose: function onClose() {}, onDestroy: function onDestroy() {}, onAction: function onAction() {} };
}).service("$ngConfirm", ["$rootScope", "$ngConfirmDefaults", "$ngConfirmBase", function (a, b, c) {
  return function (a, d, e) {
    return "string" == typeof a && (a = { content: a, buttons: b.defaultButtons }, "string" == typeof d ? a.title = d || !1 : a.title = !1, "object" == (typeof d === "undefined" ? "undefined" : _typeof(d)) && (a.scope = d), "object" == (typeof e === "undefined" ? "undefined" : _typeof(e)) && (a.scope = e)), "undefined" == typeof a && (a = {}), a = angular.extend({}, b, a), new c(a);
  };
}]).factory("$ngConfirmBase", ["$rootScope", "$ngConfirmDefaults", "$timeout", "$compile", "$ngConfirmTemplate", "$interval", "$templateRequest", "$log", "$q", function (a, b, c, d, e, f, g, h, i) {
  var j = function j(a) {
    angular.extend(this, a), this._init();
  };return j.prototype = { _init: function _init() {
      this._lastFocused = angular.element("body").find(":focus"), this._id = Math.round(999999 * Math.random()), this.open();
    }, _providedScope: !1, _prepare: function _prepare() {
      var b = this;this.$el = angular.element(e.default), b.scope ? this._providedScope = !0 : (this._providedScope = !1, b.scope = a.$new()), "function" == typeof this.onScopeReady && this.onScopeReady.apply(this, [this.scope]), this.$confirmBox = this.$el.find(".ng-confirm-box"), this.$confirmBoxParent = this.$el.find(".ng-confirm-box-p"), this.$titleContainer = this.$el.find(".ng-confirm-title-c"), this.$title = this.$el.find(".ng-confirm-title"), this.$icon = this.$el.find(".ng-confirm-icon-c"), this.$content = this.$el.find(".ng-confirm-content"), this.$confirmBg = this.$el.find(".ng-confirm-bg"), this.$contentPane = this.$el.find(".ng-confirm-content-pane"), this.$closeIcon = this.$el.find(".ng-confirm-closeIcon"), this.$bs3Container = this.$el.find(".ng-bs3-container"), this.$buttonContainer = this.$el.find(".ng-confirm-buttons"), this.$scrollPane = this.$el.find(".ng-confirm-scrollpane");var c = "ng-confirm-box" + this._id;if (this.$confirmBox.attr("aria-labelledby", c), this.$content.attr("id", c), this._setAnimationClass(this.animation), this.setDismissAnimation(this.backgroundDismissAnimation), this.setTheme(this.theme), this.setType(this.type), this._setButtons(this.buttons), this.setCloseIcon(this.closeIcon), this.setCloseIconClass(this.closeIconClass), this.setTypeAnimated(this.typeAnimated), this.useBootstrap ? (this.setColumnClass(this.columnClass), this.$el.find(".ng-bs3-row").addClass(this.bootstrapClasses.row).addClass("justify-content-md-center justify-content-sm-center justify-content-xs-center justify-content-lg-center"), this.setContainerFluid(this.containerFluid)) : this.setBoxWidth(this.boxWidth), this.setTitleClass(this.titleClass), this.setTitle(this.title), this.setIcon(this.icon), this.setBgOpacity(this.bgOpacity), this.setRtl(this.rtl), this._contentReady = i.defer(), this._modalReady = i.defer(), i.all([this._contentReady.promise, this._modalReady.promise]).then(function () {
        b.isAjax && (b.setContent(b.content), b.loading(!1)), "function" == typeof b.onReady && b.onReady.apply(b, [b.scope]);
      }), this.contentUrl) {
        this.loading(!0), this.isAjax = !0;var d = this.contentUrl;"function" == typeof this.contentUrl && (d = this.contentUrl()), this.isAjaxLoading = !0, g(d).then(function (a) {
          b.content = a, b._contentReady.resolve(), b.isAjaxLoading = !1;
        }, function () {
          b.content = "", b._contentReady.resolve(), b.isAjaxLoading = !1;
        });
      } else {
        var f = this.content;"function" == typeof this.content && (f = this.content()), this.content = f, this.setContent(this.content), this._contentReady.resolve();
      }"none" == this.animation && (this.animationSpeed = 1, this.animationBounce = 1), this.$confirmBg.css(this._getCSS(this.animationSpeed, 1));
    }, isAjax: !1, isAjaxLoading: !1, isLoading: !1, _hideClass: "ng-confirm-el-hide", _loadingClass: "ng-confirm-loading", loading: function loading(a) {
      this.isLoading = a, a ? this.$confirmBox.addClass(this._loadingClass) : this.$confirmBox.removeClass(this._loadingClass);
    }, setContent: function setContent(a) {
      if (!this.$content) return void h.error("Attempted to set content before $content is defined");a = "<div>" + a + "</div>";var b = d(a)(this.scope);this.$content.append(b);
    }, _typeList: ["default", "blue", "green", "red", "orange", "purple", "dark"], _typePrefix: "ng-confirm-type-", _pSetType: "", setType: function setType(a) {
      if (this._typeList.indexOf(a.toLowerCase()) == -1) return h.warn("Invalid dialog type: " + a), !1;var b = this._typePrefix + a;this.$el.removeClass(this._pSetType).addClass(b), this._pSetType = b;
    }, _setTypeAnimatedClass: "ng-confirm-type-animated", setTypeAnimated: function setTypeAnimated(a) {
      a ? this.$confirmBox.addClass(this._setTypeAnimatedClass) : this.$confirmBox.removeClass(this._setTypeAnimatedClass);
    }, _pTitleClass: "", setTitleClass: function setTitleClass(a) {
      this.$titleContainer.removeClass(this._pTitleClass).addClass(a), this._pTitleClass = a;
    }, setBoxWidth: function setBoxWidth(a) {
      return this.useBootstrap ? void h.warn("Cannot set boxWidth as useBootstrap is set to true. use columnClass instead.") : void this.$confirmBox.css("width", a);
    }, setContainerFluid: function setContainerFluid(a) {
      return this.useBootstrap ? (a ? this.$bs3Container.removeClass(this.bootstrapClasses.container).addClass(this.bootstrapClasses.containerFluid) : this.$bs3Container.removeClass(this.bootstrapClasses.containerFluid).addClass(this.bootstrapClasses.container), void (this.containerFluid = a)) : void h.warn("Cannot set containerFluid as useBootstrap is set to false.");
    }, _pSetColumnClass: "", setColumnClass: function setColumnClass(a) {
      if (!this.useBootstrap) return void h.warn("Cannot set columnClass as useBootstrap is set to false, use bixWidth instead");a = a.toLowerCase();var b;switch (a) {case "xl":case "xlarge":
          b = "col-md-12";break;case "l":case "large":
          b = "col-md-8 col-md-offset-2";break;case "m":case "medium":
          b = "col-md-6 col-md-offset-3";break;case "s":case "small":
          b = "col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1";break;case "xs":case "xsmall":
          b = "col-md-2 col-md-offset-5";break;default:
          b = a;}this.$confirmBoxParent.removeClass(this._pSetColumnClass).addClass(b), this._pSetColumnClass = b;
    }, setIcon: function setIcon(a) {
      "function" == typeof a && (a = a()), a ? (this.$icon.html(angular.element("<i></i>").addClass(a)), this.$icon.removeClass(this._hideClass)) : this.$icon.addClass(this._hideClass), this.icon || this.title ? this.$titleContainer.removeClass(this._hideClass) : this.$titleContainer.addClass(this._hideClass), this.icon = a;
    }, setTitle: function setTitle(a) {
      "function" == typeof a && (a = a()), a ? (this.$title.html(a), this.$title.removeClass(this._hideClass)) : this.$title.addClass(this._hideClass), this.icon || this.title ? this.$titleContainer.removeClass(this._hideClass) : this.$titleContainer.addClass(this._hideClass), this.title = a;
    }, setCloseIcon: function setCloseIcon(a) {
      0 == this._buttonCount && null == a && (a = !0), a ? this.$closeIcon.removeClass(this._hideClass) : this.$closeIcon.addClass(this._hideClass), this.closeIcon = a;
    }, setCloseIconClass: function setCloseIconClass(a) {
      var b;b = a ? angular.element("<i></i>").addClass(this.closeIconClass) : angular.element("<span>&times;</span>").addClass(this.closeIconClass), this.$closeIcon.html(b), this.closeIconClass = a;
    }, _animationPrefix: "ng-confirm-animation-", _pSetAnimation: "", _setAnimationClass: function _setAnimationClass(a) {
      var b = this._prefixThis(a, this._animationPrefix);this.$confirmBox.removeClass(this._pSetAnimation).addClass(b), this._pSetAnimation = b;
    }, _removeAnimationClass: function _removeAnimationClass() {
      this.$confirmBox.removeClass(this._pSetAnimation), this._pSetAnimation = "";
    }, _bgDismissPrefix: "ng-confirm-hilight-", _pSetDismissAnimation: "", setDismissAnimation: function setDismissAnimation(a) {
      var b = this._prefixThis(a, this._bgDismissPrefix);this.$confirmBox.removeClass(this._pSetDismissAnimation).addClass(b), this._pSetDismissAnimation = b;
    }, _prefixThis: function _prefixThis(a, b) {
      return a = a.split(","), angular.forEach(a, function (c, d) {
        c.indexOf(b) == -1 && (a[d] = b + c.trim());
      }), a.join(" ").toLowerCase();
    }, _setButtons: function _setButtons(a) {
      var b = this;"object" != (typeof a === "undefined" ? "undefined" : _typeof(a)) && (a = {}), angular.forEach(a, function (c, d) {
        "function" == typeof c && (a[d] = c = { action: c }), a[d].text = c.text || d, a[d].btnClass = c.btnClass || "btn-default", a[d].action = c.action || angular.noop, a[d].keys = c.keys || [], a[d].disabled = c.disabled || !1, "undefined" == typeof c.show && (c.show = !0), a[d].show = c.show, angular.forEach(a[d].keys, function (b, c) {
          a[d].keys[c] = b.toLowerCase();
        });var e = angular.element('<button type="button" class="btn"><span class="ng-confirm-btn-text"></span></button>');a[d].setText = function (a) {
          e.find(".ng-confirm-btn-text").html(a);
        }, a[d].setBtnClass = function (b) {
          e.removeClass(a[d].btnClass).addClass(b), a[d].btnClass = b;
        }, a[d].setDisabled = function (b) {
          b ? e.attr("disabled", "disabled") : e.removeAttr("disabled"), a[d].disabled = b;
        }, a[d].setShow = function (c) {
          c ? e.removeClass(b._hideClass) : e.addClass(b._hideClass), a[d].show = c;
        }, a[d].setText(a[d].text), a[d].setBtnClass(a[d].btnClass), a[d].setDisabled(a[d].disabled), a[d].setShow(a[d].show), e.click(function (a) {
          a.preventDefault(), b.triggerButton(d);
        }), a[d].el = e, b.$buttonContainer.append(e);
      }), this.buttons = a, this._buttonCount = Object.keys(a).length;
    }, _buttonCount: 0, _themePrefix: "ng-confirm-", _pSetTheme: "", setTheme: function setTheme(a) {
      var b = this._prefixThis(a, this._themePrefix);this.$el.removeClass(this._pSetTheme).addClass(b), this._pSetTheme = b;
    }, _rtlClass: "ng-confirm-rtl", setRtl: function setRtl(a) {
      a ? this.$el.addClass(this._rtlClass) : this.$el.removeClass(this._rtlClass), this.rtl = a;
    }, setBgOpacity: function setBgOpacity(a) {
      this.$confirmBg.css("opacity", a), this.bgOpacity = a;
    }, _cubic_bezier: "0.36, 0.55, 0.19", _getCSS: function _getCSS(a, b) {
      return { "-webkit-transition-duration": a / 1e3 + "s", "transition-duration": a / 1e3 + "s", "-webkit-transition-timing-function": "cubic-bezier(" + this._cubic_bezier + ", " + b + ")", "transition-timing-function": "cubic-bezier(" + this._cubic_bezier + ", " + b + ")" };
    }, _hash: function _hash(a) {
      var b = a.toString(),
          c = 0;if (0 == b.length) return c;for (var d = 0; d < b.length; d++) {
        var e = b.toString().charCodeAt(d);c = (c << 5) - c + e, c &= c;
      }return c;
    }, _digestWatchUnRegister: !1, _bindEvents: function _bindEvents() {
      var a = this;this._digestWatchUnRegister = this.scope.$watch(function () {
        a.setDialogCenter("Digest watcher");
      }), this.$closeIcon.on("click." + a._id, function () {
        a._closeClick();
      }), angular.element(window).on("resize." + a._id, function () {
        a.setDialogCenter("Window Resize");
      }), angular.element(window).on("keyup." + a._id, function (b) {
        a._reactOnKey(b);
      }), this.$scrollPane.on("click", function () {
        a._scrollPaneClick();
      }), this.$confirmBox.on("click", function () {
        a.boxClicked = !0;
      });
    }, _unBindEvents: function _unBindEvents() {
      angular.element(window).off("resize." + this._id), angular.element(window).off("keyup." + this._id), this.$closeIcon.off("click." + this._id), this._digestWatchUnRegister && this._digestWatchUnRegister();
    }, _reactOnKey: function _reactOnKey(a) {
      var b = this,
          c = angular.element(".ng-confirm");if (c.eq(c.length - 1)[0] !== this.$el[0]) return !1;var d = a.which;if (!$(this.$el).find(":input").is(":focus") || !/13|32/.test(d)) {
        var e = this._getKey(d);if ("esc" === e && this.escapeKey) if (1 == this.escapeKey) this._scrollPaneClick();else if ("string" == typeof this.escapeKey || "function" == typeof this.escapeKey) {
          var f = !1;f = "function" == typeof this.escapeKey ? this.escapeKey() : this.escapeKey, f && (angular.isDefined(this.buttons[f]) ? this._buttonClick(f) : h.warn("Invalid escapeKey, no buttons found with name " + f));
        }angular.forEach(this.buttons, function (a, c) {
          a.keys.indexOf(e) != -1 && b._buttonClick(c);
        });
      }
    }, _scrollPaneClick: function _scrollPaneClick() {
      if (this.boxClicked) return this.boxClicked = !1, !1;var c,
          a = !1,
          b = !1;if (c = "function" == typeof this.backgroundDismiss ? this.backgroundDismiss() : this.backgroundDismiss, "string" == typeof c && angular.isDefined(this.buttons[c]) ? (a = c, b = !1) : b = "undefined" == typeof c || 1 == !!c, a) {
        var d = this.buttons[a].action.apply(this, [this.scope, this.buttons[a]]);b = "undefined" == typeof d || !!d;
      }b ? this.close() : this.hiLightModal();
    }, _closeClick: function _closeClick() {
      var c,
          a = !1,
          b = !1;if (c = "function" == typeof this.closeIcon ? this.closeIcon() : this.closeIcon, "string" == typeof c && angular.isDefined(this.buttons[c]) ? (a = c, b = !1) : b = "undefined" == typeof c || 1 == !!c, a) {
        var d = this.buttons[a].action.apply(this, [this.scope, this.buttons[a]]);b = "undefined" == typeof d || 1 == !!d;
      }b && this.close();
    }, _hilightAnimating: !1, _hilightClass: "ng-confirm-hilight", hiLightModal: function hiLightModal() {
      var a = this;this._hilightAnimating || (this._hilightAnimating = !0, this.$confirmBox.addClass(this._hilightClass), setTimeout(function () {
        a._hilightAnimating = !1, a.$confirmBox.removeClass(a._hilightClass);
      }, this.animationSpeed));
    }, _buttonClick: function _buttonClick(a) {
      var b = this.buttons[a].action.apply(this, [this.scope, this.buttons[a]]);return "function" == typeof this.onAction && this.onAction.apply(this, [this.scope, a]), "undefined" == typeof b || b ? this.close() : this.scope.$apply(), b;
    }, triggerButton: function triggerButton(a) {
      return this._buttonClick(a);
    }, setDialogCenter: function setDialogCenter(a) {
      a = a || "n/a";var b = this.$content,
          c = b.outerHeight(),
          d = this.$contentPane.outerHeight(),
          e = b.children();if (0 != e.length) {
        var f = parseInt(e.eq(0).css("margin-top"));f && (c += f);
      }var g = angular.element(window).height(),
          h = this.$confirmBox.outerHeight();if (0 != h) {
        var k,
            i = h - d + c,
            j = this.offsetTop + this.offsetBottom;i > g - j || !this.alignMiddle ? (k = { "margin-top": this.offsetTop, "margin-bottom": this.offsetBottom }, angular.element("body").addClass("ng-confirm-no-scroll-" + this._id)) : (k = { "margin-top": (g - i) / 2, "margin-bottom": 0 }, angular.element("body").removeClass("ng-confirm-no-scroll-" + this._id)), this.$contentPane.css({ height: c }).scrollTop(0), this.$confirmBox.css(k);
      }
    }, _getKey: function _getKey(a) {
      switch (a) {case 192:
          return "tilde";case 13:
          return "enter";case 16:
          return "shift";case 9:
          return "tab";case 20:
          return "capslock";case 17:
          return "ctrl";case 91:
          return "win";case 18:
          return "alt";case 27:
          return "esc";}var b = String.fromCharCode(a);return !!/^[A-z0-9]+$/.test(b) && b.toLowerCase();
    }, open: function open() {
      var a = this;return this._prepare(), c(function () {
        a._open();
      }, 100), !0;
    }, _open: function _open() {
      var a = this;"function" == typeof this.onOpenBefore && this.onOpenBefore.apply(this, [this.scope]), angular.element(this.container).append(this.$el), a.setDialogCenter("_open"), setTimeout(function () {
        a.$contentPane.css(a._getCSS(a.animationSpeed, 1)), a.$confirmBox.css(a._getCSS(a.animationSpeed, a.animationBounce)), a._removeAnimationClass(), a.$confirmBg.removeClass("ng-confirm-bg-h"), a.$confirmBox.focus(), setTimeout(function () {
          a._bindEvents(), a.$confirmBox.css(a._getCSS(a.animationSpeed, 1)), a._modalReady.resolve(), "function" == typeof a.onOpen && a.onOpen.apply(a, [a.scope]), a._startCountDown();
        }, a.animationSpeed);
      }, 100);
    }, _autoCloseKey: !1, _autoCloseInterval: 0, _startCountDown: function _startCountDown() {
      var a = this;if ("string" == typeof this.autoClose) {
        var b = this.autoClose.split("|");if (2 != b.length) return void h.error("Invalid option for autoClose. example 'close|10000'");this._autoCloseKey = b[0];var c = b[1],
            d = c / 1e3;if (!angular.isDefined(this.buttons[this._autoCloseKey])) return void h.error('Auto close button "' + a._autoCloseKey + '" not defined.');var e = angular.element('<span class="ng-confirm-timer"></span>');this.buttons[this._autoCloseKey].el.append(e), this._autoCloseInterval = setInterval(function () {
          var b = d ? " (" + --d + ")" : "";e.html(b), d < 1 && (a._stopCountDown(), a._buttonClick(a._autoCloseKey));
        }, 1e3);
      }
    }, _stopCountDown: function _stopCountDown() {
      this._autoCloseInterval && clearInterval(this._autoCloseInterval);
    }, closed: !1, isClosed: function isClosed() {
      return this.closed;
    }, isOpen: function isOpen() {
      return !this.closed;
    }, close: function close() {
      var a = this;"function" == typeof this.onClose && this.onClose.apply(this, [this.scope]), this._unBindEvents(), this._stopCountDown(), this._setAnimationClass(this.closeAnimation), this.$confirmBg.addClass("ng-confirm-bg-h");var b = .4 * this.animationSpeed;return setTimeout(function () {
        a.$el.remove(), a.closed = !0, a._providedScope || a.scope.$destroy(), "function" == typeof a.onDestroy && a.onDestroy.apply(a, [a.scope]), angular.element("body").removeClass("ng-confirm-no-scroll-" + a._id), a._lastFocused.focus(), a = void 0;
      }, b), !0;
    } }, j;
}]);

/***/ })
/******/ ]);