mainApp.controller("config",function($scope,$http, Clientes){
        $http.get(getUrl('config')).then(function(response){
            response = response.data;
            $scope.config = response;
        });
        $scope.onReady = function(item) {
            $scope.cambio = true;
        };
        $scope.uploadFile = function(files) {
            var fd = new FormData();
            fd.append("img", files[0]);
            $http.post(getUrl('setLogo'), fd, {
                withCredentials: true,
                headers: {'Content-Type': undefined },
                transformRequest: angular.identity
            }).then(function(response){
                response = response.data;
                if(response.img)
                {
                    toastr.success('Logo cambiado');
                    changeImg(files[0],$('#logo'));
                    changeImg(files[0],$('#navbar-logo'));
                }
                },
                function(response)
                {
                        toastr.error('Error en cambio de logo');
                })
        };
        $scope.guardar = function()
        {
            $http.put(getUrl('config'),$scope.config)
            .then(function(response){
                response = response.data;
                if(response.cambio_config)
                {
                    toastr.success('Cambios guardados');
                }
            });
        }
    });
