var controladores = {};
    controladores.sesion = function(
        $scope,
        $http,
        MetodosIngreso,
        Usuarios,
        $rootScope,
        swipeService,
        session,
        $ngConfirm
        )
    {  
        $scope.setInputMode = (method)=>
        {
            isManual = method=='0';
            if(!isManual)
            {
                $scope.getMis();
            }
            $rootScope.manualCardInput = isManual;
        }
        $scope.$watch(function() {
            return session.get('inputMethodId');
        }, function(methodId) {
            $scope.setInputMode(methodId);
            $scope.inputMethodId = methodId;

            if(!$scope.manualCardInput)
            {
                $scope.inputMethod = swipeService.getInputMethodId();
                swipeService.enable();
            }
            else
            {
                swipeService.disable();
            }
        });
      
        $scope.setInputMethod = (method={id:0,nombre:'manual'})=>
        {
            $http
            .post(getUrl('miByAuth'),{id:method.id})
            .then(function(response){
                    response = response.data;
                    if(response.inputMethodSetted)
                    {
                        swipeService.setInputMethod(method);
                    }
                }
            );
        }

        $scope.getMis = function(item) 
        {
            $http.get(getUrl("mi")).then(function(response){
                response = response.data;
                $scope.mitodos = response.todo;
            });
        };

        $scope.logout = function()
        {
            $http.post(getUrl('logout'),{})
            .then(function(response){
                session.logOut();
                location.assign('login');
            },
            function(response)
            {
                toastr.error("Er")
            });
        }
        $scope.showModalTermsConditions = ()=>
        {

            $scope.modalTermsAndConditions = 
            $ngConfirm({
                    title: '',
                    escapeKey: true,
                    content: `
                    <div >
                    <h3>Términos y condiciones</h3>
                    <div style="    overflow: scroll;
                    max-height: 400px;">
                        <p>
                            `+session.get('termsAndConditions')+`
                        </p>
                    </div>
                    <div class="row" style="margin-top:15px;padding-bottom:15px;">
                        <div class="text-center">
                            <button ng-click="acceptTermsConditions()" 
                            class="btn btn-info buttonMinimum">
                                Aceptar
                            </button>
                            <button ng-click="closeModalTermsConditions()" class="btn 
                            btn-default m-r-3 buttonMinimum">
                                Cerrar
                            </button>
                            
                        </div>
                    </div>
                    </div>
                    `,
                    scope: $scope,
                    columnClass: 'col-md-6 col-md-offset-3',
      
                });
        }
        $scope.closeModalTermsConditions = ()=>
        {
            $scope.modalTermsAndConditions.close();
        }
        $scope.acceptTermsConditions = ()=>
        {
            $scope.closeModalTermsConditions();
            $http.post(getUrl('acceptTermsConditions'))
            .then((response)=>
            {
                if(response.data.save)
                {
                    toastr.success('Gracias');
                    session.delete('acceptedTermsConditions');
                }
            })
        }

        $scope.usuarioFullName = session.get('nombre') + ' ' +  session.get('apellido');
        $scope.comercioName = session.get('usrComercio') || 'Configuración';

        $scope.tyc = session.get('tyc');

        if(session.get("acceptedTermsConditions")==="0")
        {
            toastr.remove();
            toastr.warning('Leer terminos y condiciones del servicio', 'Importante',{
                    timeOut: 0,
                    onclick: 
                    function() {
                        $scope.showModalTermsConditions();
                    }
                }
            );
        }
        if(session.get("tipoId") == '1')
        {
            $scope.admin = true;
        }else
        {
            $('.admin').hide();
        }
    }
    controladores.dash = function($scope,$http)
    {
        $scope.chartsData = {};
        $scope.chartsData.buysAvgs = {};
        $scope.chartsData.swapsAvgs = {};

        $scope.chartsData.swapsAvgs.events=
        $scope.chartsData.buysAvgs.events= {
            draw:(data)=>
            {
                if(data.type === 'bar') 
                {
                    data.group.append(new Chartist.Svg('circle', {
                      cx: data.x2,
                      cy: data.y2,
                      r: 8
                    }, 'ct-slice-pie'));
                }
            }
        }

        $scope.setChartsData=(dash)=>
        {
            $buysAvgsData = [
                [
                dash.compra.usr.siempre.AVG,
                dash.compra.usr.lastMonth.AVG,
                ],
                [
                dash.compra.global.siempre.AVG,
                dash.compra.global.lastMonth.AVG,
                ]
            ];

            $swapsAvgsData = [
                [
                    dash.canje.usr.siempre.AVG,
                    dash.canje.usr.lastMonth.AVG
                ],
                [
                    dash.canje.global.siempre.AVG,
                    dash.canje.global.lastMonth.AVG,
                ]    
            ];

            $scope.chartsData.buysAvgs.data = {
              labels: ['USR/GLOBAL Siempre', 'USR/GLOBAL Mes'],
              series: $buysAvgsData,
              
            }, {
              high: 10,
              low: -10,
              axisX: {
                labelInterpolationFnc: function(value, index) {
                  return index % 2 === 0 ? value : null;
                }
              }
            }
            $scope.chartsData.swapsAvgs.data = {
              labels: ['USR/GLOBAL Siempre', 'USR/GLOBAL Mes'],
              series: $swapsAvgsData
            };
            $scope.chartsData.userGlobal = {};

            $usrCanjesPorciento = (dash.canje.usr.siempre.COUNT/dash.canje.global.siempre.COUNT)*100;
            $scope.chartsData.userGlobal.data={
                labels:['Global','Usuario'],
                series:[100-$usrCanjesPorciento,$usrCanjesPorciento]
            }
            $scope.chartsData.monthCompare = {};

            $scope.chartsData.monthCompare.data = {
                series:[
                            {
                                name:'comparativa',
                                data:[
                                dash.compra.comparativa.haceDosMeses,
                                dash.compra.comparativa.haceUnMes,
                                dash.compra.comparativa.esteMes,
                                ]
                            }
                        ],
                labels:['Dos meses atrás','Un mes atrás','Este mes']
            }
            $scope.chartsData.monthCompare.option = {
                series: {
                        'comparativa': {
                          lineSmooth: Chartist.Interpolation.simple(),
                          showArea:true
                        }
                      }
            }
        }

        $.blockUI({ message: '<img src="img/login/loading.gif">' });
        $http.get(getUrl('dash')).then(function(response){
            $scope.dash = response.data;
            $scope.setChartsData($scope.dash);
            $.unblockUI();
        });
        
    }
    mainApp.controller(controladores);