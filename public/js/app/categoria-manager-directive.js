mainApp.directive('categoriaManager',
 function ($ngConfirm,$http) 
 {
    return {
        link:function(scope,element,attr)
        {
            scope.inpCategory = '';
            scope.addParentCategory = false;
            scope.manager = true;
            scope.getAll = ()=>
            {
                $http.get(getUrl('articuloCategorias'))
                .then(function(response)
                {
                    scope.categorias = response.data;
                    scope.selected = scope.categorias.find((e)=>e.id == scope.selectedId);
                });
            }
            scope.getAll();
            scope.setSelected = function(category=null)
            {
                scope.selected = category;
                scope.selectedId = category.id;
            }
            scope.createChild = function(parent,title)
            {
                $http.post(getUrl('articuloCategoriasCreate'),{
                    parentId:(parent && parent.id)||null,
                    title
                })
                .then(function(response)
                {   
                    scope.inpCategory = '';
                    scope.addParentCategory = false;
                    scope.categorias = response.data.categoriaAll;
                    parent && (parent.createChild = false);
                    scope.inpCategory = '';
                });
            }
            scope.deleteNode = function(node)
            {
                 $http({
                        params:{
                            id:node.id
                        },
                        url:getUrl('articuloCategoriaDelete'),
                        method:"DELETE"
                    })
                    .then(function(response)
                    {
                        if(response.data.delete)
                        {
                            scope.getAll();
                        }
                    });
            }
        },
        restrict: 'EA',
        transclude:true,
        template:   `<div style="overflow:scroll">
                        <button ng-show="addParentCategory==false" 
                         class="btn-block btn btn-default"
                        ng-click="addParentCategory=true">
                            + Agregar categoría
                        </button>
                        <div class="input-group m-b" ng-show="addParentCategory==true"  style="display:inline-flex;width:100%;">
                            <span class="input-group-prepend">
                                <button type="button" ng-click="createChild(null,inpCategory)" 
                                ng-disabled="!inpCategory || inpCategory==''" 
                                class="btn btn-primary" disabled="disabled" style="border-radius:0;">+</button> 
                            </span> 
                            <input type="text" placeholder="Nueva categoría" 
                            ng-model="inpCategory" class="form-control 
                            ng-enter="createChild(null,inpCategory)"
                            ng-pristine ng-valid ng-empty ng-touched">
                        </div>
                        <div>
                        <ul class="directory-list">
                            <li ng-class="{'selected':selected==parent}" class="folder"  
                            ng-repeat="parent in categorias | filter:{parentId:null}">
                                <span ng-click="setSelected(parent)" ng-class="{'invert':selected==parent}" 
                                class="category-title" >{{parent.title}} </span>
                                <a class="btn btn-xs" class="text-muted" ng-class="{'invert':selected==parent}" 
                                ng-click="parent.createChild = true">
                                         <small class="text-muted">+ Agregar subcategoría</small>
                                        </a>
                                <a class="btn btn-xs" ng-class="{'invert':selected==parent}" 
                                confirmation-needed
                                ng-click="deleteNode(parent)">
                                         <span class="text-warning"><i class="fa fa-times"></i></span>
                                        </a>
                                    <div ng-if="parent.createChild == true">
                                        <div class="input-group m-b" style="display:inline-flex;width:100%;">
                                        <span class="input-group-prepend">
                                            <button type="button" ng-click="createChild(parent,inpCategory)" 
                                            ng-disabled="!inpCategory || inpCategory==''" 
                                            class="btn btn-primary" style="border-radius:0;">+</button> </span> 
                                            <input type="text" placeholder="Nueva categoría" 
                                            ng-model="inpCategory" class="form-control" ng-enter="createChild(null,inpCategory)">
                                        </div>
                                    </div>
                                <div ng-if="(categorias | filter:{parentId:parentId}:true).length > 0">
                                    <div ng-include="'view/categoria-nodo'" ng-init="parentId = parent.id"></div>
                                </div>
                            </li>
                        </ul>
                        </div>
                    </div>
                    `
    };
});