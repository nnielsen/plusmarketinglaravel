 mainApp.controller("clienteCompras",function($scope,$http,$routeParams,Clientes, Compras)
 {
        $scope.cliente = Clientes.get({id:$routeParams.id});

        $scope.editItem = function(index)
        {
            $scope.editingItem = $scope.compras[index];
        }
        $http.get('compras/all/'+$routeParams.id)
        .then(function(response)
            {
                $scope.compras = response.data.todo;
            });
        $scope.update = function(compra)
        {
            Compras.update(compra).$promise.then(function(response)
                {
                    toastr.success("Compra actualizada");
                });
        }
        $scope.elim = function(compra) {
            $scope.eliminandoCompra =true;
            Compras.remove({id:compra.id}).$promise.then(function(response){
                if(response.delete) {
                    toastr.info("Eliminada");
                    window.location.href = "#!/cliente/edit/"+$routeParams.id;
                    $scope.eliminandoCompra = false;
                }
            },function(rejection){$scope.eliminandoCompra=false;});
        };
    });
    mainApp.controller("clientes",function($scope,$http, Clientes){
        $scope.inpSearch = '';
        $scope.initParams = (object={})=>
        {
            return $scope.params = Object.assign({
                page:1
            },object);    
        }
        $scope.showMore = ()=>
        {
            $scope.params.page++;
            $scope.getAll();
        }
        $scope.search = (queryString)=>
        {
            $scope.searchString = queryString;
            $scope.params = Object.assign({queryString},{page:1});
            $scope.getAll($scope.params);
        }

        $scope.getAll = (params=$scope.params,append=true)=>
        {
            typeof $scope.todo == 'undefined' && ($scope.todo=[]);
            $http(
                {
                    url: getUrl('clientesAll'), 
                    method: "GET",
                    params
                 }).then(function(response)
                {
                    $scope.lastPage = response.data.last_page;
                    if(append)
                    {
                        response.data.data.forEach((cliente)=>{
                            !$scope.todo.find((e)=>e.id == cliente.id) &&
                            $scope.todo.push(cliente);
                        });    
                    }else {
                        $scope.todo = response.data.data;
                    }
                    
                    $scope.modif = response.modif;
                });
        }
        $scope.initParams();
        $scope.getAll();
        $scope.clearSearch = ()=>
        {
            $scope.inpSearch = $scope.searchString = '';
            delete $scope.params.queryString;
            $scope.getAll($scope.initParams(),false);
        }
         $scope.delete = function(cliente,index) {
            Clientes.remove({id:cliente.id}).$promise.then(function(response){
                if(response.delete) {
                    toastr.info("Eliminado");
                    $scope.todo.splice(index,1);
                }
            });
        };
        $scope.export = (format)=>
        {
            let params= {format};
            $http(
                {
                    url: getUrl('clientesExport'), 
                    method: "GET",
                    params
                 }).then(function(response)
                {
                    downloadFromHttpResponse(response);
                });
        }
    });
    mainApp.controller("clienteEdit",function($scope,$http,$routeParams, Clientes,$rootScope){
        $scope.cliente = Clientes.get({id:$routeParams.id});
        $scope.changeUbication = false;
        $scope.onReady = function(item) {
            $scope.cambio = true;
        };

        if(localStorage.getItem("tipo") != '0')
        {
            $('.admin').hide();
        }
        $scope.setIdMagnetico = function(data)
        {
            $scope.cliente.idmagtarjeta = data.idMagnetico;
            $scope.$apply();
        }
        $scope.newOperation = ()=>
        {
            location.assign('#!/operacion/'+$routeParams.id);
        }
        $scope.rootscopeAltaReadSuccess = $rootScope.$on('card-read-success',(event,data)=>
        {
            $scope.setIdMagnetico({idMagnetico:data});
        });
        $scope.guardar = function(item) {
            $.blockUI({ message: '<img src="img/login/loading.gif">' });
            Clientes.update({id:$routeParams.id}, $scope.cliente).$promise.then(function(response){
                    $scope.cambio = false;
                    $.unblockUI();
                    if(response.update)
                    {
                        $scope.changeUbication = false;
                        $scope.cliente = response.update;
                        toastr.success('Cambios guardados');    
                    }
                    if(response.msj)
                    {
                        toastr.error(response.msj);
                    }
                
            },function(rejection)
            {
                 $.unblockUI();
            });
        };
        $scope.suggestLocation = (inpString)=>
        {
            return $http({
                url:getUrl('suggestLocation'),
                params:{
                    queryString:inpString
                },
                method:"GET"
            })
            .then((response)=>
            {
                $scope.locationSuggestions = response.data;
            });
        }
        $scope.delete = function(cliente=$scope.cliente) {
            Clientes.remove({id:cliente.id}).$promise.then(function(response){
                if(response.delete) {
                    toastr.info("Eliminado");
                    location.assign("#!/clientes");
                }
            });
        };
        $scope.goToTransactions = ()=>
        {
            location.assign('#!/cliente/edit/'+$routeParams.id+'/transacciones');
        }
        $http({
                url:getUrl('clienteTransacciones'),
                params:{
                    clienteId:$routeParams.id
                },
                method:"GET"
        })
        .then(function(response){
            response = response.data.todo;
            $scope.actividades = response;
        });

    });
    mainApp.controller("clienteAlta", function($scope,$http,$routeParams, Clientes,$rootScope,$ngConfirm){
       $scope.cliente = {}, $scope.saving = false;
        $scope.alta = function(item) {
            $scope.saving = true;
            Clientes.save({},
                Object.assign($scope.cliente,{
                    locacion:2
                }))
            .$promise.then(function(response){
                $scope.saving = false;
                if(response.alta) {
                    window.location.href = "#!/clientes";
                }
                if(response.msj){
                    toastr.error(response.msj);
                }
            },function()
            {
                $scope.saving = false;
            });
        };
        $scope.suggestLocation = (inpString)=>
        {
            return $http({
                url:getUrl('suggestLocation'),
                params:{
                    queryString:inpString
                },
                method:"GET"
            })
            .then((response)=>
            {
                $scope.locationSuggestions = response.data;
            });
        }
        $scope.setIdMagnetico = function(data)
        {
            $scope.cliente.idmagtarjeta = data.idMagnetico;
            $scope.$apply();
        }
        $scope.rootscopeAltaReadSuccess =
        $rootScope.$on('card-read-success',(event,data)=>
        {
            $scope.setIdMagnetico({idMagnetico:data});
        });
        $scope.$on("$destroy",function()
        {
            $scope.rootscopeAltaReadSuccess();
        });

    });