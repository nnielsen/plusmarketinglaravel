mainApp.config(
function($routeProvider){
    $routeProvider
    .when('/main',
    {
        controller: 'sesion',
        templateUrl: 'view/dash'
    })
    .when('/clientes',
    {
        controller: 'clientes',
        templateUrl: 'view/clientes'
    })
    .when('/config',
    {
        controller: 'config',
        templateUrl: 'view/config'
    })
    .when('/articulos',
    {
        controller: 'articulos',
        templateUrl: 'view/articulos'
    })
    .when('/articulo/alta',
    {
        controller: 'articuloAlta',
        templateUrl: 'view/articulo/alta'
    })
    .when('/articulo/edit/:id',
    {
        controller: 'articuloEdit',
        templateUrl: 'view/articulo/edit'
    })
    .when('/mi',
    {
        controller: 'mi',
        templateUrl: 'view/mis'
    })
    .when('/mi/alta',
    {
        controller: 'miAlta',
        templateUrl: 'view/mi/alta'
    })
    .when('/mi/edit/:id',
    {
        controller: 'miEdit',
        templateUrl: 'view/mi/edit'
    })
    .when('/cliente/edit/:id',
    {
        controller: 'clienteEdit',
        templateUrl: 'view/cliente/edit'
    })
    .when('/cliente/edit/:id/transacciones',
    {
        controller: 'clienteTransacciones',
        templateUrl: 'view/cliente/transacciones'
    })
    .when('/cliente/alta',
    {
        controller: 'clienteAlta',
        templateUrl: 'view/cliente/alta'
    })
    .when('/usuarios',
    {
        controller: 'usuarios',
        templateUrl: 'view/usuarios'
    })
    .when('/usuario/edit/:id',
    {
        controller: 'usuarioEdit',
        templateUrl: 'view/usuario/edit'
    })
    .when('/usuario/alta',
    {
        controller: 'usuarioAlta',
        templateUrl: 'view/usuario/alta'
    })
    .when('/cambiarpass/:id',
    {
        controller: 'cambiarpass',
        templateUrl: 'view/cambiarPassword'
    })
    .when('/operacion',
    {
        controller: 'operacionController',
        templateUrl: 'view/operacion'
    })
    .when('/operacion/:clienteId',
    {
        controller: 'operacionController',
        templateUrl: 'view/operacion'
    })
    .when('/reporte',
    {
        controller: 'reporte',
        templateUrl: 'view/reporte'
    })
    .when('/ranking',
    {
        controller: 'ranking',
        templateUrl: 'view/ranking'
    })
    .when('/contacto',
    {
        controller: 'sesion',
        templateUrl: 'view/contactoSoporte'
    })
    .when('/sucursales',
    {
        controller: 'sucursalTableController',
        templateUrl: 'view/sucursales'
    })
    .when('/sucursal/alta',
    {
        controller: 'sucursalAltaController',
        templateUrl: 'view/sucursal/alta'
    })
    .otherwise({ redirectTo:'/main'});
});
