 mainApp.controller("usuarios",function($scope,$http, Usuarios){
        $scope.inpSearch = '';
        $scope.initParams = ()=>
        {
            $scope.params = {
                page:1
            };    
        }
        $scope.showMore = ()=>
        {
            $scope.params.page++;
            $scope.getAll($scope.params);
        }
        $scope.search = (queryString)=>
        {
            $scope.searchString = queryString;
            $scope.params = Object.assign({queryString},$scope.initParams);
            $scope.getAll($scope.params);
        }
        $scope.getAll = (params=$scope.initParams()) =>
        {    
            typeof $scope.todo == 'undefined' && ($scope.todo=[]);
            
            $http(
                {
                    url: getUrl('usuariosGetAll'), 
                    method: "GET",
                    params
                 })
            .then(function(response){
                $scope.lastPage = response.data.last_page;
                response.data.data.forEach((usuario)=>{
                        !$scope.todo.find((e)=>e.id == usuario.id) &&
                        $scope.todo.push(usuario);
                    });
                    $scope.modif = response.modif;
            });
        }
        $scope.getAll();
         $scope.clearSearch = ()=>
        {
            $scope.inpSearch = $scope.searchString = '';
            $scope.getAll();
        }
        $scope.delete = function(item,index) {
            Usuarios.remove({id:item.id}).$promise.then(function(response){
                if(response.delete) {
                    toastr.info("Eliminado");
                    $scope.todo.splice(index,1);
                }
            },function(rejection){
                //toastr.error(rejection.data.msg);
            });
        };
        $scope.export = (format)=>
        {
            let params= {format};
            $http(
                {
                    url: getUrl('usuariosExport'), 
                    method: "GET",
                    params
                }).then(function(response)
                {
                    downloadFromHttpResponse(response);
                });
        }
    });
    mainApp.controller("usuarioEdit",function($scope,$http,$routeParams, Usuarios){
        $scope.usuario = Usuarios.get({id:$routeParams.id});

        $scope.onReady = function(item) {
            $scope.cambio = true;
        };
        $scope.delete = function(item=$scope.usuario) {
            Usuarios.remove({id:$routeParams.id}).$promise.then(function(response){
                if(response.delete) {
                    toastr.info("Eliminado");
                    window.location.href = "#!/usuarios";
                }
            },function(rejection){toastr.error(rejection.data.msg);});
        };
        $scope.guardar = function(item) {
            $.blockUI({ message: '<img src="img/login/loading.gif">' });
            Usuarios.update({id:$routeParams.id}, $scope.usuario).$promise.then(
                function(response){
                    $scope.cambio = false;
                    $.unblockUI();
                    toastr.success('Cambios guardados');
                },function()
                {
                    $.unblockUI();
                }
                );
        };

    });
    mainApp.controller("usuarioAlta",function($scope,$http,$routeParams, Usuarios){
       $scope.usuario = {},$scope.saving = false;
        $scope.alta = function(item) {
            $scope.saving = true;
             Usuarios.save({},$scope.usuario).$promise.then(function(response){
                if(response.alta) {
                    window.location.href = "#!/usuarios";
                }
                if(response.msj){
                    toastr.error(response.msj);
                }
            }, function()
            {
                $scope.saving = false;
            });
        };

    });
    mainApp.controller("cambiarpass",function($scope,$http,$routeParams, Usuarios){
        $scope.usuario = Usuarios.get({id:$routeParams.id});
        $scope.url = 'cambiarpass/usuario/'+ $scope.usuario.id;
        $scope.inpPassword = {};
       

        $scope.cambiar = function(pass) {
            
            $http.put(getUrl('cambiarPassword')+'/'+ $routeParams.id,$scope.inpPassword)
            .then(function(response){
                response = response.data;
                if(response.update)
                {
                    toastr.success('Password cambiada con exito');
                    window.location.href = "#!/usuarios";
                }
                else if (response.msj)
                {
                    toastr.error(response.msj);
                }
            });
        };

    });
