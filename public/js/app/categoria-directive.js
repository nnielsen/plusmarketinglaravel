mainApp.directive('categoriaItem',
 function ($ngConfirm,$http) 
 {
    return {
        link:function(scope,element,attr)
        {
            scope.inpCategory = '';
            scope.addParentCategory = false;
            scope.edit = function()
            {
                $http.get(getUrl('articuloCategorias'))
                .then(function(response)
                {
                    scope.categorias = response.data;
                    scope.selected = scope.categorias.find((e)=>e.id == scope.selectedId);
                });
                scope.setSelected = function(category=null)
                {
                    scope.selected = category;
                    scope.selectedId = category.id;
                }
                scope.createChild = function(parent,title)
                {
                    $http.post(getUrl('articuloCategoriasCreate'),{
                        parentId:(parent && parent.id)||null,
                        title
                    })
                    .then(function(response)
                    {   
                        scope.addParentCategory = false;
                        scope.categorias = response.data.categoriaAll;
                        parent && (parent.createChild = false);
                        scope.inpCategory = '';
                    });
                }
                scope.editModal =  $ngConfirm({
                    title: '',
                    offsetTop: 20,
                    content: `
                     <div style="overflow:scroll">
                        <h3>Seleccionar categoría</h3>
                        <button ng-show="addParentCategory==false" 
                         class="btn-block btn btn-default"
                        ng-click="addParentCategory=true">
                            + Agregar categoría
                        </button>
                        <div class="input-group m-b" ng-show="addParentCategory==true"  style="display:inline-flex">
                            <span class="input-group-prepend">
                                <button type="button" ng-click="createChild(null,inpCategory)" 
                                ng-disabled="!inpCategory || inpCategory==''" 
                                class="btn btn-primary" disabled="disabled">+</button> 
                            </span> 
                            <input type="text" placeholder="Nueva categoría" 
                            ng-model="inpCategory" class="form-control 
                            ng-enter="createChild(null,inpCategory)" 
                            ng-pristine ng-valid ng-empty ng-touched">
                        </div>
                        <div>
                        <ul class="directory-list">
                            <li ng-class="{'selected':selected==parent}" class="folder"  
                            ng-repeat="parent in categorias | filter:{parentId:null}">
                                <span ng-click="setSelected(parent)" ng-class="{'invert':selected==parent}" 
                                class="category-title" >{{parent.title}} </span>
                                <a class="btn btn-xs" ng-class="{'invert':selected==parent}" 
                                ng-click="parent.createChild = true">
                                            + <small class="text-muted">Agregar subcategoría</small>
                                            </a>
                                        <div ng-if="parent.createChild == true">
                                            <div class="input-group m-b" style="display:inline-flex;width:100%;">
                                            <span class="input-group-prepend">
                                                <button type="button" ng-click="createChild(parent,inpCategory)" 
                                                ng-disabled="!inpCategory || inpCategory==''" 
                                                class="btn btn-primary" style="border-radius:0;">+</button> </span> 
                                                <input type="text" placeholder="Nueva categoría" 
                                                ng-enter="createChild(parent,inpCategory)" 
                                                ng-model="inpCategory" class="form-control">
                                            </div>
                                        </div>
                                <div ng-if="(categorias | filter:{parentId:parentId}:true).length > 0">
                                    <div ng-include="'view/categoria-nodo'" ng-init="parentId = parent.id"></div>
                                </div>
                            </li>
                        </ul>
                        </div>
                    </div>
                    `,
                    scope,
                    columnClass: 'col-md-6 col-md-offset-3',
                });
            }
        },
        restrict: 'EA',
        transclude:true,
        template:   `<div class="form-group">
                        <label><strong>Categor&iacute;a</strong></label>
                        <a class="pull-right" ng-click="edit()">
                            <i class="fa fa-pencil"></i><small> Editar</small>
                        </a>
                        <p ng-if="!categoriaTree" class="text-muted">Sin categoría</p>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item" ng-repeat="categoria in categoriaTree | orderBy:'-'">
                                <span ng-class="{'text-muted':selectedId!=categoria.id}" href="index.html">{{categoria.title}}</span>
                            </li>
                        </ol>
                    </div>
                    `,
        scope: {
            categoriaTree: '=categoriaItem',
            selectedId:'='
        }
    };
});