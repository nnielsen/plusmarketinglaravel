 mainApp.controller("operacionController",function($scope,$http,$rootScope,$ngConfirm,$routeParams,session)
    {
        $scope.readSuccessWatcher = $rootScope.$on('card-read-success',(event,data)=>
        {
            if(typeof $scope.cliente != 'undefined')
            {
                 $ngConfirm({
                    title: 'Lectura de tarjeta',
                    content: 'Querés buscar al cliente con Tarjeta ID ' + data + '?',
                    scope: {$scope},
                    buttons: {
                        accept: {
                            text: 'Aceptar',
                            btnClass: 'btn-info',
                            action: function(scope, button){
                                $scope.getCliente({idMagnetico:data})
                            }
                        },
                        close: function(scope, button){
                            //
                        }
                    }
                });
            }else {
                $scope.getCliente({idMagnetico:data});
            }
        });

        if($routeParams.clienteId)
        {
            $.blockUI({ message: '<img src="img/login/loading.gif">' });
            $http({
                        url: getUrl('clienteSearch'), 
                        method: "GET",
                        params: {id:$routeParams.clienteId}
                     })
            .then(function(response)
            {
                $.unblockUI();
                $scope.cliente = response.data;
                $("#wizard").steps("next")
            },function(rejection)
            {
                toastr.error('Cliente no existente');
            });
        }
        $scope.getCleanInput = ()=>
        {
            $scope.inpCliente = {
                dni:'',
                nombre:'',
                idMagnetico:''
            };
        }
        $scope.resetClient = ()=>
        {
            $scope.getCleanInput();
            $("#wizard").steps("reset");
        }
        $scope.clearErrorPurchaseItemsInput = ()=>
        {
            $scope.purchaseItemError = {};
        }
        $scope.clearErrorPurchaseItemsInput();
        $scope.getCleanItemCompra = ()=>
        {
            $scope.itemCompra = {
                monto:'',
                descripcion:'',
                cantidad:1
            }
        }
        $scope.getCleanItemCompra();
        var rootscopeCompraReadSuccess = $rootScope.$on("cardReadSuccess",
            function(event,data){$scope.getCliente(data)});
        
        $scope.addCompraItem = ()=>
        {
            $scope.addCompraItem = true;
        }
        $scope.$on("$destroy",function()
        {
            rootscopeCompraReadSuccess();
            //$scope.swipeDestroyer();
        });
        $scope.bindDestroyer = function(func)
        {
            $scope.swipeDestroyer = func;
        }
        $scope.errorPurchaseItems = {};
        $scope.clear
        $scope.addPurchaseItem = (item=$scope.itemCompra)=>
        {
            if($scope.validatePurchaseItem($scope.itemCompra))
            {
                $scope.purchaseItems.push(angular.copy(item));
                $scope.getCleanItemCompra();        
            }
        }
        $scope.validatePurchaseItem = (item)=>
        {
            $scope.clearErrorPurchaseItemsInput();
            if(!item.descripcion || item.descripcion == ''){
                $scope.purchaseItemError.descripcion = true;
            }
            if(!item.monto || item.monto == '') {
                $scope.purchaseItemError.monto = true;
            }
            if ( isNaN(item.cantidad) || item.cantidad < 1)
            {
                $scope.purchaseItemError.cantidad = true;
            }
            console.log($scope.purchaseItemError)
            if(Object.entries($scope.purchaseItemError).length === 0 && $scope.purchaseItemError.constructor === Object) {
                return true;
            } else {
                toastr.error("Item incompleto");
            }
            
        }
        $scope.deletePurchaseItem = (index)=>
        {
            $scope.purchaseItems.splice(index,1);
        }
        $scope.deleteSwapItem = (index)=>
        {
            $scope.swapItems.splice(index,1);
        }
        $scope.getCliente = function(inpCliente = $scope.inpCliente)
        {
            if(!$scope.gettingCliente)
            {
                $scope.gettingCliente = true;
                $('#infoCliente').fadeOut(200);
                $http({
                        url: getUrl('clienteSearch'), 
                        method: "GET",
                        params: inpCliente
                     }).then(function(response){
                    if(undefined !== response && response.data)
                    {
                        response = response.data;    
                    }
                    if(undefined !== response && response.id)
                    {
                        $scope.cliente = response;
                        $('#infoCliente').fadeIn(500);
                        toastr.success('Cliente existente');
                        $("#wizard").steps("reset");
                        $("#wizard").steps("next");
                        $scope.setOperationType();
                    }
                    $scope.gettingCliente = false;
                },
                function(response)
                {
                    $scope.gettingCliente = false;
                    $scope.cliente = null;
                    $("#wizard").steps("reset");
                    toastr.error("Cliente no existente");
                });
            }
        }
        
        $scope.initArticulosParams = () =>
        {
            $scope.articulosParams = {
                page:1
            };    
        }
        $scope.showMoreArticulos = ()=>
        {
            $scope.articulosParams.page++;
            $scope.getAll($scope.articulosParams);
        }
        $scope.searchArticulo = (queryString)=>
        {
            $scope.searchStringArticulo = queryString;
            $scope.articulosParams = Object.assign({queryString},{page:1});
            $scope.getAll($scope.articulosParams);
        }
        $scope.articulos = [];
        $scope.getArticulos = (params=$scope.init) =>
        {
            return $http({
                    url: getUrl('articulosSearch'), 
                    method: "GET",
                    params: params
                 })
            .then(function(response){
                 $scope.lastPage = response.data.last_page;
                response.data.data.forEach((articulo)=>{
                        !$scope.articulos.find((e)=>e.id == articulo.id) &&
                        ($scope.articulos.push(articulo) && (articulo.selected=false));
                    });
                    $scope.modif = response.modif;
            });    
        }
        
        $scope.showSelectCanje = () =>
        {
            $scope.modalSelectCanje = $ngConfirm({
                    title: '',
                    backgroundDismiss: true,
                    escapeKey: true,
                    content: `
                        <div class="ibox" style="margin-top:20px;">
                        <h3 class="text-muted">Artículos / Canje</h3>
                            <div class="ibox-content">
                                <div class="input-group">
                                    <input type="text" placeholder="Nombre o descripción del artículo" 
                                    ng-enter="getArticulos(inpArticulo)" 
                                    class="input form-control" ng-model="inpArticulo.queryString">
                                    <span class="input-group-btn">
                                        <button type="button" ng-click="getArticulos(inpArticulo)" 
                                        class="btn btn btn-primary"> 
                                        <i class="fa fa-search"></i> Buscar</button>
                                    </span>
                                </div>
                                <div style="min-height:150px;">
                                    <ul class="nav nav-tabs">
                                        <span class="pull-right small text-muted">
                                        {{swapItems.length}} articulo{{swapItems.length==1?"":"s"}} seleccionado{{swapItems.length==1?"":"s"}}
                                        </span>
                                    </ul>
                                    <div class="tab-content">
                                        <div id="tab-1" class="tab-pane active">
                                            <div class="full-height-scroll">
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-hover">
                                                        <thead>
                                                            <th>Articulo</th>
                                                            <th>Puntos</th>
                                                            <th>Stock</th>
                                                            <th></th>
                                                        </thead>
                                                        <tbody>
                                                            <tr ng-repeat="articulo in articulos | filter:inpArticulo.queryString" 
                                                            ng-class="{'selected-row':articulo.selected==true}">
                                                                <td width="70%"><a data-toggle="tab"  class="client-link"> 
                                                                    <a class="pull-left">
                                                                    <div style="padding-right:10px;">
                                                                        <img src="downImg/{{articulo.imagen}}" alt="IMAGEN" class="img-circle imgMin" 
                                                                        ng-show="{{articulo.imagen}}">
                                                                        <img class="no-image" src="img/NA.png" ng-hide="{{articulo.imagen}}" 
                                                                        class="img-circle imgMin">
                                                                    </div>
                                                                    </a>{{articulo.nombre}}</a>
                                                                </td>
                                                                <td width="10%">
                                                                    {{articulo.puntos}}
                                                                </td>
                                                                <td width="10%">
                                                                    {{articulo.stock}}
                                                                </td>
                                                                <td>
                                                                    <div  ng-show="articulo.stock">
                                                                        <a ng-if="articulo.selected==false" class="btn btn-success dim btn-circle"
                                                                         type="button" ng-click="addSwapItem(articulo);articulo.selected = true;">
                                                                            <i class="fa fa-external-link-square"></i>
                                                                        </a>
                                                                        <a ng-if="articulo.selected==true" class="btn btn-warning dim btn-circle" 
                                                                        type="button" ng-click="deleteSwapItem(articulo);articulo.selected = false;">
                                                                            <i class="fa fa-times"></i>
                                                                        </a>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <div class="text-center text-muted" ng-show="articulos.length == 0">
                                                        <span>
                                                            <h3>Usa el buscador para seleccionar los articulos</h3>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                    <div class="modal-footer">
                        <button ng-click="closeModalSelectCanje()" type="button" class="btn btn-info">Listo</button>
                    </div>
                </div>
                    `,
                    scope:$scope,
                    columnClass: 'ng-confirm-box-pxcol-md-6 col-md-offset-3 articles-select',
      
                });
        }
        $scope.closeModalSelectCanje = ()=>
        {
            $scope.modalSelectCanje.close();
        }
        $scope.getDescripciones = function(queryString)
        {
            typeof $scope.descripciones == 'undefined' && ($scope.descripciones = []);
            $http({
                    url: getUrl('compraDescripcionSugerencia'), 
                    method: "GET",
                    params: {queryString}
                 }).then(function(response){
                response.data.forEach((item)=>
                    {
                        !$scope.descripciones.find((i)=>i.descripcion == item.descripcion)
                        && $scope.descripciones.push(item);
                    });
            });
        }
        $scope.getClienteSugerencias = function(q)
        {
            q && q!='' && 
            $http(
                {
                    url: getUrl('suggestCliente'), 
                    method: "GET",
                    params: {queryString:q}
                }).then(function(response){
                $scope.clientesSugerencias = response.data;
            });
        }
        
        $scope.addSwapItem = (item)=>
        {
            $scope.swapItems.push(Object.assign(item, {cantidad:1}));
        }

        $scope.setCompra = function(dataCompra)
        {
            $scope.doingCompra = true;
            $.blockUI({ message: '<img src="img/login/loading.gif">' });
            $http.post(getUrl('compraCreate'),
                {
                    purchaseItems:$scope.purchaseItems,
                    clienteId:$scope.cliente.id
                }
            ).then(function(response){
                response = response.data;
                $scope.doingCompra = false;

                $.unblockUI();
                if(response.compra)
                {
                    toastr.success('Compra ingresada');    
                    $scope.cliente = response.cliente;
                    $scope.comprobante = 'view/compra/comprobante/' + response.id;
                    $("#wizard").steps("next");
                }
                if(response.errors)
                {
                    $scope.handleTransactionError(response.errors);
                }
            },
            function(response)
            {
                $.unblockUI();
                $scope.doingCompra = false;
            });
        }
        
        $scope.setCanje = function(articulo)
        {
            $.blockUI({ message: '<img src="img/login/loading.gif">' });
            $http.post(getUrl('canjeCreate'),{    
                'swapItems':$scope.swapItems.map(({id,cantidad,nombre})=> {return {id,cantidad,nombre}}),
                'clienteId':$scope.cliente.id
            }).then(function(response){
                $.unblockUI();
                response = response.data;
                if(response.canje)
                {
                    $scope.cliente = response.cliente;
                    toastr.success('Canje ingresado');

                    $scope.comprobante = 'view/canje/comprobante/' + response.id;
                    $("#wizard").steps("next");
                }

                if(response.errors)
                {
                    $scope.handleTransactionError(response.errors);
                }
            },
            function(response)
            {
                $scope.handleTransactionError(response);
                $.unblockUI();
            });
        }
        $scope.handleTransactionError = function(response)
        {
            if(response.transport_exception)
            {
                toastr.warning('Error en el envio de mail');
            }
        }
        $scope.verComprobante = function()
        {
            var url = $scope.comprobante,
            token = session.get('token');
            $.ajax({
                url: url,
                type: 'get',
                headers: {
                    'Authorization':'Bearer ' + token
                },
                success: function (data) 
                {
                    var windowName = 'Comprobante Plus Marketing'; 
                    var w = window.open('about:blank', windowName, 'width=1000, height=700, left=24, top=24, scrollbars, resizable,toolbar=no');
                    if (w == null || typeof(w)=='undefined') {  
                        toastr.error('Por favor permita las ventanas emergentes en este navegador.'); 
                    } 
                    else 
                    {  
                        w.document.open();
                        w.document.write(data);
                        w.document.close();
                    }
                }
            });
        }
        $scope.setOperationType = (type='compra')=>
        {
            if(type == 'compra')
            {
                $scope.setOperacion = $scope.setCompra;
                $scope.purchaseItems = [];
            }else {
                $scope.setOperacion = $scope.setCanje;
                $scope.swapItems = [];
            }
        }
        $scope.goToClient = (cliente) =>
        {
            location.assign("#!/cliente/edit/"+cliente.id);
        }
        $scope.setOperationType();

        $scope.$on("$destroy", function() {
           $scope.readSuccessWatcher();
       });
       $scope.clearErrorPurchaseItemsInput();
  
    });