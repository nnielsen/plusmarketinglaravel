mainApp.controller("articulos",function($scope,$http, Clientes,Articulos){
        $scope.inpSearch = '',$scope.saving = false;
        $scope.initParams = (object={})=>
        {
            $scope.params = Object.assign({
                page:1
            },object);    
        }
        $scope.showMore = ()=>
        {
            $scope.params.page++;
            $scope.getAll($scope.params);
        }
        $scope.search = (queryString)=>
        {
            $scope.searchString = queryString;
            $scope.getAll($scope.initParams({queryString}));
        }

        $scope.getAll = (params=$scope.params,append=true) =>
        {
            typeof $scope.todo == 'undefined' && ($scope.todo=[]);
            $http(
                {
                    url: getUrl('articulosAll'), 
                    method: "GET",
                    params
                 })
            .then(function(response)
            {
                $scope.lastPage = response.data.last_page;
                if(append)
                {
                    response.data.data.forEach((articulo)=>{
                        !$scope.todo.find((e)=>e.id == articulo.id) &&
                        $scope.todo.push(articulo);
                    });    
                }else {
                    $scope.todo = response.data.data;
                }
                $scope.modif = response.modif;
            });
        }
        $scope.initParams();
        $scope.getAll();
        $scope.clearSearch = ()=>
        {
            $scope.inpSearch = $scope.searchString = '';
            delete $scope.params.queryString;
            $scope.getAll($scope.initParams(),false);
        }
        $scope.edit = (articulo)=>{
            location.assign("#!/articulo/edit/"+articulo.id);
        }
        $scope.delete = function(item,index) {
                Articulos.remove({id:item.id}).$promise.then(function(response){
                    if(response.delete) {
                        toastr.info("Eliminado");
                        $scope.todo.splice(index,1);
                    }
                });
            };
        $scope.export = (format)=>
        {
            let params= {format};
            $http(
                {
                    url: getUrl('articulosExport'), 
                    method: "GET",
                    params
                 }).then(function(response)
                {
                    downloadFromHttpResponse(response);
                });
        }    
    });
mainApp.controller("articuloAlta",function($scope,$http,$routeParams, Articulos)
{

        $scope.uploadFile = function(files) {
            var fd = new FormData();
            fd.append("img", files[0]);
            token = localStorage.getItem('token');
            toastr.info('Subiendo imagen');
            $http.post(getUrl('uploadArticuloImagen')+$scope.altaArticuloId, fd, {
                withCredentials: true,
                headers: {'Content-Type': undefined ,auth:token },
                transformRequest: angular.identity
            }).then(function(reponse){$.unblockUI();},function(response)
            {   
                $.unblockUI();
                toastr.info('Podés volver a intentar agregar imagen al artículo desde la pantalla de edición')
            })
        };
        $scope.alta = function(item) {
            $scope.saving = true;
             Articulos.save({},$scope.articulo).$promise.then(function(response){
                $scope.saving = false;
                if(response.alta) {
                    $scope.altaArticuloId=response.alta.id;
                    toastr.success('Alta de articulo exitosa.');
                    $.blockUI({ message: '<img src="img/login/loading.gif">' });
                    if($('#file').prop('files').length > 0)
                    {
                        $scope.uploadFile($('#file').prop('files'));
                    }
                    setTimeout(function(){ window.location.href = "#!/articulos";$.unblockUI(); }, 1000);
                }
                if(response.msj){
                    toastr.error(response.msj);
                }
            },function(){
                $scope.saving = false;
            });
        };

    });
mainApp.controller("articuloEdit",function($scope,$http,$routeParams, Articulos)
{
    $scope.$watch(function() {
        return $scope.articulo.categoriaId;
    }, function(categoryId) {
        $scope.onReady();
        if(categoryId)
        {
            $http({
                params:{
                    categoryId
                },
                url:getUrl('updateCategoryTree'),
                method:"GET"
            })
            .then(function(response)
            {
                $scope.articulo.categoriaTree = response.data;
            });
        }
    });
        $scope.articulo = Articulos.get({id:$routeParams.id});
        if($scope.articulo.$resolved && $scope.articulo.$resolved == false)
        {
            window.location.href = "#!/articulos";
        }

        $scope.onReady = function() {
            $scope.cambio = true;
        };
        $scope.delete = function(item = $scope.articulo) {
            Articulos.remove({id:item.id}).$promise.then(function(response){
                if(response.delete) {
                    toastr.info("Eliminado");
                    location.assign("#!/articulos");
                }
            });
        };    
        $scope.uploading=false;
         $scope.uploadFile = function(files) {
            var fd = new FormData();
            fd.append("img", files[0]);
            token = localStorage.getItem('token');
            toastr.info('Subiendo imagen');
            $scope.uploading = true;
            $.blockUI({ message: '<img src="img/login/loading.gif">' });
            $http.post(getUrl('uploadArticuloImagen')+$routeParams.id, fd, {
                withCredentials: true,
                headers: {'Content-Type': undefined ,auth:token },
                transformRequest: angular.identity
            }).then(function(response){
                $.unblockUI();
                toastr.success('Imagen modificada');
                $scope.articulo.imgUrl = response.data.url;
                changeImg(files[0],$('#imgArticulo'));
                $scope.uploading = false;
            },function(response)
            {   
                $.unblockUI();
                toastr.info('Podés volver a intentar agregar imagen al artículo desde la pantalla de edición')
            })
        };
        $scope.setImagen = function(id)
        {
            $scope.articulo.imagen = id;
        }

        $scope.guardar = function(item) {
            $.blockUI({ message: '<img src="img/login/loading.gif">' });
            Articulos.update({id:$routeParams.id}, $scope.articulo).$promise.then(
                function(response){
                    if(response.msj)
                    {
                        $.unblockUI();
                        toastr.error(response.msj);
                    }
                    if(response.update)
                    {
                        $scope.cambio = false;
                        $.unblockUI();
                        toastr.success('Cambios guardados');    
                    }
                    
            },function(rejection){
                $.unblockUI();
            });
        };
    });