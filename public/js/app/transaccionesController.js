   mainApp.controller("clienteTransacciones",function($scope,$http,$routeParams, Clientes,$rootScope,$ngConfirm){
        
        $scope.clearInputSearch = ()=>
        {
            return $scope.inpSearch = {
                'fromDate':'',
                'toDate':''
            };
        }
        $scope.clearInputSearch();
        $scope.getCliente = ()=>
        {
            $scope.cliente = Clientes.get({id:$routeParams.id});    
        }
        $scope.getCliente();
        $scope.initParams = (object={})=>
        {
            return $scope.params = Object.assign(object,{
                page:1,
                clienteId:$routeParams.id
            });    
        }

        $scope.showMore = ()=>
        {
            $scope.params.page++;
            $scope.getAll($scope.params,true);
        }

        $scope.delete = (item,index)=>
        {
            $scope._delete(item,index);
        }
        $scope.edit = (item)=>
        {
            $scope._methodResolver(item,'Edit');
        }
        $scope._methodResolver = (item,action)=>
        {
            $itemType = item.idCompra?"compra":"canje";
            $scope[$itemType+action](item);
        }
        $scope.compraEdit = (compra)=>
        {
            if(!compra.edit)
            {
                $scope.getCompraDetails(compra);
            }
            compra.edit = !compra.edit;
        }
        $scope.getCompraDetails = (compra)=>
        {
            $http.get(getUrl('compra')+'/items',{params:{id:compra.idCompra}})
                .then(function(response)
                {
                    compra.items = response.data;
                });
        }
        $scope.compraItemEdit = (compraItem)=>
        {
            $http.put(getUrl('compra')+'/item',compraItem)
                .then(function(response)
                {
                    if(response.data.item)
                    { 
                        $scope.getAll($scope.params);
                    }
                });
        }
        $scope.canjeEdit = (canje) =>
        {
            if(!canje.edit)
            {
                $http.get(getUrl('canje')+'/items',{params:{id:canje.idCanje}})
                .then(function(response)
                {
                    canje.items = response.data;
                });
            }           
            canje.edit =! canje.edit;
        }
        $scope.changes = (item)=>
        {
            item.changes = true;
        }
        $scope._delete = (item,index)=>
        {
            $http({
                    params:{id:item.idCompra?item.idCompra:item.idCanje},
                    url:getUrl(item.idCompra?"compra":"canje"),
                    method:"DELETE"
                }).then(function(response)
                {
                    if(response.data.delete)
                    {
                        item.deleted_at = true;
                        //$scope.getCliente();
                    }
                },function(rejection)
                {
                    rejection.data.error && toastr.error(rejection.data.error);
                });
        }
        $scope.deleteItem = (parentItem,item,index)=>
        {
            let params = {
                id:item.id
            },url = getUrl(parentItem.idCompra?'compraItem':'canjeItem');
            $http({
                    params:params,
                    url:url,
                    method:"DELETE"
                }).then(function(response)
                {
                    parentItem.puntos -= item.puntos * item.cantidad;
                    if(response.data.delete)
                    {
                        parentItem.items.splice(index,1);
                    }else if(response.data.deleteParent)
                    {
                        $scope.getAll($scope.params);
                        toastr.info('Se ha anulado la transaccion');
                    }
                },function(rejection)
                {
                    rejection.data.error && toastr.error(rejection.data.error);
                });
        }
        
        $scope.getAll = (params=$scope.params,append=false)=>
        {
            typeof $scope.todo == 'undefined' && ($scope.todo=[]);
               return $http({
                    url:getUrl('clienteTransacciones'),
                    params,
                    method:"GET"
                }).then(function(response)
                {
                    $scope.lastPage = response.data.totalPages;
                    if(append)
                    {
                        response.data.todo.forEach((item)=>{
                        $scope.todo.push(item);
                    });
                    }else {
                        $scope.todo = response.data.todo;
                    }
                    
                });
        }
        $scope.filtrar = (inpSearch)=>
        {
            $scope.getAll($scope.initParams(inpSearch))
            .then(()=>
            {
                $scope.actualFilter = inpSearch;
            });
        }
        $scope.clearFilter = () =>
        {
            $scope.initParams();
            $scope.getAll()
            .then(()=>
            {
                delete $scope.actualFilter;
                $scope.clearInputSearch();
            });
        }
        $scope.initParams();
        $scope.getAll();

    });