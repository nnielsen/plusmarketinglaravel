mainApp.directive('confirmationNeeded', 
  ['$ngConfirm',function ($ngConfirm) 
  {
  return {
    priority: 1,
    terminal: true,
    link: function (scope, element, attr) 
    {
      var msg = attr.confirmationNeeded || "¿Estás seguro?";
      var title = attr.confirmationTitle || "Confirmar";
      var confirmClass = attr.confirmClass || "info";
      var clickAction = attr.ngClick;

      scope.cancelAction = function()
      {
        scope.modal.close();
      }
      scope.doClickAction = function()
      {
        scope.goTo && location.assign(scope.goTo) ||
        scope.$eval(clickAction);
        scope.modal.close();
      }  

      element.bind('click',function () {
       scope.modal =  $ngConfirm({
                    title: '',
                    content: `
                    <div style="margin:20px 20px 0px 20px;width:90%;heigth:200px;">
                    <h3>`+title+`</h3>
                        `+msg+`
                    <div class="row" style="margin-top:15px;">
                        <div class="text-center">
                            <button ng-click="cancelAction()" class="btn 
                            btn-default m-r-3 buttonMinimum">
                                Cancelar
                            </button>
                            <button ng-click="doClickAction()" 
                            class="btn btn-` + confirmClass + ` buttonMinimum">
                                Confirmar
                            </button>
                        </div>
                    </div>
                    </div>
                    `,
                    scope: scope,
                    columnClass: 'col-md-6 col-md-offset-3',
      
                });
      });
    }
  };
}]);