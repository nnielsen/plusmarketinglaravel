mainApp.directive('cardInput',
 function () 
 {
    return {
        restrict: 'EA',
        transclude:true,
        template:   `<div class="text-center m-t-md animated fadeInRight" style="margin-bottom:1em;" ng-show="manualCardInput">
                        <label>Ingrese el ID de tarjeta </label>
                        <div class="form-group">
                            <div>
                               <input ng-model="cardInput" name="tarjeta" class="form-control" ng-enter="searchCallback()"/>
                            </div>
                        </div>
                    </div>
                    <div ng-show="!manualCardInput" class="text-center m-t-md animated fadeInRight">      
                     <div class="text-center">
                        <i class="fa fa-credit-card big-icon"></i>
                        <div class="id-magnetico text-info">
                            <h1>{{valueRead}}</h1>
                        </div>
                    </div>
                            
                        <label class="text-muted">Lector esperando tarjeta</label>
                    </div>`,
        scope: {
            cardInput: '=',
            manualCardInput:'=',
            valueRead:'=',
            searchCallback:'&?'
        }
    };
});