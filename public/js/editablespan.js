 var mod = angular.module("mk.editablespan", []);
 mod.directive("editablespan", function() {
     return {
         restrict: "E",
         template: ` <div>
         <div ng-hide="editing" style="padding:3px;border:1px solid #e5e6e7;">
         <span ng-class="spanClass">{{text | sinDatos:\'Sin datos\'}}</span>
         </div>
        <form ng-show="editing"><input type="text" ng-class="inputClass"></form><div>`,
         scope: {
             text: "=model",
             onReady: "&",
             spanClass: "@",
             inputClass: "@"
         },
         replace: !0,
         link: function(a, b) {
             function c() {
                 d(), h(!0), l[0].focus()
             }

             function d() {
                 l.bind("blur", function() {
                      l[0].value && e(), f()
                 }), l.bind("keyup", function(a) {
                     i(a) && f()
                 }), k.bind("submit", function() {
                     l[0].value && e(), f()
                 })
             }

             function e() {
                 a.text = l[0].value, a.$apply(), a.onReady()
             }

             function f() {
                 g(), h(!1)
             }

             function g() {
                 l.unbind(), k.unbind()
             }

             function h(b) {
                 a.editing = b, a.$apply()
             }

             function i(a) {
                 return a && 27 == a.keyCode
             }
             var j = angular.element(b.children()[0]),
                 k = angular.element(b.children()[1]),
                 l = angular.element(b.children()[1][0]);
             j.bind("click", function() {
                 l[0].value = a.text, c()
             })
         }
     }
 });