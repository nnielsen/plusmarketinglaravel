<?php

return [

	'genders'=>[
		['name'=>'masc','value'=>1],
		['name'=>'fem','value'=>0],
		['name'=>'nc','value'=>2]
	],
	'civilStates'=>[
		['name'=>'marr','value'=>1],
		['name'=>'sin','value'=>0],
		['name'=>'cou','value'=>2],
		['name'=>'wid','value'=>3]
	],
	'userTypes'=>
	[
		'admin'=>1,
		'business'=>2
	],
	'fileTypes'=>
	[
		'jpg','png','gif'
	],
	'instanceManager'=>
	[
		'errors'=>
		[
			'STATE_CHANGE_FAIL'=>'Hubo un error al intentar cambiar el estado de la instancia'
		],
		'states'=>
		[
			'play'=>1,
			'pause'=>0
		]
	]
];
