<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateConfiguracionInstancia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configuracion_instancia', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('instanceId')->unsigned();
            $table->foreign('instanceId')->references('id')->on('instance');
            $table->string('fb',70)->nullable();
            $table->string('tw',70)->nullable();
            $table->string('ig',70)->nullable();
            $table->string('direccionLinea1')->nullable();
            $table->string('direccionLinea2')->nullable();
            $table->string('telefono')->nullable();
            $table->longText('terminosCondiciones')->nullable();
            $table->integer('logoId')->unsigned()->nullable();
            $table->foreign('logoId')->references('id')->on('file');
            $table->string('idMagneticoRegex')->nullable();
            $table->integer('mailPort')->nullable();
            $table->string('mailHost',70)->nullable();
            $table->string('mailUsername',70)->nullable();
            $table->string('mailPassword',70)->nullable();
            $table->string('mailEncryption',5)->nullable();
            $table->string('mailFromMail',70)->nullable();
            $table->string('mailFromName',70)->nullable();
            $table->string('mailSMTPSecure',3)->nullable();
            $table->boolean('mailSMTPPeerVerif')->nullable();

            $table->timestamps();
        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('configuracion_instancia');
    }
}
