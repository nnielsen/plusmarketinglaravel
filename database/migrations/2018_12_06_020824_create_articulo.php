<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticulo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articulo', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->longText('descripcion');
            $table->integer('imagenId')->unsigned()->nullable();
            $table->foreign('imagenId')->references('id')->on('file');
            $table->integer('categoriaId')->unsigned()->nullable();
            $table->foreign('categoriaId')
                ->references('id')
                    ->on('articulo_categoria')
                    ->onDelete('set null');
            $table->integer('visitasCatalogo')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articulo');
    }
}
