<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class PuntosCanjeView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('
            CREATE VIEW puntos_canje AS
                SELECT  canje_item.id as canjeItemId,
                        (SELECT articulo_puntos.`puntos` as articuloPuntos 
                            FROM articulo_puntos
                            WHERE articulo_puntos.`articuloId` = canje_item.`articuloId`
                            AND canje_item.`created_at` > articulo_puntos.`created_at`
                            ORDER BY articulo_puntos.`created_at` DESC
                            LIMIT 1) as puntosCanje,
                        canje_item.cantidad
                FROM canje_item
            ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('puntos_canje');
    }
}
