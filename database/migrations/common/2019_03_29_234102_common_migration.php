<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use App\instanceManager;

class CommonMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        ini_set('memory_limit', '512M');
        $instanceManager = instanceManager::getInstance();
        $instanceManager->setCommonDB();
        DB::disableQueryLog();
        Schema::create('provincia', function (Blueprint $table) {
                $table->increments('id');
                $table->string('nombre');
            });
            DB::unprepared(file_get_contents(base_path() . DIRECTORY_SEPARATOR  . 'database/insert_provincias.sql'));

            Schema::create('ciudad', function (Blueprint $table) {
                $table->increments('id');
                $table->string('nombre');
                $table->integer('cp');
                $table->integer('provincia_id')->unsigned();
                $table->foreign('provincia_id')->references('id')->on('provincia');
            });
            DB::unprepared(file_get_contents(file_get_contents(base_path() . DIRECTORY_SEPARATOR  . 'database/insert_ciudades.sql')));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
