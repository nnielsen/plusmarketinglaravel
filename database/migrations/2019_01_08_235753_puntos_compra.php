<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PuntosCompra extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('
            CREATE VIEW puntos_compra AS
                SELECT  compra_item.id as compraItemId,
                    compra_item.`monto` *
                    (SELECT puntos_peso.`coeficiente`
                    FROM puntos_peso
                    WHERE compra_item.`created_at` >= puntos_peso.`created_at`
                    ORDER BY puntos_peso.`created_at` DESC
                    LIMIT 1) as puntosCompra,
                    compra_item.cantidad
                    FROM compra_item
            ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('puntos_compra');
    }
}
