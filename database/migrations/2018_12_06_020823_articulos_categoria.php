<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ArticulosCategoria extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('articulo_categoria', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->integer('parentId')
                ->nullable()->unsigned();
            $table->foreign('parentId')
                ->references('id')->on('articulo_categoria')
                ->onUpdate('cascade')->onDelete('cascade');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articulo_categoria');
    }
}
