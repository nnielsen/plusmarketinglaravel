<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EnvioMail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('envio_mail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ext_id')->unsigned()->nullable();
            $table->char('tipo',10);
            $table->dateTime('enviado');
            $table->dateTime('leido')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
