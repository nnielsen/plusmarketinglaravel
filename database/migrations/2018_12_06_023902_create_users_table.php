<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('alias');
            $table->string('nombre');
            $table->string('apellido');
            $table->string('mail');
            $table->string('telefono');
            $table->integer('inputMethodId')->unsigned()->nullable;
            $table->smallInteger('tipoId')->unsigned();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->boolean('acceptedTermsConditions')->default(false);
            $table->integer('instanciaId')->unsigned()->default('1');
            $table->foreign('instanciaId')->references('id')->on('instance');
            $table->integer('sucursal_id')->unsigned()->nullable();
            $table->foreign('sucursal_id')->references('id')->on('sucursal')
                    ->onDelete('set null');
            $table->string('api_token', 60)->unique()->nullable();
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
