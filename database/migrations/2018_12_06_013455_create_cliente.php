<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCliente extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cliente', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('apellido');
            $table->string('mail',100);
            $table->string('calle');
            $table->integer('altura');
            $table->tinyInteger('piso')->nullable();
            $table->string('depto',4)->nullable();
            $table->integer('ciudad_id')->nullable()->unsigned();
            $table->integer('metodoIngresoId')->nullable();
            $table->string('codigoPostal',10)->nullable();
            $table->string('telefono',30);
            $table->timestamp('nacimiento');
            $table->tinyInteger('genero');
            $table->tinyInteger('estadoCivil');
            $table->string('dni',9);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cliente');
    }
}
