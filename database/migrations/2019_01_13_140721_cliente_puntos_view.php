<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ClientePuntosView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('
            CREATE VIEW puntos_cliente AS
                    SELECT cliente.*, COALESCE((
                    SELECT SUM(puntos_compra.`puntosCompra` * puntos_compra.cantidad)
                                AS puntosCompra
                                FROM puntos_compra 
                                WHERE puntos_compra.`compraItemId` IN
                                    (SELECT compra_item.id
                                    FROM compra_item
                                    RIGHT JOIN compra
                                    ON compra.`id` = compra_item.`compraId`
                                    WHERE compra.`clienteId` = cliente.id
                                    AND compra.`deleted_at` IS NULL
                                    AND compra_item.`deleted_at` IS NULL )
                ),0) - COALESCE(
                (
                    SELECT SUM(puntos_canje.`puntosCanje` * puntos_canje.cantidad)
                            AS puntosCanje
                            FROM puntos_canje
                            WHERE puntos_canje.`canjeItemId` IN
                                (SELECT canje_item.id
                                FROM canje_item
                                RIGHT JOIN canje
                                ON canje.`id` = canje_item.`canjeId`
                                WHERE canje.`clienteId` = cliente.id
                                AND canje.`deleted_at` IS NULL)
                ),0) as puntos_totales 
                FROM cliente
                HAVING puntos_totales != 0
                ORDER BY puntos_totales DESC
            ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("puntos_cliente");
    }
}
