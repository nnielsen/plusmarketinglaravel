<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCanjeItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('canje_item', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('canjeId')->unsigned();
            $table->foreign('canjeId')->references('id')->on('canje');
            $table->integer('cantidad')->unsigned();
            $table->integer('articuloId')->unsigned();
            $table->foreign('articuloId')->references('id')->on('articulo');
            $table->timestamps();
            $table->softDeletes();
            $table->index(['canjeId','id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('canje_item');
    }
}
