q<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompraItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('compra_item', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('compraId')->unsigned();
            $table->foreign('compraId')->references('id')->on('compra');
            $table->integer('cantidad')->unsigned();
            $table->string('descripcion');
            $table->double('monto');
            $table->timestamps();
            $table->softDeletes();
            $table->index(['compraId','id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('compra_item');
    }
}
