<?php

use Faker\Generator as Faker;

$factory->define(App\tarjeta::class, function (Faker $faker) {
    return [
        'idMagnetico'=>$faker->numberBetween($min = 1000, $max = 9999)
    ];
});
