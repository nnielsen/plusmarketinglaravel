<?php

use Faker\Generator as Faker;

$factory->define(App\articulo::class, function (Faker $faker) {
    return [
        'nombre'=>$faker->domainWord,
        'descripcion'=>$faker->domainName
    ];
});
