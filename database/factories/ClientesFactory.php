<?php

use Faker\Generator as Faker;

$factory->define(App\cliente::class, function (Faker $faker) {
    return [
        'nombre'=>$faker->name,
            'apellido'=>$faker->lastName,
            'mail'=>$faker->unique()->email,
            'calle'=>$faker->streetName,
            'altura'=>$faker->buildingNumber,
            'ciudad_id'=>$faker->numberBetween($min = 1111, $max = 1199),
            'telefono'=>$faker->phoneNumber,
            'metodoIngresoId'=>0,
            'nacimiento'=>$faker->date($format = 'Y-m-d', $max = '-30 years'),
            'genero'=>$faker->randomElement($array = array ('1','2')),
            'estadoCivil'=>$faker->randomElement($array = array ('1','2','3')),
            'dni'=>$faker->numberBetween($min = 900000, $max = 40000000)
    ];
});
