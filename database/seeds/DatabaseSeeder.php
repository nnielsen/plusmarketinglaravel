<?php
namespace Database\Seeds;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\cliente;
use App\tarjeta;
use App\User;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
       /* $cliente = new cliente([
            'nombre'=>'Nico',
            'apellido'=>'Nielsen',
            'mail'=>'niconiels@gmail.com',
            'calle'=>'Austria',
            'altura'=>'1444',
            'piso'=>5,
            'depto'=>13,
            'ciudad'=>'BSAS',
            'pais'=>'telefono',
            'telefono'=>'1111111111',
            'metodoIngresoId'=>0,
            'nacimiento'=>date('Y-m-d'),
            'genero'=>1,
            'estadoCivil'=>1,
            'dni'=>'36594730'
        ]);
        $cliente->save();
        $tarjeta = new tarjeta(['idMagnetico'=>1000]);
        $cliente->tarjeta()->save($tarjeta);
	       

	    $usuario = new User([
            'alias'=>'admin',
            'nombre'=>'admin',
            'apellido'=>'admin',
            'mail'=>'niconiels@gmail.com',
            'telefono'=>'1111111111',
            'metodoIngresoId'=>0,
            'password'=>''
        ]);
        $usuario->tipoId = \Config::get('constants.userTypes.admin');
        $usuario->save();

        */

        factory(App\cliente::class, 10)->create()->each(function ($cliente) {
	        $tarjeta = new tarjeta(['idMagnetico'=>1000]);
	        $cliente->tarjeta()->save(factory(App\tarjeta::class)->make());
	    });
	    factory(App\articulo::class, 10)->create()->each(function ($articulo) {
			$stock = new App\articuloStock(['amount'=>rand(1,1000)]);
        	$puntos = new App\articuloPuntos(['puntos'=>rand(10,10000)]);
        	$articulo->getStock()->save($stock);
        	$articulo->getPuntos()->save($puntos);
	    });

         DB::table('file_types')->insert(
                array(
                    'extension' => 'jpg'
                )
            );
         DB::table('puntos_peso')->insert(
                array(
                    'coeficiente'=>1,
                    'created_at'=>date('Y-m-d'),
                    'updated_at'=>date('Y-m-d')
                )
            );
          DB::table('instance')->insert(
                array(
                    'name' => 'default',
                    'bussinessName'=>'bussinessName',
                    'url'=>'url',
                    'adminPass'=>'adminPass',
                    'adminUser'=>'adminUser'
                )
            );
           factory(App\User::class, 1)->create()->each(function($user)
            {
                $verification_code = str_random(30); //Generate verification code
                DB::table('user_verifications')->insert(['user_id'=>$user->id,'token'=>$verification_code]);
            });
            DB::table('configuracion_instancia')->insert(
                array(
                    'instanceId' => 1,
                    'mailPort'=> env('MAIL_PORT'),
                    'mailHost'=> env('MAIL_HOST'),
                    'mailUsername'=> env('MAIL_USERNAME'),
                    'mailPassword'=> env('MAIL_PASSWORD'),
                    'mailEncryption'=> env('MAIL_ENCRYPTION'),
                    'mailFromName'=> env('MAIL_FROM_NAME'),
                    'mailFromMail'=> env('MAIL_FROM_ADDRESS')
                )
            );

    }
}
