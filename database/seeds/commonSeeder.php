<?php
namespace Database\Seeds;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;

class commonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		Schema::create('provincia', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
        });
        DB::unprepared(file_get_contents(base_path() . DIRECTORY_SEPARATOR  . 'database/insert_provincias.sql'));

        Schema::create('ciudad', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->integer('cp');
            $table->integer('provincia_id')->unsigned();
            $table->foreign('provincia_id')->references('id')->on('provincia');
        });
    }
}
